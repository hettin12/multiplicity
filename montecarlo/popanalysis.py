"""
Functions for reading / writing population montecarlo data.

Functions for plotting results of montecarlo runs.
"""

import os, glob

import matplotlib.pyplot as plt
from numpy import log10


def get_model_paths(modelDir):
    """Use glob to find all of the files in this directory that correspond
    to the model.  Return a list of paths."""
    globString = os.path.join(modelDir, 'run_*.sqlite3')
    return glob.glob(globString)


def create_fig():
    """Create the figure to add data to."""
    fig = plt.figure(figsize=[18,12])
    plt.clf()
    return fig


def overplot_eoi_hist(eoiData, ax, color='k', alpha=0.05):
    """Overplot the e/i histogram data onto the given figure."""
    n, bins, __ = ax.hist(eoiData, 100, range=[0,20], visible=False)
    binCenters = [bins[i] + (bins[i+1]-bins[i])/2. for i in range(len(bins)-1)]
    logn = []
    for value in n:
        if value == 0:
            logn.append(-1.0)
        else:
            logn.append(log10(value))
    ax.plot(binCenters, logn, alpha=alpha, color=color, ls='steps-mid')


def save_figure(fig, plotPath):
    """Clean up plot and save it to file."""    
    ax = fig.gca()
    ax.set_ylim(-0.2, 5)
    ax.set_xlabel('e/i')
    ax.set_ylabel('log(N)')
    plt.tight_layout()
    fig.savefig(plotPath)

