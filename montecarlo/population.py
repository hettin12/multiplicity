import numpy as np

from multiplicity.radialvelocity import orbit, absoluterv


def load_population_config(configPath, requestedSetNumber):
    """Load the population config file containing values for each parameter in the form:
    #setNumber   binaryFraction   massRatioPowerIndex   lowestMassRaio   separationDistributionPower   minSeparation(AU)   maxSeparation(AU)
    Returns a dictionary."""
    configDict = {}
    with open(configPath, 'r') as inFile:
        for line in inFile.readlines():
            if len(line.strip()) == 0: continue
            if line[0] == '#': continue
            tup = line.split()
            thisSet = int(tup[0])
            if thisSet == requestedSetNumber:
                configDict = { 'binaryFraction':float(tup[1]),
                               'beta':float(tup[2]),
                               'qlow':float(tup[3]),
                               'alpha':float(tup[4]),
                               'amin':float(tup[5]),
                               'amax':float(tup[6]) }
                break
    if len(configDict) == 0:
        raise Exception('set number %d is not found in config file' % requestedSetNumber)
    keys = ['binaryFraction', 'beta', 'qlow', 'alpha', 'amin', 'amax']
    for k in keys:
        if k not in configDict:
            raise Exception('%s not found in config file.' % k)
    return configDict


def pull_from_powerlaw(index, low, high):
    """ Return a random draw from a power-law 
        distribution given index, low, high.
    """
    if index == -1:
        log_value = np.random.uniform(np.log10(low), np.log10(high))
        return pow(10, log_value)
    else:
        n = index + 1
        uni = np.random.random()
        value = uni*(pow(high,n) - pow(low,n)) + pow(low,n)
        value = pow(value, (1./n))
        return value


def draw_primary_mass():
    """Draw a mass from the Kroup IMF.

    Distribution from Kroupa (2001)
        p(m) = C*m^-a         a = {2.3 for m > 0.5 Msun
    This uses the inverse method to map x = [0.0, 1.0) onto p(m)
        F^-1 = ((1-a)*x/C + low^(1-a))^ (1/ 1-a))
    C is the normalization constant
        C = 1.0 / integral(m^-a * dm) from lo to hi

    The masses returned are for F0-G0 stars ~ 1.0 Msun to 1.8 Msun
    """
    return pow((-1.3*0.4110*np.random.random() + 1.0), (-0.7692))


def simulate_system(fiberid, fiberDataDict, configDict, outDB):
    """Simulate a system and compute e/i for this fiber, given
    a param dictionary and fiber data (errs, obs, etc.)."""
    taimids = fiberDataDict['taimids']
    tobs = [t - taimids[0] for t in taimids]
    errs = fiberDataDict['empiricalErrs']
    #fudgeFactor = configDict['fudge']
    #if fudgeFactor != 1.0:
    #    errs = [e*fudgeFactor for e in errs]

    binary = np.random.random() < configDict['binaryFraction']

    if binary:
        m1 = draw_primary_mass()
        q = pull_from_powerlaw(configDict['beta'], configDict['qlow'], 1.0)
        a = pull_from_powerlaw(configDict['alpha'], configDict['amin'], configDict['amax'])
        incl = np.random.random() * (2.*np.pi)        # Random inclination [0.0, 2pi).
        nu = np.random.random() * (2.*np.pi)          # Random mean anomoly [0.0, 2pi) at t0.
        rv0 = 0.0
        cleanRVs = orbit.calculate_velocities(tobs, m1, q, a, rv0, incl, nu)
    else:
        m1, q, a, = 0.0, 0.0, 0.0
        cleanRVs = [0.0 for t in tobs]

    dirtyRVs = []
    for i, rv in enumerate(cleanRVs):
        dirtyRVs.append(rv + np.random.normal(0.0, scale=errs[i]))

    eoveri = absoluterv.calc_eoveri(dirtyRVs, errs)

    # write to database
    if outDB.id_in_table(fiberid) is None:
        outDB.insert_entry(fiberid)
    outDB.set_many_values( fiberid, 
                           isBinary=binary,
                           massPrimary=m1,
                           massRatio=q,
                           separation=a,
                           exposureCount=len(tobs),
                           taimids=str(taimids),
                           empiricalErrs=str(errs),
                           absRVs=str(dirtyRVs),
                           eoveri=eoveri )
                          #period=
                          #exposureSNR=
                          #feh=
                          #bayesFactor=


