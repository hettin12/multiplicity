"""
Template class is a subclass of Spectrum.  This allows you to create a 'Spectrum' by reading
in simple (wave, flux, var) values.

"""

import os

import numpy as np

from dsdt.Spectrum import Spectrum
import dsdt.Mask as Mask


def load_template_file(templatePath):
    """Open the template file and read in the three columns.  
    Return np.ndarrays (lamb, flux, var)."""
    if not os.path.exists(templatePath):
        raise Exception("Could not find %s" % self.templatePath)

    with open(templatePath, 'r') as inFile:
        lamb, flux, var = [], [], []
        for line in inFile.readlines():
            c0, c1, c2 = line.split()
            lamb.append(float(c0))
            flux.append(float(c1))
            var.append(float(c2))

    return np.array(lamb), np.array(flux), np.array(var)


class Template(Spectrum):
    def __init__(self, templatePath=None):
        """Read in the appropriate template information and create a spectrum. 
        Template files are assumed to be normalized."""

        # Read in the template file and set the values
        self.lamb, self.flux, self.var = load_template_file(templatePath)
        self.n = len(self.lamb)
        self.sdssMask = np.zeros(self.n, dtype=np.int32)
        self.dsdtMask = np.zeros(self.n, dtype=np.int32)        

        # Check lengths of arrays are equal
        nF, nS, nD, nV = len(self.flux), len(self.sdssMask), len(self.dsdtMask), len(self.var)
        if (self.n != nF) or (self.n != nS) or (self.n != nD) or (self.n != nV):
            raise RuntimeError("arrays not same length: " + "lamb/flux/sdssMask/dsdtMask/var = %d/%d/%d/%d/%d" % (self.n, nF, nS, nD, nV))

        self.smooth = {}
        self.crReplacement = None

