"""
Functions for combining fibers together to create a master template.
"""
import os, sys

import numpy as np
import matplotlib.pyplot as plt

from multiplicity.prepfiber import prepfiber
from dsdt.der_snr import DER_SNR


def get_fiber_objects(fiberlist, verbose=False):
    """Load the Fiber objects for each in the list and return
    a List of objects."""
    fiberObjectList = []
    for n, fiberid in enumerate(fiberlist):
        thisFiber = prepfiber.get_Fiber(fiberid)
        if thisFiber is None:
            raise Exception("Warning: %s Fiber not found." % fiberid)
        fiberObjectList.append(thisFiber)
        if verbose and ((n+1) % 100 == 0):
            print '%d loaded...' % (n+1)
    return fiberObjectList


def get_coadds(fiberObjectList):
    """Given a fiberlist, return a List of coadd spectra from
    the individual Fiber objects."""
    coaddSpectra = []
    for fiber in fiberObjectList:
        thisCoaddSpectrum = fiber.coadd
        coaddSpectra.append(thisCoaddSpectrum)
    return coaddSpectra


def deshift_spectra(fiberObjectList, spectraList):
    """De-shift each of the spectra given by using the fiber's 
    best-z value (taken from zBest values).

    CONSIDER USING SSPP_DR10: RV_ADOP INSTEAD.
    """
    deshiftedSpectra = []
    for fiber, spec in zip(fiberObjectList, spectraList):
        thisDeshift = spec.deshift(fiber.get_best_z())
        deshiftedSpectra.append(thisDeshift)
    return deshiftedSpectra


def calc_template_wavelength(fiberObjectList, spectraList):
    """Use the average number of pixels for nPix and 
    make sure wavelength coverage allows all spectra to be
    included.  Spectra should be de-shifted.
    Return the wavelength solution."""
    nPix = int(np.median([f.nPix for f in fiberObjectList]))
    minWave = max([s.lamb[0] for s in spectraList])
    maxWave = min([s.lamb[-1] for s in spectraList])
    templateLogWave = np.linspace(np.log10(minWave), np.log10(maxWave), nPix)
    templateWave = 10.0**templateLogWave
    return templateWave


def combine_to_form_template(spectraList):
    """Given a list of spectra, all resampled to the same wavelength
    solution, average the spectra together and return a Spectrum
    object."""
    templateSpec = spectraList[0].copy()
    for i,spec in enumerate(spectraList):
        if i == 0:
            continue
        templateSpec += spec

    templateSpec.flux /= float(len(spectraList))
    templateSNR = DER_SNR(templateSpec.flux)
    templateSpec.var = (templateSpec.flux / templateSNR)**2.0
    return templateSpec.lamb, templateSpec.flux, templateSpec.var


def generate_template(fiberlist, verbose=False, regionSet=None):
    """Use the fibers to create a template by cleaning, deshifting, 
    and averaging.  Return the template (lamb, flux, var)."""
    # Get Fiber objects
    if verbose:
        print 'Loading Fibers...'
    fiberObjectList = get_fiber_objects(fiberlist, verbose=verbose)
    # Get coadd Spectra
    coaddSpectra = get_coadds(fiberObjectList)
    # Normalize and clean the coadds
    if verbose:
        print 'Normalizing...'
    if regionSet is None:
        cleanedCoaddSpectra = prepfiber.clean_spectra(coaddSpectra, trimSize=0)
    elif regionSet == 'F':
        cleanedCoaddSpectra = prepfiber.clean_spectra(coaddSpectra, trimSize=0, patchRegions=prepfiber.F_STAR_REGIONS)
    elif regionSet == 'G':
        cleanedCoaddSpectra = prepfiber.clean_spectra(coaddSpectra, trimSize=0, patchRegions=prepfiber.G_STAR_REGIONS)
    elif regionSet == 'K':
        cleanedCoaddSpectra = prepfiber.clean_spectra(coaddSpectra, trimSize=0, patchRegions=prepfiber.K_STAR_REGIONS)
    elif regionSet == 'M':
        cleanedCoaddSpectra = prepfiber.clean_spectra(coaddSpectra, trimSize=0, patchRegions=prepfiber.M_STAR_REGIONS)
    # De-redshift the normalized specs
    if verbose:
        print 'De-Shifting...'
    deshiftedSpectra = deshift_spectra(fiberObjectList, cleanedCoaddSpectra)
    # Determine template wavelengths
    templateWave = calc_template_wavelength(fiberObjectList, deshiftedSpectra)
    # Resample the normalized/deshifted coadds to the new wavelengths
    if verbose:
        print 'Resampling...'
    resampledSpecs = [s.resample(templateWave, weighted=True) for s in deshiftedSpectra]
    # Combine resampled, redshifted coadds into a template
    if verbose:
        print 'Averaging...'
    templateLamb, templateFlux, templateVar  = combine_to_form_template(resampledSpecs)
    return templateLamb, templateFlux, templateVar


def plot_template(lamb, flux, var):
    """Plot the template spectrum. Return a pyplot Figure."""
    fig = plt.figure(figsize=[32,9])
    plt.plot(lamb, flux, color='DodgerBlue')
    plt.xlabel('Wavelength (Angstroms)')
    plt.ylabel('Normalized Flux')
    plt.tight_layout()
    return fig

