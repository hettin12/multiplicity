"""
Read and write SSPP FITS files.  

Select F-type stars within a FITS file based on various criteria.

Perform quality cuts based on SNR, pixel count, exposure count.

"""
import os

import pyfits
import numpy as np

from multiplicity.prepfiber import prepfiber

def degrees_RA_to_hours(ra_in_degrees):
    """Convert right ascension from degrees [0,360] to HH:MM:SS."""
    if (ra_in_degrees > 360) or (ra_in_degrees < 0):
        raise Exception('Degrees outside of limit %f' % dec_in_degrees)
    hours = ra_in_degrees / 15.0
    h = int(hours)
    remainder = abs(hours - h)
    minutes = 60.0 * remainder
    m = int(minutes)
    remainder = abs(minutes - m)
    seconds = 60.0 * remainder
    s = '%0.3f' % seconds
    return '%02d:%02d:%s' % (h, m, s.zfill(6))


def degrees_DEC_to_sexigesimal(dec_in_degrees):
    """Convert declination from degrees [-90,90] to DD:MM:SS."""
    if (dec_in_degrees > 90) or (dec_in_degrees < -90.0):
        raise Exception('Degrees outside of limit %f' % dec_in_degrees)
    d = int(dec_in_degrees)
    remainder = abs(dec_in_degrees - d)
    minutes = 60.0 * remainder
    m = int(minutes)
    remainder = abs(minutes - m)
    seconds = 60.0 * remainder
    s = '%0.3f' % seconds
    return '%02d:%02d:%s' % (d, m, s.zfill(6))


def fiber_path_exists(plate, mjd, fiber):
    """Return boolean if the fiber file path exists on this machine."""
    spFiberFile = "spFiber-%04d-%05d-%03d.fits" % (int(plate), int(mjd), int(fiber)) 
    for path in os.environ['DSDT_DATA'].split(':'):
        platePath = os.path.join(path, "%04d" % int(plate))
        fibersPath = os.path.join(platePath, "fibers")
        fileName = os.path.join(fibersPath, spFiberFile)
        if os.path.exists(fileName):
            return True
    return False


def get_column_names(fitsPath):
    """Return a list of the column names."""
    hdulist = pyfits.open(fitsPath, memmap=True)
    primary = hdulist[0]
    cols = hdulist[1].columns    
    hdulist.close()
    return [c.name for c in cols]


def open_fits(fitsPath, printCols=False):
    """Open a FITS file and return the 'columns' and 'data'."""
    # Open FITS files and read in HDUs.
    hdulist = pyfits.open(fitsPath, memmap=True)
    primary = hdulist[0]
    cols = hdulist[1].columns
    data = hdulist[1].data
    hdulist.close()                                              # If memory mapping, do this after reading individual HDUs.
    if printCols:
        print cols
    return cols, data


def get_data_dictionary(fitsPath, useSpecObjAsKey=False):
    """Return all the data as one large dictionary with fiberid as the key.
    Each entry in the dictionary is a dictionary itself with column name 
    as the key.  Takes a long time to read into RAM."""
    cols, data = open_fits(fitsPath)
    dataDict = {}
    for i in range(len(data)):
        if i % 1000 == 0:
            print i
        plate = data[i].field('PLATE')
        mjd = data[i].field('MJD')
        fiber = data[i].field('FIBER')
        pmf = '%04d-%05d-%03d' % (int(plate), int(mjd), int(fiber))
        specobjid = int(data[i].field('SPECOBJID'))

        thisDict = dict(zip(cols.names, data[i]))
        if useSpecObjAsKey:
            dataDict[specobjid] = thisDict
        else:
            dataDict[pmf] = thisDict
    return dataDict


def get_stellar_param_dictionary(fitsPath):
    """Same as get data dictionary, but each fiber only has
    {[Fe/H], Teff, logg, gmag}.  Returns a dictionary of dictionaries."""
    cols, data = open_fits(fitsPath)
    dataDict = {}
    for i in range(len(data)):
        plate = data[i].field('PLATE')
        mjd = data[i].field('MJD')
        fiber = data[i].field('FIBER')
        feh = data[i].field('FEH_ADOP')
        teff = data[i].field('TEFF_ADOP')
        logg = data[i].field('LOGG_ADOP')
        gmag = data[i].field('G_MAG')
        pmf = '%04d-%05d-%03d' % (int(plate), int(mjd), int(fiber))
        thisDict = {'feh': feh, 'teff': teff, 'logg': logg, 'gmag': gmag}
        dataDict[pmf] = thisDict
    return dataDict


def get_row_by_fid(fitsPath, fiberid):
    """Given a PMF fiberid, find that row in the database and return the row.
    If not found, return None."""
    cols, data = open_fits(fitsPath, printCols=False)
    mjdList = data.field('MJD')
    plateList = data.field('PLATE')
    fiberList = data.field('FIBER')

    plate, mjd, fiber = fiberid.split('-')
    plate = int(plate)
    mjd = int(mjd)
    fiber = int(fiber)
    for i in range(len(data)):
        if mjd == mjdList[i]:
            if plate == plateList[i]:
                if fiber == fiberList[i]:
                    return data[i]
    return None


def write_FITS(index, data, outPath):
    """Given an index and FITS data, write a new FITS file
    using only the rows specified in the Index."""
    thisData = data[np.array(index)]
    new_hdu = pyfits.BinTableHDU(thisData)
    new_hdu.writeto(outPath)


def write_fiberlist(index, data, outPath):
    """Give an index, FITS data, and outPath, write a text
    file with a list of the fiber PMF values."""
    thisData = data[np.array(index)]
    mjd = thisData.field('MJD')
    plate = thisData.field('PLATE')
    fiber = thisData.field('FIBER')
    with open(outPath, 'w') as outFile:
        for i in range(len(fiber)):
            outFile.write('%04d-%05d-%03d\n' % (int(plate[i]), int(mjd[i]), int(fiber[i])))


def merge_classifications(indiciesList):
    """Give a list of index lists, perform a 'and' operation on
    each element and return the final index."""
    if len(indiciesList) <= 0:
        raise Exception("Must specifiy at least one constraint for F-type stars.")

    finalList = []
    for n, index in enumerate(indiciesList):
        if n == 0:
            finalList = index
        else:
            finalList = [a and b for a,b in zip(finalList, index)]            
    return finalList


def get_index_from_pmf(data, pmfList):
    """Get indices for fibers in the data based on an input list of
    PMF values.  Returns a list of indices."""
    mjd    = data.field('MJD')
    plate  = data.field('PLATE')
    fiber  = data.field('FIBER')

    index = []

    for i in range(len(fiber)):
        if i % 100000 == 0:
            print '%d fibers checked.' % i

        pmf = '%04d-%05d-%03d' % (int(plate[i]), int(mjd[i]), int(fiber[i]))
        if pmf in pmfList:
            index.append(True)
        else:
            index.append(False)

    print '%d of %d entries found.' % (sum(index), len(pmfList))
    return index



def select_Ftype(data, checkHammer=True, checkElodie=False, checkSubclass=True, minLogg=3.75):
    """Get the list of fiber PMFs that are reported as any F-type star using all classifiers specified
    in the arguments (hammer and/or elodie). Stars must also have valid values for stellar params
    including {Teff, logg, [Fe/H]}.  Also make a cut at logg >= 3.75.
    Returns PMFs in list form."""
    subcla = data.field('SPECTYPE_SUBCLASS')
    #elodie = data.field('SPECTYPE_ELODIE')     # Doesn't contain all 10 F-spectral types. Only F2, F5, F9.  Not present at all in DR9/DR10.
    hammer = data.field('SPECTYPE_HAMMER')     # Contains all 10 F-spectral types. F0 - F9
    feh    = data.field('FEH_ADOP')
    teff   = data.field('TEFF_ADOP')
    logg   = data.field('LOGG_ADOP')
    mjd    = data.field('MJD')
    plate  = data.field('PLATE')
    fiber  = data.field('FIBER')

    print 'There are %d fibers in this FITS file.' % len(fiber)

    hammerIndex = []
    #elodieIndex = []
    subclassIndex = []
    validParamsIndex = []
    dwarfIndex = []
    onDiskIndex = []

    for i in range(len(fiber)):
        if i % 100000 == 0:
            print '%d fibers checked.' % i

        # Find F-type stars
        if hammer[i][0] == 'F':
            hammerIndex.append(True)
        else:
            hammerIndex.append(False)

        #if elodie[i][0] == 'F':
        #    elodieIndex.append(True)
        #else:
        #    elodieIndex.append(False)

        if subcla[i][0] == 'F':
            subclassIndex.append(True)
        else:
            subclassIndex.append(False)

        # Find fibers with valid parameters
        if logg[i] <= -9998.0 or feh[i] <= -9998.0 or teff[i] <= -9998.0:
            validParamsIndex.append(False)
        else:
            validParamsIndex.append(True)

        # Find dwarfs
        if logg[i] >= minLogg:
            dwarfIndex.append(True)
        else:
            dwarfIndex.append(False)

        # Ensure fits file is on disk
        if fiber_path_exists(plate[i], mjd[i], fiber[i]):
            onDiskIndex.append(True)
        else:            
            onDiskIndex.append(False)

    # Check that all index lists are same length
    for indexSize in [len(hammerIndex), len(subclassIndex), len(validParamsIndex), len(dwarfIndex), len(onDiskIndex)]:
        assert indexSize == len(fiber)

    # Combine F-type values into a single index
    indiciesToUse = []
    if checkHammer:
        indiciesToUse.append(hammerIndex)
    if checkElodie:
        indiciesToUse.append(elodieIndex)
    if checkSubclass:
        indiciesToUse.append(subclassIndex)
    fStarIndex = merge_classifications(indiciesToUse)


    # Print statistics on search
    print "Statistics:"
    print "%d fibers total in this File." % len(fiber)
    print "%d classified as F-type by Hammer." % sum(hammerIndex)
    #print "%d classified as F-type by Elodie." % sum(elodieIndex)
    print "%d classified as F-type by Subclass." % sum(subclassIndex)
    print "%d classified as F-type using %d specified classifications." % (sum(fStarIndex), len(indiciesToUse))
    print "%d fibers had valid stellar parameters." % sum(validParamsIndex)
    print "%d fibers are dwarfs (logg >= 3.75)." % sum(dwarfIndex)
    print "%d fibers had FITS files currently on disk." % sum(onDiskIndex)

    print "%d are slassified as F-type dwarfs." % sum(merge_classifications([fStarIndex, dwarfIndex]))
    print "%d are F-type dwarfs and have valid params." % sum(merge_classifications([fStarIndex, dwarfIndex, validParamsIndex]))
    finalIndex = merge_classifications([fStarIndex, dwarfIndex, validParamsIndex, onDiskIndex])
    print "%d are F-type dwarfs with valid params, and have file on disk." % sum(finalIndex)

    return finalIndex


def quality_cut(data, snrMin, pixelMin, expMin):
    """Load each fiber and look at the number of good pixels and signal-to-noise.  Return
    an index of fibers that have enough good exposures to qualify."""
    mjd    = data.field('MJD')
    plate  = data.field('PLATE')
    fiber  = data.field('FIBER')

    index = []
    for i in range(len(fiber)):
        # Load Fiber
        fiberid = '%04d-%05d-%03d\n' % (int(plate[i]), int(mjd[i]), int(fiber[i]))
        thisFiber = prepfiber.get_Fiber(fiberid)
        if thisFiber is None:
            index.append(False)
            continue

        # Get Fiber info
        goodPixelCounts = [len(exp.whereGood()) for exp in thisFiber.raw]
        exposureSNR = thisFiber.snr

        # Count number of good exposures
        nGoodExp = 0
        for e in range(thisFiber.nExp):
            if goodPixelCounts[e] >= pixelMin and exposureSNR[e] >= snrMin:
                nGoodExp += 1

        # Decide if Fiber is good
        if nGoodExp >= expMin:
            index.append(True)
        else:
            index.append(False)

    assert len(index) == len(fiber)
    print '%d Fibers passed the quality cuts.' % sum(index)
    return index

