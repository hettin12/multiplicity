"""
Functions for selecting fibers in SNR/FeH bins and calculating empirical errors
using the Median Absolute Deviation.
"""
import os

import numpy as np

from multiplicity.radialvelocity import absoluterv
from multiplicity.empiricalerrors import ploterrors, fitting

from multiplicity.figures.figures import setup_plot_directory

def calculate_1D_solution(snrGroups, sigmas, const=None, fig=None, label=None):
    """Given a list of snrGroups and sigmas (x and y) for a single
    metallicity group, calculate a 1/x solution.  To fit a solution, 
    there must be at least four data points."""
    cleanX, cleanY = [], []
    for snr, sigma in zip(snrGroups, sigmas):
        if sigma > 0.0:
            cleanX.append(snr)
            cleanY.append(sigma)

    if len(cleanX) < 4:
        return None
    
    thisSolution = fitting.fit_data(cleanX, cleanY, const)

    if fig is not None:
        modelX = np.linspace(15,65,100)
        if const is None:
            model = lambda x: thisSolution[0]/x + thisSolution[1]
        else:
            model = lambda x: thisSolution[0]/x + const
        modelY = model(modelX)
        ploterrors.plot_sigma_vs_snr(snrGroups, sigmas, fig, label, modelX, modelY)

    return thisSolution


def calculate_sigma_using_MAD(velocities, minRVCount=250, plotPath=None):
    """Given a list of RVs, calcualte and return the sigma from the median aboslute deviation."""
    if len(velocities) < minRVCount:
        return 0.0
    MAD = np.median(abs(np.array(velocities) - np.median(velocities)))
    if plotPath is not None:
        ploterrors.plot_MAD_comparison(velocities, MAD, plotPath)
    return MAD*1.4826
    

def select_exposures(fiberDict, fehLimits, snrLimits, minExpPerFiber=2):
    """Given a ditionary of fiber data, find all individual
    exposures that have a FeH and SNR within the bounds specified by
    limits. Returns a single list of RVs.  A fiber's RVs are only added
    if there is at least minExpPerFiber."""
    rvList = []
    for pmf, params in fiberDict.iteritems():
        if fehLimits[0] < params['feh'] <= fehLimits[1]:
            cleanRVs = []
            for snr, rv in zip(params['exposureSNR'], params['absRVs']):
                if snrLimits[0] < snr <= snrLimits[1]:
                    cleanRVs.append(rv)
            if len(cleanRVs) >= minExpPerFiber:
                rvList.extend(cleanRVs)
    return rvList


def deshift_radial_velocities(fiberDict):
    """For each fiber, calculate the median radial velocity, and
    offset each exposure to the rest frame value be subtracting from
    the mean.  Return a dictionary with deshifted velocities."""
    deshiftedFiberDict = {}
    for pmf, params in fiberDict.iteritems():
        thisMean = np.mean(params['absRVs'])
        newParams = params
        newParams['absRVs'] = [rv - thisMean for rv in params['absRVs']]
        deshiftedFiberDict[pmf] = newParams
    return deshiftedFiberDict


def calc_empirical_errors(tableDict, fehGroups, snrGroups, fehThresh=0.25, snrThresh=2.5, minRVCount=250, minExpPerFiber=2, plotDir=None):
    """Calculate the coefficients for a function that describes
    the empirical error as a function of [Fe/H] and SNR. The form 
    of the function is sigma = (p1*feh + p2) / snr + p2"""
    # Define metallicity and SNR limits
    fehLimits = [(g-fehThresh, g+fehThresh) for g in fehGroups]
    snrLimits = [(g - min(snrThresh, g-20), g + min(snrThresh, g-20)) for g in snrGroups]

    # Deshift every exposure
    deshiftedFibers = deshift_radial_velocities(tableDict)

    # Calculate sigmas for each bin
    sigmas = {}
    for fehGroup, fehLimit in zip(fehGroups, fehLimits):
        sigmas[fehGroup] = {}
        for snrGroup, snrLimit in zip(snrGroups, snrLimits):
            theseRVs = select_exposures(deshiftedFibers, fehLimit, snrLimit, minExpPerFiber=minExpPerFiber)
            thisPlotPath = None
            if plotDir is not None:
                thisDir = os.path.join(plotDir, '%.2f/' % fehGroup)
                if not os.path.isdir(thisDir):
                    os.mkdir(thisDir)
                thisPlotPath = os.path.join(thisDir, '%.1f_MAD.png' % snrGroup)
            sigmas[fehGroup][snrGroup] = calculate_sigma_using_MAD(theseRVs, minRVCount=minRVCount, plotPath=thisPlotPath)
        
    # Calculate 1D solution with 2 free params (m/x + b)
    fig_A = None
    if plotDir is not None:
        fig_A = ploterrors.make_empirical_figure()
    solutions_A = {}
    for fehGroup, sigDict in sorted(sigmas.iteritems()):
        snrList, sigmaList = zip(*sigDict.iteritems())
        thisSolution = calculate_1D_solution(snrList, sigmaList, fig=fig_A, label=str(fehGroup))
        solutions_A[fehGroup] = thisSolution
    for key, value in solutions_A.iteritems():
        try:
            print '(Fe/H = %.2f)  sigma = %f/SNR + %f' % (key, value[0], value[1])
        except:
            print 'FeH', key
            raise
    if plotDir is not None:
        fig_A.tight_layout()
        fig_A.savefig(os.path.join(plotDir, 'step_A_sigma_vs_snr.png'))

    # calculate mean(b)
    p2 = np.median([value[1] for key,value in solutions_A.iteritems()])
    print 'Using median %.3f' % p2

    # Calculate 1D solution with 1 free param (m/x + C) using mean(b)
    fig_B = None
    if plotDir is not None:
        fig_B = ploterrors.make_empirical_figure()
    solutions_B = {}
    for fehGroup, sigDict in sorted(sigmas.iteritems()):
        snrList, sigmaList = zip(*sigDict.iteritems())
        thisSolution = calculate_1D_solution(snrList, sigmaList, const=p2, fig=fig_B, label=str(fehGroup))
        solutions_B[fehGroup] = thisSolution
    for key, value in solutions_B.iteritems():
        try:
            print '(Fe/H = %.2f)  sigma = %f/SNR + %f' % (key, value[0], p2)
        except:
            print 'FeH', key
            raise
    if plotDir is not None:
        fig_B.tight_layout()
        fig_B.savefig(os.path.join(plotDir, 'step_B_sigma_vs_snr.png'))

    # Find a linear solution to the new 'm' values
    feh, slope = zip(*solutions_B.iteritems())
    x = [i for i in feh]
    y = [i[0] for i in slope]
    solution_C = fitting.linear_fit(x, y)
    print solution_C
    if plotDir is not None:
        fig = ploterrors.plot_linear_fit(x, y, solution_C)
        fig.tight_layout()
        fig.savefig(os.path.join(plotDir, 'step_C_linear_fit.png'))

    # Plot final solution
    if plotDir is not None:
        fig = ploterrors.make_empirical_figure()
        modelX = np.linspace(15,65,100)
        markers = ['^', 'o', '<', 'D', '>', 's', 'v', '*', 'H']
        m = 0
        for fehGroup, sigDict in sorted(sigmas.iteritems()):
            snrList, sigmaList = zip(*sigDict.iteritems())
            model = lambda x: (solution_C[0]*fehGroup + solution_C[1]) / x + p2
            modelY = model(modelX)
            label = r'[Fe/H] $\simeq$ %.2f' % fehGroup
            ploterrors.plot_sigma_vs_snr(snrList, sigmaList, fig, label, modelX, modelY, marker=markers[m])
            m += 1

        plotPathDict = setup_plot_directory(plotDir, 'step_C_sigma_vs_snr')
        fig.gca().legend(loc=1)
        RATIO = 1.4
        fig.set_size_inches((3.0, 3.0/RATIO))
        fig.tight_layout(pad=0.1)
        fig.savefig(plotPathDict['smallEPS'])
        fig.savefig(plotPathDict['smallPNG'])
        fig.set_size_inches((7.0, 7.0/RATIO))
        fig.tight_layout(pad=0.1)
        fig.savefig(plotPathDict['bigEPS'])
        fig.savefig(plotPathDict['bigPNG'])

    # Return (p0, p1, p2) where sigma = (p0*feh + p1) / snr + p2
    return (solution_C[0], solution_C[1], p2)


