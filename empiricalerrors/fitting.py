"""
Functions used for fitting 1D and 2D functions to the 
empirical error data points.  Inputs include the sigma value
for specific FeH/SNR bins.
"""
from scipy.optimize import curve_fit
import numpy as np

def fit_data(x, y, const=None):
    """Fit the functional form m/x + b to the data points
    x and y.  Return the coeffients of the function. Use constant
    b if specified."""
    if const is None:
        func = lambda x, m, b: m/x + b
    else:
        func = lambda x, m: m/x + const

    popt, pcov = curve_fit(func, x, y, p0=None)
    return popt


def linear_fit(x, y):
    """Fit a line to a set of (x,y) points."""
    # Initial Guess using first and last points
    tup = zip(x,y)
    tup.sort(key=lambda t: t[0])
    dx = x[-1] - x[0]
    dy = y[-1] - y[0] 
    slope = dy / dx
    offset = (y[0] - slope*x[0])
    p0Guess = (slope, offset)
    
    # Solve
    func = lambda x, m, b: m*x + b
    popt, pcov = curve_fit(func, np.array(x), np.array(y), p0=p0Guess)
    return popt

