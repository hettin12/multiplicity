"""
Plotting functions for the empirical error analysis.
"""

import os

import numpy as np
from scipy.stats import norm

import matplotlib.pyplot as plt

from multiplicity.figures import figures

def make_empirical_figure():
    """Make and return a matplotlib figure."""
    return plt.figure(figsize=[12,12])


def plot_MAD_comparison(originalMeasurements, MAD, plotPath):
    """Given measurements and a MAD value, plot a histogram of the measurements,
    and over plot a histogram of an equal number of synthetic measurments, drawn
    from a Gaussian of sigma = 1.4826*MAD."""
    sigma = 1.4826*MAD
    mu = 0.0
    x = np.linspace(-50,50,101)
    model = norm(loc=mu, scale=MAD*1.4826)

    plt.figure(figsize=[18,9])
    plt.hist(originalMeasurements, 60, range=[-50,50], normed=True, label='Original Velocities')
    plt.plot(x, model.pdf(x), label=r'Normal PDF from MAD: $\mu=$%.2f $\sigma=$%.2f' % (mu, MAD*1.4826), lw=4, color='red')
    plt.title('MAD Comparison')
    plt.xlabel('Radial Velocity (km/s)')
    plt.ylabel('Normalized Count')
    plt.tight_layout()
    plt.savefig(plotPath)
    

def plot_sigma_vs_snr(snrBins, sigmas, fig, label, modelX=None, modelY=None, marker='o'):
    """For this single group of metallicity, plot a scatter showing
    the relationship between signal-to-noise, and empirical error."""
    ax = fig.gca()
    thisColor= ax._get_lines.color_cycle.next()
    if modelX is not None:
        ax.plot(modelX, modelY, color=thisColor)
    ax.plot(snrBins, sigmas, marker=marker, ls='none', ms=6, label=label, color=thisColor)
    ax.set_xlabel('SNR')
    ax.set_ylabel('$\sigma_\mathrm{RV}$ (km/s)')
    ax.set_ylim(1.0, 5.0)
    ax.set_xlim(20, 60)


def plot_linear_fit(x, y, p):
    """Plot a set of x,y points and a linear fit specified by
    parameters y = p0*x + p1."""
    func = lambda x: p[0]*x + p[1]
    fig = plt.figure(figsize=[9,9])
    plt.plot(x, y, ls='none', marker='o', ms=8)
    modelX = np.linspace(-2.5, 0.5, 10)
    modelY = func(modelX)
    plt.plot(modelX, modelY)
    plt.xlabel('Fe/H')
    plt.ylabel('Slope')
    plt.tight_layout()
    return fig

