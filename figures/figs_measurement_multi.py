"""
Histograms displaying measurment information broken down by groups.
"""
import matplotlib.pyplot as plt
import numpy as np

from multiplicity.radialvelocity import absoluterv
import figures

widths = [5, 3, 1]
colors = ['DodgerBlue', 'black', 'red']


def plot_multiple_RV_combo(dataDictList, plotDir):
    """Plot average RV measurement histogram and std(RV measurement) histogram
    for all groups in the list as a two subplot combo."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'comboRV_hist_multi')
    fig = plt.figure()

    plt.subplot(211)
    thisRange = [-620, 620]
    nBins = 30
    for m,dataDict in enumerate(dataDictList):
        rvLists = figures.get_all_by_key(dataDict, 'absRVs')
        errLists = figures.get_all_by_key(dataDict, 'empiricalErrs')
        meanRV = [absoluterv.calc_weighted_mean(rvs, errors) for rvs,errors in zip(rvLists, errLists)]
        n, binEdges, __ = plt.hist(meanRV, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    plt.xlabel(r'$\overline{\rm{RV}}$ (km/s)')
    plt.ylabel(r'log(N)')
    plt.xlim(-600, 600)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()

    plt.subplot(212)
    thisRange = [-2, 58]
    nBins = 30
    for m,dataDict in enumerate(dataDictList):
        rvLists = figures.get_all_by_key(dataDict, 'absRVs')
        errLists = figures.get_all_by_key(dataDict, 'empiricalErrs')
        stdRV = [absoluterv.calc_weighted_std(rvs, errors) for rvs,errors in zip(rvLists, errLists)]
        n, binEdges, __ = plt.hist(stdRV, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    plt.xlabel(r'std(RV) (km/s)')
    plt.ylabel(r'log(N)')
    plt.xlim(0)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()

    GOLDEN_RATIO = 1.61803
    SMALL_FIG_SIZE = (3.0, 3.0*GOLDEN_RATIO)
    fig.set_size_inches(SMALL_FIG_SIZE)
    fig.tight_layout(pad=0.1)
    fig.savefig(plotPathDict['smallEPS'])
    fig.savefig(plotPathDict['smallPNG'])


def plot_multiple_meanRV_hist(dataDictList, plotDir):
    """Plot average RV measurement histogram for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'meanRV_hist_multi')
    fig = plt.figure()
    thisRange = [-620, 620]
    nBins = 30
    for m,dataDict in enumerate(dataDictList):
        rvLists = figures.get_all_by_key(dataDict, 'absRVs')
        errLists = figures.get_all_by_key(dataDict, 'empiricalErrs')
        meanRV = [absoluterv.calc_weighted_mean(rvs, errors) for rvs,errors in zip(rvLists, errLists)]
        n, binEdges, __ = plt.hist(meanRV, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    plt.xlabel(r'$\overline{\rm{RV}}$ (km/s)')
    plt.ylabel(r'log(N)')
    plt.xlim(-600, 600)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()
    plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)


def plot_multiple_stdRV_hist(dataDictList, plotDir):
    """Plot std(RV measurements) histogram for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'stdRV_hist_multi')
    fig = plt.figure()
    thisRange = [-2, 58]
    nBins = 30
    for m,dataDict in enumerate(dataDictList):
        rvLists = figures.get_all_by_key(dataDict, 'absRVs')
        errLists = figures.get_all_by_key(dataDict, 'empiricalErrs')
        stdRV = [absoluterv.calc_weighted_std(rvs, errors) for rvs,errors in zip(rvLists, errLists)]
        n, binEdges, __ = plt.hist(stdRV, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    plt.xlabel(r'std(RV) (km/s)')
    plt.ylabel(r'log(N)')
    plt.xlim(0)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()
    plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)


def plot_multiple_meanErr_hist(dataDictList, plotDir):
    """Plot average measurement error histogram for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'meanErr_hist_multi')
    fig = plt.figure()
    thisRange = [-1, 9]
    nBins = 20
    for m,dataDict in enumerate(dataDictList):
        errLists = figures.get_all_by_key(dataDict, 'empiricalErrs')
        meanErr = [absoluterv.calc_i(errs) for errs in errLists]
        n, binEdges, __ = plt.hist(meanErr, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    plt.xlabel(r'$\overline{\sigma}$ (km/s)')
    plt.ylabel(r'log(N)')
    plt.xlim(0, 8)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()
    plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)


def plot_multiple_stdErr_hist(dataDictList, plotDir):
    """Plot std(measurement errors) histogram for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'stdErr_hist_multi')
    fig = plt.figure()
    thisRange = [-0.05, 1.0]
    nBins = 21
    for m,dataDict in enumerate(dataDictList):
        errLists = figures.get_all_by_key(dataDict, 'empiricalErrs')
        stdErr = [absoluterv.calc_unbiased_sample_standard_deviation(errs) for errs in errLists]
        n, binEdges, __ = plt.hist(stdErr, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    plt.xlabel(r'std($\sigma$)')
    plt.ylabel(r'log(N)')
    plt.xlim(0, 1)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()
    plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)


def plot_multiple_eoveri_hist(dataDictList, plotDir):
    """Plot hisogram of the e/i values for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'eoveri_hist_multi')
    fig = plt.figure()
    thisRange = [-1, 20]
    nBins = 21
    for m,dataDict in enumerate(dataDictList):
        eoveri = figures.get_all_by_key(dataDict, 'eoveri')
        n, binEdges, __ = plt.hist(eoveri, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    plt.xlabel(r'$e/i$')
    plt.ylabel(r'log(N)')
    plt.xlim(0, 20)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()
    #plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)

