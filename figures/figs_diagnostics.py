"""Plot diagnostic figures like the Geweke diagnostic.
"""
import matplotlib.pyplot as plt

def plot_geweke(zscores, plotPath, labels=None):
    fig = plt.figure(figsize=[8,20])
    for p, scores in enumerate(zscores):
        ax0 = plt.subplot(510+1+p)
        x, y = zip(*scores)
        plt.scatter(x,y)
        if labels is not None:
            plt.title(labels[p])
        if p == 2:
            plt.ylabel('z-score in units of sigma')
        plt.axhline(-2, ls='--')
        plt.axhline(2, ls='--')
        plt.ylim(-2.5, 2.5)
        plt.xlim(0)

    plt.xlabel('First Iteration')
    plt.tight_layout()
    fig.savefig(plotPath)
