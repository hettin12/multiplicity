"""
Rubbish at the moment.
"""
import os

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
from scipy.interpolate import interp1d
from scipy.stats import linregress

import figures

widths = [3, 2, 1]
#widths = [5, 3, 1]
colors = ['DodgerBlue', 'black', 'red']
errColors = ['RoyalBlue', 'DimGrey', 'Tomato']
MINOR_TICKS = [0.301, 0.4771, 0.6021, 0.69897, 0.77815, 0.8451, 0.9031, 0.95424,
               1.301, 1.4771, 1.6021, 1.69897, 1.77815, 1.8451, 1.9031, 1.95424,
               2.301, 2.4771, 2.6021, 2.69897, 2.77815, 2.8451, 2.9031, 2.95424,
               3.301, 3.4771, 3.6021, 3.69897, 3.77815, 3.8451, 3.9031, 3.95424,]
labels = ['Metal-Poor', 'Metal-Intermediate', 'Metal-Rich']


def plot_plate_fractions(plateCounts, plateFractions, limit, plotDir):
    """Plot histograms showing the number of binaryies per plate and
    the fraction of f-type stars that this represents."""
    plt.figure()
    plt.hist(plateCounts, 20, [0,40], log=True)
    plt.xlabel('Binaries on plate (%.2f cutoff).' % (limit))
    plt.ylabel('N')
    plotPath = os.path.join(plotDir, 'plateCount_hist.png')
    plt.savefig(plotPath)

    plt.figure()
    plt.hist(plateFractions, 20, [0,1], log=True)
    plt.xlabel('Fraction of F-type binaries per plate (%.2f cutoff).' % (limit))
    plt.ylabel('N')
    plotPath = os.path.join(plotDir, 'plateFraction_hist.png')
    plt.savefig(plotPath)

    plt.figure()
    plt.scatter(plateFractions, plateCounts)
    plt.ylabel('Binaries on plate')
    plt.xlabel('F-type binary fraction on plate')
    plt.ylim(0, 40)
    plt.xlim(0, 1)
    plotPath = os.path.join(plotDir, 'plate_scatter_count_fraction.png')
    plt.savefig(plotPath)


def plot_binary_fractions(dataDictionaryList, plotDir, cutoffList=[-100, -50, -25], normalize=False):
    """Plot binary fraction as function of metallicity group, for several different
    cutoff values."""
    key = 'isBinFrac'
    figures.rcParams['axes.labelsize'] = 12
    figures.rcParams['xtick.labelsize'] = 12
    figures.rcParams['ytick.labelsize'] = 13

    # Set up the directory and create the figure and left/right axes
    plotPathDict = figures.setup_plot_directory(plotDir, 'binary_fractions')
    fig = plt.figure(figsize=[8,4])
    ax1 = fig.gca()
    ax2 = ax1.twinx()

    # Set up the labels, colors, markers, and linestyles
    labels = [r'$\eta$ > %.2f' % x for x in cutoffList]
    colors = ['LimeGreen', 'Indigo', 'OrangeRed']
    linestyles = ['-', '--', ':', '-.', '-', '-', '--', ':', '-.', '-', '--', ':',]
    longdashes = [30,2,30,2]
    markers = ['s', 'D', 'o']
    rich_80_fraction = None    # used for ax2 labels

    # For each cutoff:
    for j, cutoff in enumerate(cutoffList):
        binaryFractions, binarySTDs, medianFEHs, minFEHs, maxFEHs = [], [], [], [], []
        # For each metallicity group:
        for m, dataDict in enumerate(dataDictionaryList):
            # Calculate the median metallicity and the range
            N = len(dataDict)
            feh = figures.get_all_by_key(dataDict, 'feh')
            med_feh = np.median(feh)
            medianFEHs.append(med_feh)
            print m, med_feh
            minFEHs.append(med_feh - min(feh))
            maxFEHs.append(max(feh) - med_feh)

            # Calculate binary fraction for this group
            binaryStars = []
            for pmf, data in dataDict.iteritems():
                if data[key] > cutoff:
                    binaryStars.append(pmf)
            B = len(binaryStars)
            binaryFractions.append(float(B) / float(N))
            binarySTDs.append(np.sqrt(float(B)) / float(N))            
            print cutoff, m, B, '/', N, '=', binaryFractions[m], '+/-', binarySTDs[m]

        # Normalize the metallicity groups to the metal-rich group
        if normalize:
            const = binaryFractions[-1]
            if cutoff == 0.80:
                rich_80_fraction = const
            binarySTDs = [bs / const for bs in binarySTDs]
            binaryFractions = [bf / const for bf in binaryFractions]

        # Plot the (relative) binary fractions
        xerrs = [minFEHs, maxFEHs]
        line, __, __ = ax1.errorbar(medianFEHs, binaryFractions, yerr=binarySTDs, capsize=7, color=colors[j], ls='none', elinewidth=3-j, zorder=(-10 + 2*j))
        ax1.plot(medianFEHs, binaryFractions, color=colors[j], label=labels[j], ls='none', marker=markers[j], ms=11-2*j, zorder=32)
        if j == 4 or j == 9:
            line.set_dashes(longdashes)

        # Calculate and plot the weighted linear regression for this cuttoff
        import statsmodels.api as sm
        x = np.linspace(-4, 1, 10)
        X = medianFEHs
        X = sm.add_constant(X)
        Y = binaryFractions
        W = [1. / sig / sig for sig in binarySTDs]  # i think this is right
        wls_model = sm.WLS(Y, X, weights=W)
        results = wls_model.fit()
        print results.params
        y = lambda x: results.params[1]*x + results.params[0]
        ax1.plot(x, y(x), color=colors[j], ls=linestyles[j], zorder=(-22+j))

    ax1.set_xlabel('[Fe/H]')
    ax1.set_xlim(-2.5, 0.5)
    ax1.set_ylim(0.45, 1.15)
    if normalize:
        ax1.set_ylabel(r'$f_b\ /\ f_b^{\mathrm{rich}}$')
    else:
        ax1.set_ylabel(r'$f_b$')
        ax1.set_ylim(0, 0.06)

    ax2.set_ylim(0.45, 1.15)
    if rich_80_fraction is not None:
        print rich_80_fraction
        ticks = ax2.get_yticks()
        newValues = [tick*rich_80_fraction for tick in ticks]
        newLabels = ['%.3f' % frac for frac in newValues]
        ax2.set_yticklabels(newLabels)
        ax2.set_ylabel(r'$f_b\ (\eta > 0.80)$')
        ax2.tick_params(axis='y', colors=colors[1])
        ax2.yaxis.label.set_color(colors[1])

    leg_handles, leg_labels = ax1.get_legend_handles_labels()
    ax1.legend(leg_handles[::-1], leg_labels[::-1], loc=4, prop={'size':13}, handlelength=1, numpoints=1, frameon=False)
    fig.savefig(plotPathDict['bigEPS'])
    fig.savefig(plotPathDict['bigPNG'])
    #figures.save_figures(fig, plotPathDict)


def plot_multiple_isBinary_histogram(dataDictList, plotDir):
    """Plot histograms of isBinaryFraction for all metallicity groups."""
    if len(dataDictList) == 1:
        return
    plotPathDict = figures.setup_plot_directory(plotDir, 'isBinary_hist_multi')

    fig = plt.figure()
    ax1 = fig.gca()

    #thisRange = [-0.02, 1.02]
    #nBins = 52
    thisRange = [-0.04, 1.04]
    nBins = 27

    for m,dataDict in enumerate(dataDictList):
        isBinaryFractions = figures.get_all_by_key(dataDict, 'isBinaryFraction') 

        c = [x > 0.5 for x in isBinaryFractions]
        print sum(c), 'binaries in %s' % labels[m]

        # Get histogram and plot of finite values
        n, binEdges, __ = plt.hist(isBinaryFractions, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log scale
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        ax1.plot(x, n, ls='steps-mid', label=labels[m], lw=widths[m], color=colors[m])

    ax1.axvline(0.65, color='OrangeRed', ls='-', zorder=-32, lw=0.5)
    ax1.axvline(0.80, color='Indigo', ls='-', zorder=-32, lw=0.5)
    ax1.axvline(0.95, color='LimeGreen', ls='-', zorder=-32, lw=0.5)
    ax1.set_xlabel(r'$\eta$')
    ax1.xaxis.label.set_fontsize(11)
    ax1.set_ylabel('log(N)')
    ax1.set_xlim(0, 1)
    ax1.set_ylim(-0.4, 4.0)
    leg = ax1.legend(loc=1, frameon=True, prop={'size':6}) # Adjust legend size here.
    leg.get_frame().set_linewidth(0.0)
    figures.save_figures(fig, plotPathDict)


def plot_multiple_bayes_histogram(dataDictList, plotDir):
    """Plot histograms of -2lnK for all metallicity groups."""
    if len(dataDictList) == 1:
        return
    plotPathDict = figures.setup_plot_directory(plotDir, 'bayesfactor_hist_multi')

    fig = plt.figure()
    gs = gridspec.GridSpec(1, 2, width_ratios=[12, 1])
    ax1 = plt.subplot(gs[0])
    ax2 = plt.subplot(gs[1], sharey=ax1)

    xmin = -1500
    thisRange = [xmin, 50]
    nBins = 40

    for m,dataDict in enumerate(dataDictList):
        lnK = figures.get_all_by_key(dataDict, '-2lnK')

        # Get histogram and plot of finite values
        n, binEdges, __ = plt.hist(lnK, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log scale
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        ax1.plot(x, n, ls='steps-mid', label=labels[m], lw=widths[m], color=colors[m])

        # Find the inf values
        idx = np.where(np.array(lnK) < xmin)[0]
        notPictured = len(idx)
        for i in idx:
            lnK[i] = -3100
        thisLabel = '%d Not Pictured' % notPictured

        # Plot inf values
        n, binEdges, __ = plt.hist(lnK, 3, range=[-3200, -3000], visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        ax2.plot(x, n, ls='steps-mid', lw=widths[m], color=colors[m])

    ax1.set_xlabel('-2lnK')
    ax1.set_ylabel('log(N)')
    ax1.set_xlim(50, xmin)
    ax1.set_ylim(-0.4, 4.0)
    # Set ticks for large figure
    #ax1.set_xticks([0, -200, -400, -600, -800, -1000, -1200, -1400, -1500])
    #ax1.set_xticklabels(['0', '-200', '-400', '-600', '-800', '-1000', '-1200', '-1400', '   '])
    # Set ticks for small figure
    ax1.set_xticks([0, -400, -800, -1200, -1500])
    ax1.set_xticklabels(['0', '-400', '-800', '-1200', '   '])
    ax1.set_yticks(MINOR_TICKS, minor=False)  # Set to TRUE for large figure.
    ax1.set_yticks([0, 1, 2, 3, 4])    
    ax1.set_yticklabels(['0', '1', '2', '3', '4'])
    ax1.legend(loc=1, frameon=False, prop={'size':8}) # Adjust legend size here.
    ax2.set_xlim(-2950, -3200)
    ax2.set_xticklabels([])
    ax2.set_xticks([])

    # hide the spines between ax and ax2
    ax1.spines['right'].set_visible(False)
    ax2.spines['left'].set_visible(False)
    ax1.yaxis.tick_left()
    ax1.tick_params(labeltop='off') # don't put tick labels at the top
    ax2.yaxis.tick_right()
    ax2.tick_params(labelright='off') # turn off tick labels at far right

    # Make the spacing between the two axes a bit smaller
    plt.subplots_adjust(wspace=0.4)

    # Add slash marks
    d = .03 # how big to make the diagonal lines in axes coordinates
    # arguments to pass plot, just so we don't keep repeating them
    kwargs = dict(transform=ax1.transAxes, color='k', clip_on=False)
    ax1.plot((1-.01, 1+.01), (-d, +d), **kwargs) # top-left diagonal
    ax1.plot((1-.01, 1+.01), (1-d, 1+d), **kwargs) # bottom-left diagonal
    kwargs.update(transform=ax2.transAxes) # switch to the bottom axes
    scale = 12
    offset = 0.01*scale
    ax2.plot((0-offset, 0+offset), (-d,+d), **kwargs) # top-right diagonal
    ax2.plot((0-offset, 0+offset), (1-d,1+d), **kwargs) # bottom-right diagonal

    figures.save_figures(fig, plotPathDict)


def plot_cumulative_bayes(dataDictList, plotDir):
    """Plot the bayes factor for each group in a cumulative form."""
    if len(dataDictList) == 1:
        return
    plotPathDict = figures.setup_plot_directory(plotDir, 'bayesfactor_cumulative_multi')
    xmin = -1500
    ymin = 1.0
    #plotPathDict = figures.setup_plot_directory(plotDir, 'bayesfactor_cumulative_multi_zoom')
    #xmin = -200
    #ymin = 10.0
    fig = plt.figure()

    for m,dataDict in enumerate(dataDictList):
        lnK = figures.get_all_by_key(dataDict, '-2lnK')
        idx = np.where(np.array(lnK) < xmin)[0]
        for i in idx:
            lnK[i] = xmin-20
        lnK.sort()

        x, y, z = [], [], []
        for i, val in enumerate(lnK):
            x.append(val)
            y.append(i+1)

            poisson = np.sqrt(i+1)
            z.append(poisson)

        x.append(120)
        y.append(len(lnK))
        z.append(0)
        errx = [np.mean([x[i], x[i+1]]) for i in range(len(x)-1)]
        plt.errorbar(errx, y[:-1], yerr=z[:-1], color=errColors[m], ls='none', capsize=0, zorder=2*m)
        plt.plot(x, y, ls='steps-post', label=labels[m], lw=widths[m], color=colors[m], zorder=2*m+1)

    plt.xlabel('-2lnK')
    plt.ylabel('Cumulative Number of Stars > -2lnK')
    plt.yscale('log')
    plt.ylim(ymin)
    plt.xlim(xmin,10)
    plt.legend(loc=2)
    figures.save_figures(fig, plotPathDict)


def plot_cumulative_bayes_xlog(dataDictList, plotDir):
    """Plot the bayes factor for each group in a cumulative form. Use log scale for x."""
    if len(dataDictList) == 1:
        return
    plotPathDict = figures.setup_plot_directory(plotDir, 'bayesfactor_cumulative_multi_log')
    xmin = 1550
    ymin = 1.0
    fig = plt.figure()

    for m,dataDict in enumerate(dataDictList):
        lnK = figures.get_all_by_key(dataDict, '-2lnK')
        lnK = [-1.*val + 50. for val in lnK]
        idx = np.where(np.array(lnK) > xmin)[0]
        for i in idx:
            lnK[i] = xmin+20
        lnK.sort(reverse=True)

        x, y, z = [], [], []
        for i, val in enumerate(lnK):
            x.append(val)
            y.append(i+1)

            poisson = np.sqrt(i+1)
            z.append(poisson)

        x.append(1.0)
        y.append(len(lnK))
        z.append(0)
        errx = [np.mean([x[i], x[i+1]]) for i in range(len(x)-1)]
        plt.errorbar(errx, y[:-1], yerr=z[:-1], color=errColors[m], ls='none', capsize=0, zorder=2*m)
        plt.plot(x, y, ls='steps-post', label=labels[m], lw=widths[m], color=colors[m], zorder=2*m+1)


    plt.xlabel('2lnK + 50')
    plt.ylabel('Cumulative Number of Stars > -2lnK')
    plt.yscale('log')
    plt.ylim(ymin)
    plt.xscale('log')
    plt.xlim(40.0, xmin)
    plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)


def get_cumulative_interpolation(dataDictList):
    """Return a set of x,y values for all three groups which
    has been interpolated from the data."""
    xmin = -1500
    x_new = np.linspace(-1500, 50, 1000)

    xyTuples = []
    for m,dataDict in enumerate(dataDictList):
        lnK = figures.get_all_by_key(dataDict, '-2lnK')
        idx = np.where(np.array(lnK) < xmin)[0]
        for i in idx:
            lnK[i] = xmin-100
        lnK.sort()

        x, y, z = [], [], []
        x.append(-3000)
        y.append(0)
        z.append(0)
        for i, val in enumerate(lnK):
            x.append(val)
            y.append(i+1)
            poisson = np.sqrt(i+1)
            z.append(poisson)
        x.append(120)
        y.append(len(lnK))
        z.append(0)

        errx = [np.mean([x[i], x[i+1]]) for i in range(len(x)-1)]

        #plt.errorbar(errx, y[:-1], yerr=z[:-1], color=errColors[m], ls='none', marker='o', ms=2, capsize=0, zorder=2*m)

        # Interpolate data.
        f = interp1d(x, y)
        y_new = f(x_new)
        xyTuples.append((x_new, y_new))
        #plt.plot(x_new, y_new, ls='-', label=labels[m], lw=widths[m], color=colors[m], zorder=2*m+1)

    return xyTuples


def plot_multiple_difference(dataDictList, plotDir):
    if len(dataDictList) == 1:
        return
    plotPathDict = figures.setup_plot_directory(plotDir, 'bayesfactor_diff_multi')
    xmin = -1500
    offset = 100

    fig = plt.figure()

    xyTuples = get_cumulative_interpolation(dataDictList)
    yIntermediate = np.array(xyTuples[1][1])
    thisZorder = [2, 1, 3]
    for m, tup in enumerate(xyTuples):
        x_new, y_new = tup
        x_new = np.array(x_new)
        y_new = np.array(y_new)
        z_new = np.sqrt(y_new)
        ratio = y_new / yIntermediate
        ratioErr = z_new / yIntermediate

        plt.fill_between(-1.*x_new+offset, ratio-ratioErr, ratio+ratioErr, color=colors[m], alpha=0.4, zorder=2*thisZorder[m])
        plt.plot(-1.*x_new+offset, ratio, ls='-', label=labels[m], lw=widths[m], color=colors[m], zorder=2*thisZorder[m]+1)

    plt.xlabel('2lnK + %d' % offset)
    plt.ylabel('Cumulative > -2lnK, Normalized')
    plt.xscale('log')
    plt.xlim(90,-1.*xmin+offset)
    plt.legend(loc=2)

    figures.save_figures(fig, plotPathDict)


def plot_binary_fractions_by_teff(dataDictionaryList, plotDir, cutoffList=[-100, -50, -25], normalize=True):
    """Plot binary fraction as function of temperature group, for several different
    cutoff values."""
    figures.rcParams['axes.labelsize'] = 12
    figures.rcParams['xtick.labelsize'] = 12
    figures.rcParams['ytick.labelsize'] = 12

    plotPathDict = figures.setup_plot_directory(plotDir, 'binary_fractions')
    labels = ['-2lnK < %s' % str(x) for x in cutoffList]
    colors = ['LimeGreen', 'Indigo', 'OrangeRed']
    linestyles = ['-', '--', ':', '-.', '-', '-', '--', ':', '-.', '-', '--', ':',]
    longdashes = [30,2,30,2]
    markers = ['s', 'D', 'o']
    fig = plt.figure(figsize=[8,4])

    for j, cutoff in enumerate(cutoffList):

        binaryFractions, binarySTDs, medianTEFFs, minTEFFs, maxTEFFs = [], [], [], [], []
        for m, dataDict in enumerate(dataDictionaryList):
            N = len(dataDict)
            teff = figures.get_all_by_key(dataDict, 'teff')
            med_teff = np.median(teff)
            medianTEFFs.append(med_teff)
            print m, med_teff
            minTEFFs.append(med_teff - min(teff))
            maxTEFFs.append(max(teff) - med_teff)

            # Calculate binary fraction
            binaryStars = []
            for pmf, data in dataDict.iteritems():
                if data['-2lnK'] < cutoff:
                    binaryStars.append(pmf)
            B = len(binaryStars)
            binaryFractions.append(float(B) / float(N))
            binarySTDs.append(np.sqrt(float(B)) / float(N))            
            print cutoff, m, B, '/', N, '=', binaryFractions[m], '+/-', binarySTDs[m]

        if normalize:
            const = binaryFractions[0]
            binarySTDs = [bs / const for bs in binarySTDs]
            binaryFractions = [bf / const for bf in binaryFractions]

        # Plot points
        xerrs = [minTEFFs, maxTEFFs]
        line, __, __ = plt.errorbar(medianTEFFs, binaryFractions, yerr=binarySTDs, color=colors[j], label=labels[j], ls='none', marker=markers[j], ms=12-2*j)
        if j == 4 or j == 9:
            line.set_dashes(longdashes)

        # Weighted linear regression
        import statsmodels.api as sm
        x = np.linspace(5500, 7000, 10)
        X = medianTEFFs
        X = sm.add_constant(X)
        Y = binaryFractions
        W = [1. / sig / sig for sig in binarySTDs]  # i think this is right
        wls_model = sm.WLS(Y, X, weights=W)
        results = wls_model.fit()
        print results.params
        y = lambda x: results.params[1]*x + results.params[0]
        plt.plot(x, y(x), color=colors[j], ls=linestyles[j], zorder=-1*j-2)

    plt.xlabel(r'$T_\mathrm{eff}$')
    plt.xlim(5500, 7000)
    plt.ylim(0.4, 1.2)
    if normalize:
        plt.ylabel(r'$f_b/f_b^{\mathrm{hot}}$')
    else:
        plt.ylabel(r'$f_b$')
        plt.ylim(1E-3, 1.0)
        plt.yscale('log')
    leg_handles, leg_labels = fig.gca().get_legend_handles_labels()
    plt.legend(leg_handles[::-1], leg_labels[::-1], loc=4, prop={'size':11}, handlelength=1, numpoints=1, frameon=False)
    fig.savefig(plotPathDict['bigPNG'])
