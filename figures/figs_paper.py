"""
Figures for the first paper.  Many with subplots.
"""
import matplotlib.pyplot as plt
import numpy as np

import figures
from multiplicity.radialvelocity import absoluterv

widths = [5, 3, 1]
colors = ['DodgerBlue', 'black', 'red']
labels = ['Metal-Poor', 'Metal-Intermediate', 'Metal-Rich']
markers = ['o', 's', 'D']

GOLDEN_RATIO = 1.61803
BIG_FIG_SIZE_DOUBLE = (7.0, 7.0/GOLDEN_RATIO/1.8)
MINOR_TICKS = [0.301, 0.4771, 0.6021, 0.69897, 0.77815, 0.8451, 0.9031, 0.95424,
               1.301, 1.4771, 1.6021, 1.69897, 1.77815, 1.8451, 1.9031, 1.95424,
               2.301, 2.4771, 2.6021, 2.69897, 2.77815, 2.8451, 2.9031, 2.95424,
               3.301, 3.4771, 3.6021, 3.69897, 3.77815, 3.8451, 3.9031, 3.95424,]


def feh_plus_deltaT(dataDictList, plotDir):
    """Plot metallicity histogram for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'feh_plus_deltaT')
    fig = plt.figure()
    fig.set_size_inches(BIG_FIG_SIZE_DOUBLE)
    ax1 = plt.subplot(121)
    plot_feh(ax1, dataDictList)
    ax2 = plt.subplot(122)
    plot_baseline(ax2, dataDictList, asPercent=False)
    fig.tight_layout(pad=0.1)
    plt.subplots_adjust(wspace=0.15)
    fig.savefig(plotPathDict['bigEPS'])
    fig.savefig(plotPathDict['bigPNG'])


def meanRV_plus_stdRV(dataDictList, plotDir):
    """Plot average RV measurement histogram and std(RV measurement) histogram
    for all groups in the list as a two subplot combo."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'meanRV_plus_stdRV')
    fig = plt.figure()
    fig.set_size_inches(BIG_FIG_SIZE_DOUBLE)
    ax1 = plt.subplot(121)
    plot_meanRV(ax1, dataDictList)
    ax2 = plt.subplot(122)
    plot_stdRV(ax2, dataDictList)
    fig.tight_layout(pad=0.1)
    plt.subplots_adjust(wspace=0.15)
    fig.savefig(plotPathDict['bigEPS'])
    fig.savefig(plotPathDict['bigPNG'])


def plot_meanRV(ax, dataDictList):
    """Plot histogram of weighted mean(RV) for stars."""
    thisRange = [-620, 620]
    nBins = 30
    for m,dataDict in enumerate(dataDictList):
        rvLists = figures.get_all_by_key(dataDict, 'absRVs')
        errLists = figures.get_all_by_key(dataDict, 'empiricalErrs')
        meanRV = [absoluterv.calc_weighted_mean(rvs, errors) for rvs,errors in zip(rvLists, errLists)]
        n, binEdges, __ = plt.hist(meanRV, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        ax.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    ax.set_xlabel(r'$\overline{\rm{RV}}$ (km/s)')
    ax.set_ylabel(r'log(N)')
    ax.set_xlim(-600, 600)
    ax.set_ylim(-0.4, 4)
    ax.set_yticks(MINOR_TICKS, minor=True)
    ax.set_yticks([0, 1, 2, 3, 4])    
    ax.set_yticklabels(['0', '1', '2', '3', '4'])


def plot_stdRV(ax, dataDictList):
    """Plot histogram of std(RV) for stars."""
    thisRange = [-2, 58]
    nBins = 30
    for m,dataDict in enumerate(dataDictList):
        rvLists = figures.get_all_by_key(dataDict, 'absRVs')
        errLists = figures.get_all_by_key(dataDict, 'empiricalErrs')
        stdRV = [absoluterv.calc_weighted_std(rvs, errors) for rvs,errors in zip(rvLists, errLists)]
        n, binEdges, __ = plt.hist(stdRV, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = labels[m]
        ax.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    ax.set_xlabel(r'std(RV) (km/s)')
    #ax.set_ylabel(r'log(N)')
    ax.set_xlim(0)
    ax.set_ylim(-0.4, 4)
    ax.set_yticks(MINOR_TICKS, minor=True)
    ax.set_yticks([0, 1, 2, 3, 4])    
    ax.set_yticklabels(['0', '1', '2', '3', '4'])
    ax.legend(loc=1, prop={'size':7}, frameon=False)

def plot_feh(ax, dataDictList):
    """Fe/H histogram."""
    thisRange = [-4, 1]
    nBins = 30
    for m,dataDict in enumerate(dataDictList):
        feh = figures.get_all_by_key(dataDict, 'feh')
        n, binEdges, __ = plt.hist(feh, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = labels[m]
        ax.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    ax.set_xlabel(r'[Fe/H]')
    ax.set_ylabel(r'log(N)')
    ax.set_xlim(-4.0,1.0)
    ax.set_ylim(-0.4, 4)
    ax.set_yticks(MINOR_TICKS, minor=True)
    ax.set_yticks([0, 1, 2, 3, 4])    
    ax.set_yticklabels(['0', '1', '2', '3', '4'])
    #ax.minorticks_off()
    ax.legend(loc=2, prop={'size':7}, frameon=False)


def plot_baseline(ax, dataDictList, asPercent=False):
    """Plot histogram of finalTime = initialTime for all groups in the list."""
    thisRange = [-1, 9]
    nBins = 25
    for m,dataDict in enumerate(dataDictList):
        taimids = figures.get_all_by_key(dataDict, 'taimids')
        baseline = [max(t) - min(t) for t in taimids]
        logBaseline = [np.log10(b) for b in baseline]
        n, binEdges, __ = plt.hist(logBaseline, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = labels[m]
        ax.plot(x, n, label=thisLabel, lw=widths[m], color=colors[m], drawstyle='steps-mid')
    ax.axvline(np.log10(86400), ls='--', lw=1, color='k')     # 1 day
    ax.annotate('1d', (np.log10(120000), 3.4))
    ax.axvline(np.log10(2.63E6), ls='--', lw=1, color='k')    # 1 month
    ax.annotate('1m', (np.log10(4.0E6), 3.4))
    ax.axvline(np.log10(3.16E7), ls='--', lw=1, color='k')    # 1 year
    ax.annotate('1y', (np.log10(5.0E7), 3.4))
    ax.set_xlabel(r'$\Delta t$ (log s)')
    #ax.set_ylabel(r'log(N)')
    ax.set_xlim(2.5, 9)
    ax.set_ylim(-0.4, 4)
    ax.set_yticks(MINOR_TICKS, minor=True)
    ax.set_yticks([0, 1, 2, 3, 4])    
    ax.set_yticklabels(['0', '1', '2', '3', '4'])
    #ax.minorticks_off()
    #ax.legend(loc=1)


def best_periods(dataDictList, plotDir, bayesThresh=None, plotLogA=False):
    """Plot the best periods as a histogram for each metallicity group."""
    if bayesThresh is not None:
        if plotLogA:
            plotPathDict = figures.setup_plot_directory(plotDir, '2lnK%d_amplitude_distr' % int(-1*bayesThresh))
        else:
            plotPathDict = figures.setup_plot_directory(plotDir, '2lnK%d_period_distr' % int(-1*bayesThresh))
    else:
        if plotLogA:
            plotPathDict = figures.setup_plot_directory(plotDir, 'amplitude_distr')
        else:
            plotPathDict = figures.setup_plot_directory(plotDir, 'period_distr')

    fig = plt.figure()
    ax = fig.gca()

    ymax = 0
    for m,dataDict in enumerate(dataDictList):
        if plotLogA:
            vals = figures.get_all_by_key(dataDict, 'logA')
            thisRange = [0,2.6]
            nBins = 30
        else:
            vals = figures.get_all_by_key(dataDict, 'logP')
            thisRange = [0,10]
            nBins = 60

        n, binEdges, __ = ax.hist(vals, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        ax.plot(x, n, ls='steps-mid', label=labels[m], lw=widths[m], color=colors[m])
        ymax = max([ymax, max(n)*1.2])


    if plotLogA:
        ax.set_xlim(0.2, 2.5)
        ax.set_xlabel(r'logA (km/s)')
    else:
        ax.fill_between([3.3, 4.56], 0, ymax, facecolor='yellow', linewidth=0, zorder=-1)   # logP of 4.56 is 10 hours
        ax.axvline(6.0157, ls='--', color='k', zorder=-32) # circulation limit of 12 days
        ax.set_xlim(3.8,7.2)
        ax.set_xlabel(r'logP (s)')

    ax.set_ylabel(r'N')
    ax.set_ylim(1, ymax)
    leg = ax.legend(loc=1, prop={'size':7})
    leg.get_frame().set_linewidth(0.0)
    figures.save_figures(fig, plotPathDict)



def isBinary_periods(dataDictList, plotDir):
    """Plot the total histogram of the binary period posteriors for each metallicity group."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'isBinary_period_posteriors')

    fig = plt.figure()
    ax = fig.gca()

    ymax = 0
    for m, dataDict in enumerate(dataDictList):
        logPtraces = figures.get_all_by_key(dataDict, 'logPtrace')
        vals = []
        for trace in logPtraces:
            vals.extend(trace)
        thisRange = [0,10]
        nBins = 30

        n, binEdges, __ = ax.hist(vals, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        # Normalize
        n = n / 1000.
        n = n / len(dataDict)
        print sum(n)
        ax.plot(x, n, ls='steps-mid', label=labels[m], lw=widths[m], color=colors[m])
        ymax = max([ymax, max(n)*1.2])

    ax.fill_between([3.3, 4.56], 0, ymax, facecolor='lightgoldenrodyellow', linewidth=0, zorder=-1)   # logP of 4.56 is 10 hours
    ax.axvline(6.0157, ls='--', color='k', lw=1, zorder=-32)
    ax.set_xlim(3.8,7.2)
    ax.set_xlabel(r'logP (s)')

    ax.set_ylabel(r'Combined PDF')
    ax.set_ylim(0, ymax)
    leg = ax.legend(loc=1, prop={'size':7})
    leg.get_frame().set_linewidth(0.0)
    figures.save_figures(fig, plotPathDict)


def logA_vs_logP(dataDictList, plotDir, bayesThresh=None):
    """Plot the best periods as a histogram for each metallicity group."""
    if bayesThresh is not None:
        plotPathDict = figures.setup_plot_directory(plotDir, '2lnK%d_logA_vs_logp' % int(-1*bayesThresh))
    else:
        plotPathDict = figures.setup_plot_directory(plotDir, 'logA_vs_logp')

    fig = plt.figure()
    ax = fig.gca()

    for m,dataDict in enumerate(dataDictList):
        logA = figures.get_all_by_key(dataDict, 'logA')
        logP = figures.get_all_by_key(dataDict, 'logP')
        ax.scatter(logP, logA, c=colors[m], alpha=0.5, s=6, marker=markers[m], linewidths=0)

    ax.set_xlim(3.9, 7.25)
    ax.set_ylim(0.4, 2.5)
    ax.set_xlabel(r'logP (s)')
    ax.set_ylabel(r'logA (km/s)')

    figures.save_figures(fig, plotPathDict)
