"""
Histograms displaying exposure information for the entire sample.
"""
import matplotlib.pyplot as plt
import numpy as np

import figures

def plot_meanSNR_hist(dataDict, plotDir):
    """Plot average signal-to-noise histogram for all fibers in the dataDict."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'meanSNR_hist')
    snrLists = figures.get_all_by_key(dataDict, 'exposureSNR')
    meanSNR = [np.mean(snr) for snr in snrLists]
    fig = plt.figure()
    plt.hist(meanSNR, 20, log=True)
    plt.xlabel(r'$\overline{\rm{SNR}}$')
    plt.ylabel(r'N')
    plt.xlim(15, 100)
    plt.ylim(0.3, 1E4)
    plt.minorticks_off()
    figures.save_figures(fig, plotPathDict)


def plot_stdSNR_hist(dataDict, plotDir):
    """Plot std(signal-to-noise) histogram for all fibers in the dataDict."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'stdSNR_hist')
    snrLists = figures.get_all_by_key(dataDict, 'exposureSNR')
    stdSNR = [np.std(snr) for snr in snrLists]
    fig = plt.figure()
    plt.hist(stdSNR, 20, log=True)
    plt.xlabel(r'std(SNR)')
    plt.ylabel(r'N')
    plt.xlim(0, 11)
    plt.ylim(0.3, 1E4)
    plt.minorticks_off()
    figures.save_figures(fig, plotPathDict)


def plot_baseline_hist(dataDict, plotDir, asPercent=False):
    """Plot histogram of finalTime = initialTime for all fibers in the dataDict."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'baseline_hist')
    taimids = figures.get_all_by_key(dataDict, 'taimids')
    baseline = [max(t) - min(t) for t in taimids]
    logBaseline = [np.log10(b) for b in baseline]
    fig = plt.figure()
    plt.hist(logBaseline, 25, log=True)
    plt.axvline(np.log10(86400), ls='--', lw=1, color='k')     # 1 day
    plt.annotate('1d', (np.log10(86400),2.5E3))
    plt.axvline(np.log10(2.63E6), ls='--', lw=1, color='k')    # 1 month
    plt.annotate('1m', (np.log10(2.63E6),2.5E3))
    plt.axvline(np.log10(3.16E7), ls='--', lw=1, color='k')    # 1 year
    plt.annotate('1y', (np.log10(3.16E7),2.5E3))
    plt.xlabel(r'Time range of RV observations in log(s)')
    plt.ylabel(r'N')
    plt.xlim(2, 9)
    plt.ylim(0.3, 1E4)
    plt.minorticks_off()
    figures.save_figures(fig, plotPathDict)


def plot_nExp_hist(dataDict, plotDir, asPercent=False):
    """Plot exposure count histogram for all fibers in the dataDict."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'nExp_hist')
    exposureCount = figures.get_all_by_key(dataDict, 'exposureCount')
    fig = plt.figure()
    plt.hist(exposureCount, 20, range=[0,60], log=True)#, align='left')
    plt.xlabel(r'Exposure Count')
    plt.ylabel(r'N')
    plt.xticks([3, 9, 15, 21, 27, 33, 39, 45, 51, 57])
    plt.xlim(0, 62)
    plt.ylim(0.3)
    plt.minorticks_off()
    figures.save_figures(fig, plotPathDict)
