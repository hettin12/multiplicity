"""
Histograms displaying stellar parameters for the entire sample.
"""
import matplotlib.pyplot as plt

import figures

def plot_feh_hist(dataDict, plotDir, vlines=None):
    """Plot metallicity histogram for all fibers in the dataDict."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'feh_hist')
    feh = figures.get_all_by_key(dataDict, 'feh')
    fig = plt.figure()
    plt.hist(feh, 20, log=True)
    if vlines is not None:
        for x in vlines:
            plt.axvline(x, ls='--', color='k')
    plt.xlabel(r'[Fe/H]')
    plt.ylabel(r'N')
    plt.xlim(-4.0,1.0)
    plt.ylim(0.3, 1E4)
    #plt.minorticks_off()
    figures.save_figures(fig, plotPathDict)


def plot_teff_hist(dataDict, plotDir):
    """Plot temperature histogram for all fibers in the dataDict."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'teff_hist')
    teff = figures.get_all_by_key(dataDict, 'teff')
    fig = plt.figure()
    plt.hist(teff, 20, log=True)
    plt.xlabel(r'$T_{\rm{eff}}$ (K)')
    plt.ylabel(r'N')
    plt.xlim(4500,8500)
    plt.ylim(0.3, 1E4)
    plt.xticks([5000, 6000, 7000, 8000])
    plt.minorticks_off()
    figures.save_figures(fig, plotPathDict)


def plot_logg_hist(dataDict, plotDir):
    """Plot gravity histogram for all fibers in the dataDict."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'logg_hist')
    logg = figures.get_all_by_key(dataDict, 'logg')
    fig = plt.figure()
    plt.hist(logg, 20, log=True)
    plt.xlabel(r'$\log{g}$')
    plt.ylabel(r'N')
    plt.xlim(3.7, 4.7)
    plt.ylim(0.3, 1E4)
    plt.minorticks_off()
    figures.save_figures(fig, plotPathDict)


def plot_gmag_hist(dataDict, plotDir):
    """Plot g-magnitude histogram for all fibers in the dataDict."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'gmag_hist')
    gmag = figures.get_all_by_key(dataDict, 'gmag')
    fig = plt.figure()
    plt.hist(gmag, 20, log=True)
    plt.xlabel(r'g Magnitude')
    plt.ylabel(r'N')
    plt.xlim(50,-50)
    plt.ylim(0.3, 1E4)
    plt.minorticks_off()
    figures.save_figures(fig, plotPathDict)
