#!/usr/bin/env python
"""
Plot RV Patterns for a series of fibers from the same plate.
"""

import os

import matplotlib.pyplot as plt
import numpy as np

from multiplicity.radialvelocity import rvinout


def get_master_taimids(fiberlist, dataDict):
    """Get the set of all taimids for all fibers in the dictionary.
    Return a sorted list of all taimids used in these fibers."""
    masterTaimids = []
    for pmf in fiberlist:
        taimids = dataDict[pmf]['taimids']
        for t in taimids:
            if t not in masterTaimids:
                masterTaimids.append(t)
    masterTaimids.sort()
    return masterTaimids


def add_subplot(masterTaimids, fiberDataDict, plotNum, offsets=None):
    """Add this fiber to the figure, using masterTaimids
    and any RV data."""
    # Get exposure info for this fiber
    taimids = fiberDataDict['taimids']
    rvs = fiberDataDict['absRVs']
    errs = fiberDataDict['empiricalErrs']
    cleanIDX = fiberDataDict['cleanExposureIndex']
    yz = zip(rvs, errs)
    xyz, good_xyz = {}, {}
    for i, t in enumerate(taimids):
        xyz[t] = yz[i]
        if len(cleanIDX):
            if i in cleanIDX:
                good_xyz[t] = yz[i]

    # Create data points
    x = range(len(masterTaimids))
    good_x = range(len(masterTaimids))
    y, z, good_y, good_z = [], [], [], []
    for t in masterTaimids:
        if t in xyz:
            if offsets is not None:
                y.append(xyz[t][0] - offsets[t])
            else:
                y.append(xyz[t][0])
            z.append(xyz[t][1])
        else:
            y.append(None)
            z.append(None)
        if t in good_xyz:
            if offsets is not None:
                good_y.append(xyz[t][0] - offsets[t])
            else:
                good_y.append(xyz[t][0])
            good_z.append(xyz[t][1])
        else:
            good_y.append(None)
            good_z.append(None)

    # Create error bar values
    errorTuples = [(a,b,c) for a,b,c in zip(x,y,z) if c is not None]
    ex, ey, ez = zip(*errorTuples)
    if len(cleanIDX):
        good_errorTuples = [(a,b,c) for a,b,c in zip(good_x,good_y,good_z) if c is not None]
        good_ex, good_ey, good_ez = zip(*good_errorTuples)

    # Plot
    plt.subplot(*plotNum)
    plt.plot(x, y, 'o', color='red')
    plt.errorbar(ex, ey, yerr=ez, color='red', ls='None')
    if len(cleanIDX):
        plt.plot(good_x, good_y, 'o', color='DodgerBlue')
        plt.errorbar(good_ex, good_ey, yerr=good_ez, color='DodgerBlue', ls='None')
    plt.xlim(-2, x[-1]+2)

    # Calculate y-limits
    max_y_val = max([val + ez[i] for i,val in enumerate(y)]) 
    min_y_val = min([val - ez[i] for i,val in enumerate(y)]) 
    yRange = max_y_val - min_y_val
    yMin = np.mean([val for val in y if val is not None]) - (yRange/1.2)
    yMax = np.mean([val for val in y if val is not None]) + (yRange/1.2)

    plt.ylim(yMin, yMax)
    plt.xticks([])
    plt.xlabel(fiberDataDict['fiberid'])


def plot_plate_with_dataDict(dataDict, pm, plotDir, offsets=None):
    """Make a plot of this plate showing the RV changes as a function of
    exposure (not time).  Plot all fibers from dataDict that have a specific plate-mjd value."""
    # Determine which fibers to plot, based on presence in dataDict
    fiberlist = []
    for fiberid in dataDict:
        thisPM = fiberid[:10]
        if thisPM == pm:
            fiberlist.append(fiberid)
    if not len(fiberlist):
        return
    if len(fiberlist) == 1:
        print pm
        return
    fiberlist.sort()

    # Get union of taimids, and y-range
    masterTaimids = get_master_taimids(fiberlist, dataDict)

    # Plot all fibers
    fig = plt.figure(figsize=[2+len(masterTaimids)/4., len(fiberlist)*2.])
    for i, pmf in enumerate(fiberlist):
        plotNum = (len(fiberlist), 1, i+1)
        add_subplot(masterTaimids, dataDict[pmf], plotNum, offsets=offsets)
    fig.tight_layout()
    if offsets is not None:
        plotPath = os.path.join(plotDir, '%s_with_shifts.png' % pm)
    else:
        plotPath = os.path.join(plotDir, '%s.png' % pm)
    fig.savefig(plotPath)
    del fig


def plot_plate(dbPath, pm, plotDir, offsets=None):
    """Read in dictionary and plot the plate."""
    # Read in RV data
    dataDict = rvinout.load_data(dbPath, silent=True, returnAsDict=True, cleanExposuresOnly=False)
    plot_plate_with_dataDict(dataDict, pm, plotDir, offsets=offsets)

