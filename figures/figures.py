"""
Code for making figures for a paper.  All of the plotting commands are here.
Plots are produced in various sizes and as both eps and png.

Keep in mind: 
    - alpha channels do not work with EPS.
    - default text sizes are usually too small
    - place legend in appropriate place (appropriate size)
    - order items in the legend according to how they appear in the plot
    - default line thickness is usually too small
    - default color scheme is usually crap
    - avoid red/green colorblind issues
    - think about what it will look like printed greyscale
    - use symbols in addition to colors where appropriate (greyscale)
    - adjust the amount of axes ticks / tick labels if necessary

"""
import os

from matplotlib import rcParams
rcParams['font.family'] = 'serif'
rcParams['font.size'] = 9
rcParams['legend.fontsize'] = 9
rcParams['axes.labelsize'] = 8
rcParams['axes.linewidth'] = 1.0
rcParams['xtick.labelsize'] = 8
rcParams['ytick.labelsize'] = 8
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.major.width'] = 1.0
rcParams['ytick.major.width'] = 1.0
rcParams['xtick.minor.size'] = 3
rcParams['ytick.minor.size'] = 3
rcParams['xtick.minor.width'] = 0.5
rcParams['ytick.minor.width'] = 0.5

import matplotlib.pyplot as plt
import numpy as np

import triangle

from multiplicity.radialvelocity import absoluterv
from multiplicity.figures import figs_stellar_all, figs_exposure_all, figs_measurement_all, figs_scatter_all
from multiplicity.figures import figs_exposure_multi, figs_measurement_multi, figs_stellar_multi
from multiplicity.figures import figs_paper

GOLDEN_RATIO = 1.61803
SMALL_FIG_SIZE = (3.0, 3.0/GOLDEN_RATIO)
BIG_FIG_SIZE = (7.0, 7.0/GOLDEN_RATIO)


def setup_plot_directory(basePlotDir, plotName):
    """For the variable name given, make a diretory and calculate the 
    names/paths to write images to.  Return a dictionary of paths (one
    per plot)."""
    if not os.path.isdir(basePlotDir):
        os.mkdir(basePlotDir)
    thisPlotDir = os.path.join(basePlotDir, plotName)
    if not os.path.isdir(thisPlotDir):
        os.mkdir(thisPlotDir)
    smallPNGPath = os.path.join(thisPlotDir, '%s_small.png' % plotName)
    bigPNGPath = os.path.join(thisPlotDir, '%s_big.png' % plotName)
    smallEPSPath = os.path.join(thisPlotDir, '%s_small.eps' % plotName)
    bigEPSPath = os.path.join(thisPlotDir, '%s_big.eps' % plotName)
    return {'smallPNG':smallPNGPath, 'bigPNG':bigPNGPath, 'smallEPS':smallEPSPath, 'bigEPS':bigEPSPath}


def save_figures(fig, plotPathDict):
    """Save the figure four times (eps and png in both big and small formats)."""
    fig.set_size_inches(SMALL_FIG_SIZE)
    fig.tight_layout(pad=0.1)
    fig.savefig(plotPathDict['smallEPS'])
    fig.savefig(plotPathDict['smallPNG'])
    fig.set_size_inches(BIG_FIG_SIZE)
    fig.tight_layout(pad=0.1)
    fig.savefig(plotPathDict['bigEPS'])
    fig.savefig(plotPathDict['bigPNG'])


def get_all_by_key(dataDict, key):
    """Return all values in the dictionary for a single
    key.  Return the values as a list."""
    return [data[key] for pmf,data in dataDict.iteritems()]


def plot_all_stellar_params(dataDictList, plotDir, vlines=None):
    """Call all functions for plotting each of the stellar parameters
    including FeH, logg, Teff, gmag."""
    if len(dataDictList) == 1:
        dataDict = dataDictList[0]
        figs_stellar_all.plot_feh_hist(dataDict, plotDir, vlines=vlines)
        figs_stellar_all.plot_teff_hist(dataDict, plotDir)
        figs_stellar_all.plot_logg_hist(dataDict, plotDir)
        figs_stellar_all.plot_gmag_hist(dataDict, plotDir)
    else:
        figs_stellar_multi.plot_multiple_feh_hist(dataDictList, plotDir)
        figs_stellar_multi.plot_multiple_teff_hist(dataDictList, plotDir)
        figs_stellar_multi.plot_multiple_logg_hist(dataDictList, plotDir)
        figs_stellar_multi.plot_multiple_gmag_hist(dataDictList, plotDir)


def plot_all_exposure_info(dataDictList, plotDir):
    """Call all functions for plotting each of the exposure parameters
    including mean(SNR), std(SNR), (t_f - t_i), nExp."""
    if len(dataDictList) == 1:
        dataDict = dataDictList[0]
        figs_exposure_all.plot_meanSNR_hist(dataDict, plotDir)
        figs_exposure_all.plot_stdSNR_hist(dataDict, plotDir)
        figs_exposure_all.plot_baseline_hist(dataDict, plotDir)
        figs_exposure_all.plot_nExp_hist(dataDict, plotDir)
    else:
        figs_exposure_multi.plot_multiple_meanSNR_hist(dataDictList, plotDir)
        figs_exposure_multi.plot_multiple_stdSNR_hist(dataDictList, plotDir)
        figs_exposure_multi.plot_multiple_baseline_hist(dataDictList, plotDir)
        figs_exposure_multi.plot_multiple_nExp_hist(dataDictList, plotDir)


def plot_all_measurement_info(dataDictList, plotDir):
    """Call all functions for plotting each of the exposure parameters
    including mean(RV), std(RV), mean(err), std(err)."""
    if len(dataDictList) == 1:
        dataDict = dataDictList[0]
        figs_measurement_all.plot_meanRV_hist(dataDict, plotDir)
        figs_measurement_all.plot_stdRV_hist(dataDict, plotDir)
        figs_measurement_all.plot_meanErr_hist(dataDict, plotDir)
        figs_measurement_all.plot_stdErr_hist(dataDict, plotDir)
        figs_measurement_all.plot_eoveri_hist(dataDict, plotDir)
    else:
        figs_measurement_multi.plot_multiple_meanRV_hist(dataDictList, plotDir)
        figs_measurement_multi.plot_multiple_stdRV_hist(dataDictList, plotDir)
        figs_measurement_multi.plot_multiple_RV_combo(dataDictList, plotDir)
        figs_measurement_multi.plot_multiple_meanErr_hist(dataDictList, plotDir)
        figs_measurement_multi.plot_multiple_stdErr_hist(dataDictList, plotDir)
        figs_measurement_multi.plot_multiple_eoveri_hist(dataDictList, plotDir)


def plot_all_scatter(dataDictList, plotDir):
    """Call all functions for plotting each of the scatter plots
    including (e/i vs FeH), (nExp vs FeH)."""
    if len(dataDictList) == 1:
        dataDict = dataDictList[0]
        figs_scatter_all.plot_eoveri_vs_feh_scatter(dataDict, plotDir)
        figs_scatter_all.plot_nExp_vs_feh_scatter(dataDict, plotDir)
    else:
        pass


def plot_paper_figs(dataDictList, plotDir):
    """Plot figures used in paper 1, including figures
    with subplots."""
    if len(dataDictList) == 1:
        pass
    else:
        figs_paper.feh_plus_deltaT(dataDictList, plotDir)
        figs_paper.meanRV_plus_stdRV(dataDictList, plotDir)


def plot_best_periods(dataDictList, plotDir, bayesThresh=None, plotLogA=False):
    """Plot period from the most likely traces."""
    if len(dataDictList) == 1:
        pass
    else:
        figs_paper.logA_vs_logP(dataDictList, plotDir, bayesThresh)
        figs_paper.best_periods(dataDictList, plotDir, bayesThresh, plotLogA=plotLogA)


def plot_isBinary_periods(dataDictList, plotDir):
    """Plot periods for all binaries as one total posterior summation."""
    figs_paper.isBinary_periods(dataDictList, plotDir)


def plot_triangle(dataDict, plotDir):
    """Make a triangle plot of all variables (scatter for each
    y vs x and a histogram for each x)."""
    plotPathDict = setup_plot_directory(plotDir, 'param_triangle')
    labels = ['feh', 'logg', 'teff', 'eoveri', 'exposureCount']
    #extents = [[], [], []]
    dataList = []
    for param in labels:
        theseData = get_all_by_key(dataDict, param)
        dataList.append(theseData)
    transposed = map(list, zip(*dataList))
    fig = triangle.corner(transposed, labels=labels) #, extents=extents)
    fig.savefig(plotPathDict['bigPNG'])
    fig.savefig(plotPathDict['bigEPS'])


### Contour Plots (if any) ###
#xlim, ylim = [-3,0.5], [3,20]
#H, xedges, yedges = np.histogram2d(y, x, range=[ylim, xlim], bins=20)
#plt.contour(H, extent=[yedges[0], yedges[-1], xedges[0], xedges[-1]], cmap=plt.cm.hot, linewidths=2)


### e/i Analysis ###
def plot_eoveri_by_i(dataDict, plotDir, err, errThresh):
    """Plot hisogram of the e/i values for all fibers with average err +/- 
    errThresh.  Plot the histograms, with fibers equally split by metallicity."""
    plotPathDict = setup_plot_directory(plotDir, 'eoveri_err%.1f' % err)

    dataTupleList = []
    for pmf, data in dataDict.iteritems():
        thisI = absoluterv.calc_i(data['empiricalErrs'])
        if abs(err - thisI) <= errThresh:
            dataTupleList.append((pmf, data['eoveri'], data['feh']))


    # Check if enough fibers
    eoveri = [tup[1] for tup in dataTupleList]
    if len(eoveri) < 3:
        return

    # Split fibers into three metallicity groups
    dataTupleList.sort(key=lambda t:t[2])
    N = len(dataTupleList) / 3
    group1 = dataTupleList[:N]
    group2 = dataTupleList[N:2*N]
    group3 = dataTupleList[2*N:]
    feh1 = group1[0][2]
    feh2 = group2[0][2]
    feh3 = group3[0][2]

    # Make the figures
    fig = plt.figure()
    thisRange = [-1, 22]
    nBins = 23
    groups = [group1, group2, group3]
    fehs = [feh1, feh2, feh3]
    colors = ['DodgerBlue', 'red', 'LimeGreen']
    widths = [8, 5, 3]
    for m in range(3):
        # Histogram
        n, binEdges, __ = plt.hist([tup[1] for tup in groups[m]], nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = '[Fe/H] > %.1f' % fehs[m]
        plt.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    plt.xlabel(r'std(RV) / $\overline{\sigma}$')
    plt.ylabel(r'log(N)')
    plt.xlim(0)
    plt.ylim(-0.5, 4.0)
    plt.minorticks_off()
    plt.legend(loc=1)
    save_figures(fig, plotPathDict)


def plot_cumulative_eoveri(dataDict, plotDir, err, errThresh, feh_cut_1, feh_cut_2):
    """Plot a cumulative fraction of stars by decreasing e/i."""
    plotPathDict = setup_plot_directory(plotDir, 'eoveri-cumulative_err%.1f' % err)

    tupleList = []
    for pmf, data in dataDict.iteritems():
        thisI = absoluterv.calc_i(data['empiricalErrs'])
        tupleList.append((data['eoveri'], data['feh'], thisI))

    # Split the groups up with cuts in FeH and sigma
    eoveri1 = [tup[0] for tup in tupleList if tup[1] < feh_cut_1 and abs(tup[2] - err) < errThresh]
    eoveri2 = [tup[0] for tup in tupleList if tup[1] >= feh_cut_1 and tup[1] < feh_cut_2 and abs(tup[2] - err) < errThresh]
    eoveri3 = [tup[0] for tup in tupleList if tup[1] >= feh_cut_2 and abs(tup[2] - err) < errThresh]
    eoveri = [eoveri1, eoveri2, eoveri3]
    label1 = '[Fe/H] < %.1f' % feh_cut_1
    label2 = '%.1f <= [Fe/H] < %.1f' % (feh_cut_1, feh_cut_2)
    label3 = '%.1f <= [Fe/H]' % feh_cut_2
    labels = [label1, label2, label3]
    styles = ['-', '--', '-.']

    # Plot
    fig = plt.figure()
    for m in range(3):
        x, y = [], []
        N = float(len(eoveri[m]))
        for i, eoi in enumerate(sorted(eoveri[m], reverse=True)):
            x.append(eoi)
            y.append((i+1)/N)
        plt.plot(x, y, label=labels[m], ls=styles[m])

    plt.xlim(0, 23)
    plt.xlabel(r'std(RV) / $\overline{\sigma}$')
    plt.ylabel('Cumulative PDF')
    plt.yscale('log')
    plt.legend(loc=1)
    save_figures(fig, plotPathDict)

