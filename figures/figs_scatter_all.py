"""
Scatter plots displaying information as a function of metallicity for the entire sample.
"""
import matplotlib.pyplot as plt
from numpy import corrcoef, log10, isfinite
from scipy.stats import linregress

import figures


def plot_lnk_vs_isBinaryFraction_scatter(dataDict, plotDir):
    """Plot scatter plot of lnK and isBinaryFraction."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'lnk_vs_isBinaryFraction')
    xyTuple = [(data['isBinaryFraction'], -1.0*data['-2lnK']) for pmf, data in dataDict.iteritems()]
    x, y = zip(*xyTuple)
    #print corrcoef(x, y)
    fig = plt.figure()
    plt.plot(x, y, ls='none', marker='o', ms=2, zorder=-1)
    plt.xlim(0, 1.1)
    plt.yscale('log')
    plt.xlabel(r'Fraction of traces where $isBinary$ == True')
    plt.ylabel('2lnK + 10')
    figures.save_figures(fig, plotPathDict)


def plot_eoveri_vs_feh_scatter(dataDict, plotDir):
    """Plot scatter plot of e/i versus metallicity."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'eoveri_vs_feh')
    xyTuple = [(data['feh'], data['eoveri']) for pmf,data in dataDict.iteritems()]
    x, y = zip(*xyTuple)
    fig = plt.figure()
    plt.plot(x, y, ls='none', marker='o', ms=2, zorder=-1)
    plt.xlim(-4.0, 1.0)
    #plt.ylim(0, 62)
    plt.xlabel(r'[Fe/H]')
    plt.ylabel(r'e/i')
    figures.save_figures(fig, plotPathDict)


def plot_nExp_vs_feh_scatter(dataDict, plotDir):
    """Plot scatter plot of exposure count versus metallicity."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'nExp_vs_feh')
    xyTuple = [(data['feh'], data['exposureCount']) for pmf,data in dataDict.iteritems()]
    x, y = zip(*xyTuple)
    fig = plt.figure()
    plt.plot(x, y, ls='none', marker='o', ms=2, zorder=-1)
    plt.xlim(-4.0, 1.0)
    plt.ylim(0, 62)
    plt.xlabel(r'[Fe/H]')
    plt.ylabel(r'Exposure Count')
    figures.save_figures(fig, plotPathDict)
