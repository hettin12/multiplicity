"""
Histograms displaying exposure information broken down by groups.
"""
import matplotlib.pyplot as plt
import numpy as np

import figures

widths = [5, 3, 1]
colors = ['DodgerBlue', 'black', 'red']
styles = ['-', '-', '-']

def plot_multiple_meanSNR_hist(dataDictList, plotDir):
    """Plot average signal-to-noise histogram for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'meanSNR_hist_multi')
    fig = plt.figure()
    thisRange = [0, 100] #May need to make range larger.
    nBins = 20
    for m,dataDict in enumerate(dataDictList):
        snrLists = figures.get_all_by_key(dataDict, 'exposureSNR')
        meanSNR = [np.mean(snr) for snr in snrLists]
        n, binEdges, __ = plt.hist(meanSNR, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, label=thisLabel, lw=widths[m], color=colors[m], ls=styles[m], drawstyle='steps-mid')
    plt.xlabel(r'$\overline{\rm{SNR}}$')
    plt.ylabel(r'log(N)')
    plt.xlim(15, 100)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()
    plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)


def plot_multiple_stdSNR_hist(dataDictList, plotDir):
    """Plot std(signal-to-noise) histogram for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'stdSNR_hist_multi')
    fig = plt.figure()
    thisRange = [-1, 12]
    nBins = 13
    for m,dataDict in enumerate(dataDictList):
        snrLists = figures.get_all_by_key(dataDict, 'exposureSNR')
        stdSNR = [np.std(snr) for snr in snrLists]
        n, binEdges, __ = plt.hist(stdSNR, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, label=thisLabel, lw=widths[m], color=colors[m], ls=styles[m], drawstyle='steps-mid')
    plt.xlabel(r'std(SNR)')
    plt.ylabel(r'log(N)')
    plt.xlim(0, 12)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()
    plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)


def plot_multiple_baseline_hist(dataDictList, plotDir, asPercent=False):
    """Plot histogram of finalTime = initialTime for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'baseline_hist_multi')
    fig = plt.figure()
    thisRange = [-1, 9]
    nBins = 25
    for m,dataDict in enumerate(dataDictList):
        taimids = figures.get_all_by_key(dataDict, 'taimids')
        baseline = [max(t) - min(t) for t in taimids]
        logBaseline = [np.log10(b) for b in baseline]
        n, binEdges, __ = plt.hist(logBaseline, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, label=thisLabel, lw=widths[m], color=colors[m], ls=styles[m], drawstyle='steps-mid')
    plt.axvline(np.log10(86400), ls='--', lw=1, color='k')     # 1 day
    plt.annotate('1d', (np.log10(120000), 3.4))
    plt.axvline(np.log10(2.63E6), ls='--', lw=1, color='k')    # 1 month
    plt.annotate('1m', (np.log10(4.0E6), 3.4))
    plt.axvline(np.log10(3.16E7), ls='--', lw=1, color='k')    # 1 year
    plt.annotate('1y', (np.log10(5.0E7), 3.4))
    plt.xlabel(r'$\Delta t$ $(\log\ \mathrm{s})$')
    plt.ylabel(r'log(N)')
    plt.xlim(2, 9)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()
    #plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)


def plot_multiple_nExp_hist(dataDictList, plotDir, asPercent=False):
    """Plot exposure count histogram for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'nExp_hist_multi')
    fig = plt.figure()
    thisRange = [-3, 60]
    nBins = 21
    for m,dataDict in enumerate(dataDictList):
        exposureCount = figures.get_all_by_key(dataDict, 'exposureCount')
        n, binEdges, __ = plt.hist(exposureCount, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, label=thisLabel, lw=widths[m], color=colors[m], ls=styles[m], drawstyle='steps-mid')

    plt.xlabel(r'Exposure Count')
    plt.ylabel(r'log(N)')
    plt.xticks([3, 9, 15, 21, 27, 33, 39, 45, 51, 57])
    plt.xlim(0, 62)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()
    plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)
