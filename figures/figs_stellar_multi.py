"""
Histograms displaying stellar properties broken down by groups.
"""
import matplotlib.pyplot as plt
import numpy as np

import figures

widths = [5, 3, 1]
colors = ['DodgerBlue', 'black', 'red']
labels = ['Metal-Poor', 'Metal-Intermediate', 'Metal-Rich']

def plot_multiple_feh_hist(dataDictList, plotDir):
    """Plot metallicity histogram for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'feh_hist_multi')
    fig = plt.figure()
    thisRange = [-4, 1]
    nBins = 50
    for m,dataDict in enumerate(dataDictList):
        feh = figures.get_all_by_key(dataDict, 'feh')
        n, binEdges, __ = plt.hist(feh, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = labels[m]
        plt.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    plt.xlabel(r'[Fe/H]')
    plt.ylabel(r'log(N)')
    plt.xlim(-4.0,1.0)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()
    plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)


def plot_multiple_teff_hist(dataDictList, plotDir):
    """Plot temperature histogram for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'teff_hist_multi')
    fig = plt.figure()
    thisRange = [4500,8500]
    nBins = 20
    for m,dataDict in enumerate(dataDictList):
        teff = figures.get_all_by_key(dataDict, 'teff')
        print m, np.median(teff)
        n, binEdges, __ = plt.hist(teff, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    plt.xlabel(r'$T_{\rm{eff}}$ (K)')
    plt.ylabel(r'log(N)')
    plt.xlim(4500,8500)
    #plt.xticks([5000, 6000, 7000, 8000])
    plt.ylim(-0.5, 4)
    plt.minorticks_off()
    plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)


def plot_multiple_logg_hist(dataDictList, plotDir):
    """Plot gravity histogram for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'logg_hist_multi')
    fig = plt.figure()
    thisRange = [3.7, 4.7]
    nBins = 20
    for m,dataDict in enumerate(dataDictList):
        logg = figures.get_all_by_key(dataDict, 'logg')
        n, binEdges, __ = plt.hist(logg, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    plt.xlabel(r'$\log{g}$')
    plt.ylabel(r'log(N)')
    plt.xlim(3.7, 4.7)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()
    plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)


def plot_multiple_gmag_hist(dataDictList, plotDir):
    """Plot g-magnitude histogram for all groups in the list."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'gmag_hist_multi')
    fig = plt.figure()
    thisRange = [10, 25]
    nBins = 15
    for m,dataDict in enumerate(dataDictList):
        gmag = figures.get_all_by_key(dataDict, 'gmag')
        n, binEdges, __ = plt.hist(gmag, nBins, range=thisRange, visible=False)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        n = [float(k) for k in n]
        # Make histogram log
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        # Plot
        thisLabel = 'Group %d' % m
        plt.plot(x, n, ls='steps-mid', label=thisLabel, lw=widths[m], color=colors[m])
    plt.xlabel(r'g Magnitude')
    plt.ylabel(r'log(N)')
    plt.xlim(25,10)
    plt.ylim(-0.5, 4)
    plt.minorticks_off()
    plt.legend(loc=1)
    figures.save_figures(fig, plotPathDict)

