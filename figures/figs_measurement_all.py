"""
Histograms displaying RV measurement information for the entire sample.
"""
import matplotlib.pyplot as plt
import numpy as np

from multiplicity.radialvelocity import absoluterv
import figures

def plot_meanRV_hist(dataDict, plotDir):
    """Plot average RV measurement histogram for all fibers in the dataDict."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'meanRV_hist')
    rvLists = figures.get_all_by_key(dataDict, 'absRVs')
    errLists = figures.get_all_by_key(dataDict, 'empiricalErrs')
    meanRV = [absoluterv.calc_weighted_mean(rvs, errors) for rvs,errors in zip(rvLists, errLists)]
    fig = plt.figure()
    plt.hist(meanRV, 20, log=True)
    plt.xlabel(r'$\overline{\rm{RV}}$ (km/s)')
    plt.ylabel(r'N')
    plt.ylim(0.3)
    plt.minorticks_off()
    figures.save_figures(fig, plotPathDict)


def plot_stdRV_hist(dataDict, plotDir):
    """Plot std(RV measurements) histogram for all fibers in the dataDict."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'stdRV_hist')
    rvLists = figures.get_all_by_key(dataDict, 'absRVs')
    errLists = figures.get_all_by_key(dataDict, 'empiricalErrs')
    stdRV = [absoluterv.calc_weighted_std(rvs, errors) for rvs,errors in zip(rvLists, errLists)]
    fig = plt.figure()
    plt.hist(stdRV, 20, log=True)
    plt.xlabel(r'std(RV) (km/s)')
    plt.ylabel(r'N')
    plt.ylim(0.3)
    plt.minorticks_off()
    figures.save_figures(fig, plotPathDict)


def plot_meanErr_hist(dataDict, plotDir):
    """Plot average measurement error histogram for all fibers in the dataDict."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'meanErr_hist')
    errLists = figures.get_all_by_key(dataDict, 'empiricalErrs')
    meanErr = [absoluterv.calc_i(errs) for errs in errLists]
    fig = plt.figure()
    plt.hist(meanErr, 20, log=True)
    plt.xlabel(r'$\overline{\sigma}$ (km/s)')
    plt.ylabel(r'N')
    plt.ylim(0.3)
    plt.minorticks_off()
    figures.save_figures(fig, plotPathDict)


def plot_stdErr_hist(dataDict, plotDir):
    """Plot std(measurement errors) histogram for all fibers in the dataDict."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'stdErr_hist')
    errLists = figures.get_all_by_key(dataDict, 'empiricalErrs')
    stdErr = [absoluterv.calc_unbiased_sample_standard_deviation(errs) for errs in errLists]
    fig = plt.figure()
    plt.hist(stdErr, 20, log=True)
    plt.xlabel(r'std($\sigma$)')
    plt.ylabel(r'N')
    plt.ylim(0.3)
    plt.minorticks_off()
    figures.save_figures(fig, plotPathDict)


def plot_eoveri_hist(dataDict, plotDir):
    """Plot hisogram of the e/i values for all fibers in the dataDict."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'eoveri_hist')
    eoveri = figures.get_all_by_key(dataDict, 'eoveri')
    fig = plt.figure()
    plt.hist(eoveri, 20, log=True)
    plt.xlabel(r'std(RV) / $\overline{\sigma}$')
    plt.ylabel(r'N')
    plt.xlim(0)
    plt.ylim(0.3)
    plt.minorticks_off()
    figures.save_figures(fig, plotPathDict)
