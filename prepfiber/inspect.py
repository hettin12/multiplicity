"""
Functions for analyzing a specified fiber.

Plot the spectra in raw and normalized forms, as coadd and individual exposures.
Write out a summary of the SSPP entry for a fiber.
Write out a measurement summary for a fiber, given a RV SQL database.

"""
import os

import numpy as np
import matplotlib.pyplot as plt

from multiplicity.prepfiber import prepfiber
from multiplicity.fits import fitsIO
from multiplicity.sqlwrapper import Database

def sspp_summary(ssppPath, fiberid, outDir=None):
    """Print/Write out an SSPP summary file for this fiber."""
    cols = fitsIO.get_column_names(ssppPath)
    fiberData = fitsIO.get_row_by_fid(ssppPath, fiberid)
    if fiberData is None:
        raise Exception('%s not found in database %s' % (fiberid, ssppPath))

    if outDir is not None:
        if not os.path.isdir(outDir):
            raise Exception("Out dir %s not found." % outDir)
        thisOutDir = os.path.join(outDir, '%s' % fiberid)
        if not os.path.isdir(thisOutDir):
            os.mkdir(thisOutDir)

        keyValuePairs = zip(cols, fiberData)
        outPath = os.path.join(thisOutDir, '%s_sspp.dat' % fiberid)
        with open(outPath, 'w') as outFile:
            for pair in keyValuePairs:
                outFile.write(str(pair) + '\n')
    else:
        print zip(cols, fiberData)


def measurement_summary(dbPath, fiberid, outDir=None):
    """Print/Write out a summary for the RV measurements for this fiber.
    Also calcualtes and writes (t-t0)."""
    db = Database.RVDatabase(dbPath)
    data = db.get_entry(fiberid)
    if data is None:
        raise Exception('%s not found in database %s' % (fiberid, dbPath))
    data = [str(d) for d in data]
    cols = db.get_column_names()
    
    if outDir is not None:
        if not os.path.isdir(outDir):
            raise Exception("Out dir %s not found." % outDir)
        thisOutDir = os.path.join(outDir, '%s' % fiberid)
        if not os.path.isdir(thisOutDir):
            os.mkdir(thisOutDir)
    
        keyValuePairs = zip(cols, data)
        outPath = os.path.join(thisOutDir, '%s_rvsql.dat' % fiberid)
        with open(outPath, 'w') as outFile:
            for pair in keyValuePairs:
                outFile.write(str(pair) + '\n')
                if pair[0] == 'taimids':
                    if ',' in pair[1]:
                        taimids = pair[1][1:-1].split(',')
                    else:
                        taimids = pair[1][1:-1].split()
                    t0 = int(float(taimids[0]))
                    thisDatum = [int(float(t)) - t0 for t in taimids]
                    thisPair = ('deltaTime', thisDatum)
                    outFile.write(str(thisPair) + '\n')
    else:
        print zip(cols, data)


def plot_spectra(fiber, ignoreMaskNames=None):
    """ Plot a figure with 12 subplots (4 subplots each at full lambda, 
        mid lambda, close lambda).

        1) Coadd with 1sigma error bars and physical parameters
        2) Individual exposures and SNR
        3) 'cleaned' individual exposures and 'bad pixels' flagged.
    """
    meanFlux = np.mean(fiber.raw[0].flux)
    stdFlux = np.std(fiber.raw[0].flux)
    sigmas = np.sqrt(fiber.coadd.var)
    cleanSpectra = prepfiber.clean_spectra(fiber.raw, ignoreMaskNames=ignoreMaskNames)

    fig = plt.figure(figsize=[56,28])

    # Row 0
    ####################################################################
    # Coadd Full
    ax00 = plt.subplot2grid((4,4), (0,0))
    ax00.errorbar(fiber.coadd.lamb, fiber.coadd.flux, yerr=sigmas, capsize=0, ls='none', alpha=0.5, color='DodgerBlue')
    ax00.plot(fiber.coadd.lamb, fiber.coadd.flux, color='blue')
    ax00.set_ylabel('Flux')
    ax00.set_xlim(3000, 10000)

    # Coadd Mid
    ax01 = plt.subplot2grid((4,4), (0,1))
    ax01.errorbar(fiber.coadd.lamb, fiber.coadd.flux, yerr=sigmas, capsize=0, ls='none', alpha=0.5, color='DodgerBlue', label=r'$1\sigma$')
    ax01.plot(fiber.coadd.lamb, fiber.coadd.flux, color='blue', label='Coadd')
    ax01.set_xlim(3750, 4000)
    ax01.axvline(3934.78, ls='--', color='k', alpha=0.5)
    ax01.axvline(3969.59, ls='--', color='k', alpha=0.5)
    plt.legend(loc=2, prop={'size':12})

    # Coadd Hb
    ax02 = plt.subplot2grid((4,4), (0,2))
    ax02.errorbar(fiber.coadd.lamb, fiber.coadd.flux, yerr=sigmas, capsize=0, ls='none', alpha=0.5, color='DodgerBlue')
    ax02.plot(fiber.coadd.lamb, fiber.coadd.flux, color='blue')
    ax02.axvline(4862.68, ls='--', color='k', alpha=0.5)
    ax02.set_xlim(4800, 4926)

    # Coadd Ha
    ax03 = plt.subplot2grid((4,4), (0,3))
    ax03.errorbar(fiber.coadd.lamb, fiber.coadd.flux, yerr=sigmas, capsize=0, ls='none', alpha=0.5, color='DodgerBlue')
    ax03.plot(fiber.coadd.lamb, fiber.coadd.flux, color='blue')
    ax03.axvline(6564.61, ls='--', color='k', alpha=0.5)
    ax03.set_xlim(6500, 6630)

    # Row 1
    ####################################################################    
    # Indiv Full
    ax10 = plt.subplot2grid((4,4), (1,0))
    for exp in range(fiber.nExp):
        ax10.plot(fiber.raw[exp].lamb, fiber.raw[exp].flux, alpha=0.75)
    ax10.set_ylabel('Flux')
    ax10.set_xlim(3000, 10000)
    ax10.set_ylim(meanFlux - 5.0*stdFlux,  meanFlux + 5.0*stdFlux)

    # Indiv Mid
    ax11 = plt.subplot2grid((4,4), (1,1))
    for exp in range(fiber.nExp):
        ax11.plot(fiber.raw[exp].lamb, fiber.raw[exp].flux, label='%d' % (exp))
    ax11.set_xlim(3750, 4000)
    ax11.set_ylim(meanFlux - 5.0*stdFlux,  meanFlux + 5.0*stdFlux)
    ax11.axvline(3934.78, ls='--', color='k', alpha=0.5)
    ax11.axvline(3969.59, ls='--', color='k', alpha=0.5)
    plt.legend(loc=2, prop={'size':12})

    # Indiv Hb
    ax12 = plt.subplot2grid((4,4), (1,2))
    for exp in range(fiber.nExp):
        ax12.plot(fiber.raw[exp].lamb, fiber.raw[exp].flux)
    ax12.set_xlim(4800, 4926)
    ax12.axvline(4862.68, ls='--', color='k', alpha=0.5)
    ax12.set_ylim(meanFlux - 5.0*stdFlux,  meanFlux + 5.0*stdFlux)

    # Indiv Ha
    ax13 = plt.subplot2grid((4,4), (1,3))
    for exp in range(fiber.nExp):
        ax13.plot(fiber.raw[exp].lamb, fiber.raw[exp].flux)
    ax13.set_xlim(6500, 6630)
    ax13.axvline(6564.61, ls='--', color='k', alpha=0.5)
    ax13.set_ylim(meanFlux - 5.0*stdFlux,  meanFlux + 5.0*stdFlux)

    # Row 2
    ####################################################################
    # Cleaned Full
    ax20 = plt.subplot2grid((4,4), (2,0))
    for exp in range(fiber.nExp):
        badPix = np.where(cleanSpectra[exp].flux == 1.0)[0]
        ax20.plot(cleanSpectra[exp].lamb, cleanSpectra[exp].flux - (exp+1))
        ax20.errorbar(cleanSpectra[exp].lamb[badPix], cleanSpectra[exp].flux[badPix]-(exp+1), yerr=0.25, capsize=0, ls='none', color='grey')
    ax20.set_ylabel('Normalized Flux (Bad Pix in Grey)')
    ax20.set_xlim(3000, 10000)

    # Cleaned Mid
    ax21 = plt.subplot2grid((4,4), (2,1))
    for exp in range(fiber.nExp):
        badPix = np.where(cleanSpectra[exp].flux == 1.0)[0]
        ax21.plot(cleanSpectra[exp].lamb, cleanSpectra[exp].flux - (exp+1), label='%d bad pix' % len(badPix))
        ax21.errorbar(cleanSpectra[exp].lamb[badPix], cleanSpectra[exp].flux[badPix]-(exp+1), yerr=0.25, capsize=0, ls='none', color='grey')
    ax21.set_xlim(3750, 4000)
    ax21.axvline(3934.78, ls='--', color='k', alpha=0.5)
    ax21.axvline(3969.59, ls='--', color='k', alpha=0.5)
    plt.legend(loc=2, prop={'size':12})

    # Cleaned Hb
    ax22 = plt.subplot2grid((4,4), (2,2))
    for exp in range(fiber.nExp):
        badPix = np.where(cleanSpectra[exp].flux == 1.0)[0]
        ax22.plot(cleanSpectra[exp].lamb, cleanSpectra[exp].flux - (exp+1))
        ax22.errorbar(cleanSpectra[exp].lamb[badPix], cleanSpectra[exp].flux[badPix]-(exp+1), yerr=0.25, capsize=0, ls='none', color='grey')
    ax22.axvline(4862.68, ls='--', color='k', alpha=0.5)
    ax22.set_xlim(4800, 4926)

    # Cleaned Ha
    ax23 = plt.subplot2grid((4,4), (2,3))
    for exp in range(fiber.nExp):
        badPix = np.where(cleanSpectra[exp].flux == 1.0)[0]
        ax23.plot(cleanSpectra[exp].lamb, cleanSpectra[exp].flux - (exp+1))
        ax23.errorbar(cleanSpectra[exp].lamb[badPix], cleanSpectra[exp].flux[badPix]-(exp+1), yerr=0.25, capsize=0, ls='none', color='grey')
    ax23.axvline(6564.61, ls='--', color='k', alpha=0.5)
    ax23.set_xlim(6500, 6630)

    # Row 3
    ####################################################################
    # Cleaned Overlap Full
    ax30 = plt.subplot2grid((4,4), (3,0))
    for exp in range(fiber.nExp):
        ax30.plot(cleanSpectra[exp].lamb, cleanSpectra[exp].flux, alpha=0.5)
    ax30.set_xlabel('Wavelength (Angstroms)')
    ax30.set_ylabel('Normalized Flux (Bad Pix in Grey)')
    ax30.set_xlim(3000, 10000)

    # Cleaned Overlap Mid
    ax31 = plt.subplot2grid((4,4), (3,1))
    for exp in range(fiber.nExp):
        ax31.plot(cleanSpectra[exp].lamb, cleanSpectra[exp].flux)
    ax31.set_xlabel('Wavelength (Angstroms)')
    ax31.axvline(3934.78, ls='--', color='k', alpha=0.5)
    ax31.axvline(3969.59, ls='--', color='k', alpha=0.5)
    ax31.set_xlim(3750, 4000)

    # Cleaned Overlap Hb
    ax32 = plt.subplot2grid((4,4), (3,2))
    for exp in range(fiber.nExp):
        ax32.plot(cleanSpectra[exp].lamb, cleanSpectra[exp].flux)
    ax32.axvline(4862.68, ls='--', color='k', alpha=0.5)
    ax32.set_xlabel('Wavelength (Angstroms)')
    ax32.set_xlim(4800, 4926)

    # Cleaned Overlap Ha
    ax33 = plt.subplot2grid((4,4), (3,3))
    for exp in range(fiber.nExp):
        ax33.plot(cleanSpectra[exp].lamb, cleanSpectra[exp].flux)
    ax33.axvline(6564.61, ls='--', color='k', alpha=0.5)
    ax33.set_xlabel('Wavelength (Angstroms)')
    ax33.set_xlim(6500, 6630)

    plt.tight_layout()
    return fig
