"""
Functions for loading and cleaning Fiber objects.

get_Fiber() for loading fiber.

clean_spectra() for normalizing and clipping

"""
import os, sys, datetime

import numpy as np

import dsdt.Fiber as Fiber

F_STAR_REGIONS = [(3600, 4000), (4070,4130), (4290,4360), (4830, 4890), (6540, 6580), (9000, 9500)]
G_STAR_REGIONS = [(3600, 4000), (4070,4130), (4290,4360), (4830, 4890), (5140, 5220), (5550, 5610), (5865, 5925), (6540, 6580), (8475, 8575), (8675, 8725), (9000, 9500)]
K_STAR_REGIONS = [(3600, 4000), (4070,4130), (4290,4360), (4830, 4890), (5140, 5220), (5865, 5925), (6540, 6580), (8475, 8575), (8675, 8725), (9000, 9500)]
M_STAR_REGIONS = [(3600, 4000), (4070,4130), (4290,4360), (4830, 4890), (5140, 5220), (5865, 5925), (6540, 6580), (8150, 8250), (8475, 8575), (8675, 8725), (9000, 9500)]

def get_Fiber(fiberid):
    """Given pppp-mmmmm-fff, determine if the file exists
    and load and return the Fiber obect for it. Return the Fiber object
    or None if not found."""
    plate, mjd, fiber = fiberid.split('-')
    spFiberFile = "spFiber-%04d-%05d-%03d.fits" % (int(plate), int(mjd), int(fiber)) 
    for path in os.environ['DSDT_DATA'].split(':'):
        platePath = os.path.join(path, "%04d" % int(plate))
        fibersPath = os.path.join(platePath, "fibers")
        fileName = os.path.join(fibersPath, spFiberFile)
        if os.path.exists(fileName):
            try:
                f = Fiber.Fiber(fileName)
            except Exception, e:
                message = "Exception thrown in " + spFiberFile + " (see traceback): " + str(e)
            return f
    # Only reach here if not found
    sys.stdout.write("Failed to find " + spFiberFile + '\n')
    return None


def clean_spectra(rawSpectra, trimSize=50, patchRegions=F_STAR_REGIONS, ignoreMaskNames=None):
    """Clean the fiber for use in correlations by normalizing and masking bad bits.
    Patched regions are ignored during continuum fit and can be specified (default Ca H/K, Balmer lines).
    Trimming will cut off a piece of each side equal to trimSize.
    Bad pixels are set to flux=1.0 and var=0.0.
    Extreme outliers are also set to flux=1.0 and var=0.0.
    Bit masks can be ignored by specifying them.  If CRSDSS mask is specified, use a very simple, x-sigma CR removal.
    """
    cleanSpectra = []
    for spec in rawSpectra:
        # Normalize
        normalizedSpec = spec.contin_normalize(patchRegions=patchRegions)
        # Set bad bits to 1s
        badIdx = normalizedSpec.whereBad(ignoreMaskNames=ignoreMaskNames)
        normalizedSpec.flux[badIdx] = 1.0 
        normalizedSpec.var[badIdx] = 0.0
        # Set CRs to 1s if ignoring CRSDSS
        if ignoreMaskNames is not None:
            if 'CRSDSS' in ignoreMaskNames:
                median = np.median(normalizedSpec.flux)
                sigma = np.std(normalizedSpec.flux)
                crIdx = np.where((normalizedSpec.flux > median+3*sigma))
                normalizedSpec.flux[crIdx] = 1.0 
                normalizedSpec.var[crIdx] = 0.0
        # Replace the ends with 1s
        if trimSize > 0:
            normalizedSpec.flux[:trimSize] = 1.0
            normalizedSpec.flux[-trimSize:] = 1.0
            normalizedSpec.var[:trimSize] = 0.0
            normalizedSpec.var[-trimSize:] = 0.0
        # Append normalized spectrum to list
        cleanSpectra.append(normalizedSpec)
    return cleanSpectra


def get_goodPixCount_after_normalizing(rawSpectra, patchRegions=F_STAR_REGIONS):
    """Normalize the spectrum and count the number of good pixels that survived 
    the process (finite values and no flagged bits)."""
    goodPixCounts = []
    for spec in rawSpectra:
        normalizedSpec = spec.contin_normalize(patchRegions=patchRegions)
        goodIdx = normalizedSpec.whereGood()
        goodPixCounts.append(len(goodIdx))
    return goodPixCounts
