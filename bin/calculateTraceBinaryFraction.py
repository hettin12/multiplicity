#!/usr/bin/env python
"""Read in a trace file for a specific fiber and calculate the fraction of traces that have isBinary >= 0.5.
Write this value to a file or print the value.
"""

import os, optparse

from multiplicity.markov import mcinout


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-f', '--fiberid', dest='fiberid', action='store', type=str, default=None, help='specific fiber to run')
    parser.add_option('-t', '--traceDir', dest='traceDir', action='store', type=str, default=None, help='dir with likelihood trace files for binary')
    parser.add_option('-o', '--outPath', dest='outPath', action='store', type=str, default=None, help='file to append fiber bayes factors')
    parser.add_option('-i', '--incremental', dest='incremental', action='store_true', default=False, help='will write isBinary fraction every so many traces.')
    args = parser.parse_args()[0]

    # locate and open binary trace file
    tracePath = mcinout.find_path(args.traceDir, args.fiberid)
    traces = mcinout.load_trace(tracePath)

    # calculate isBinaryFraction
    subTraces = [[], [], [], [], []]
    for i in range(len(traces)):
        idx = i % 5
        subTraces[idx].append(traces[i])

    binaryFractions = []
    for subTrace in subTraces:
        traceCount = len(subTrace)
        binaryTraces = [t for t in subTrace if t[4] >= 0.5]
        binaryCount = len(binaryTraces)
        isBinaryFraction = float(binaryCount) / float(traceCount)
        print binaryCount, traceCount
        binaryFractions.append(isBinaryFraction)

    # append to file
    if args.outPath is not None:
        with open(args.outPath, 'a') as outFile:
            if args.incremental:
                outFile.write('%s  %.8f %.8f %.8f %.8f %.8f\n' % (args.fiberid, binaryFractions[0], binaryFractions[1], binaryFractions[2], binaryFractions[3], binaryFractions[4]))
            else:
                outFile.write('%s  %d  %d  %.8f\n' % (args.fiberid, binaryCount, traceCount, isBinaryFraction))
    else:
        print 'len(binary) / len(total) = %d / %d = %.8f' % (binaryCount, traceCount, isBinaryFraction)

if __name__ == "__main__":
    main()
