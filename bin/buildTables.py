#!/usr/bin/env python
"""
Build a couple tables of information regarding multiplicity, in a format
useful for LaTeX.

\newcommand{\tblPriors}{
  \begin{deluxetable}{ccc}
    \tablecaption{MCMC Priors}
    %\tablenum{1}
    \tablehead{\colhead{Parameter} & \colhead{Lower Limit} & \colhead{Upper Limit} } 
    \startdata
      $\loga\ (\kmps)$ & 0.477 & 2.398 \\
      $\logp\ (\mathrm{s})$ & 4.0 & 7.0 \\
      $\phi$ & 0 & $2\pi$ \\
      $\vnaught\ (\kmps)$ & -600.0 & 600.0 \\
    \enddata
    \tablecomments{All parameters are drawn from a uniform prior between the limits.}
    \label{tbl:priors}
  \end{deluxetable}


"""
import optparse

from multiplicity.tables import tables
from multiplicity.radialvelocity import rvinout
from multiplicity.fits import fitsIO
from multiplicity.markov import mcinout


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='path to RV Database')
    parser.add_option('-s', '--ssppPath', dest='ssppPath', action='store', type=str, default=None, help='path to sspp Database')
    parser.add_option('-b', '--bayesPath', dest='bayesPath', action='store', type=str, default=None, help='path to bayes factors')
    args = parser.parse_args()[0]

    print 'Reading RV database...'
    dataDict = rvinout.load_data(args.dbPath, returnAsDict=True)
    print 'Finished reading.'

    if args.ssppPath is not None:
        print 'Reading SSPP FITS files...'
        ssppDict = fitsIO.get_data_dictionary(args.ssppPath)  # Takes a long time
        print 'Finished reading.'
        print 'Writing LaTeX code...'
        tables.build_eoveri_table(dataDict, ssppDict)

    if args.bayesPath is not None:
        print 'Reading bayes data file...'
        bayesDict = mcinout.load_bayesfactor_datafile(args.bayesPath)
        print 'Finished reading.'
        print 'Writing LaTeX code...'
        tables.build_bayesFactor_table(dataDict, bayesDict)

if __name__ == "__main__":
    main()


