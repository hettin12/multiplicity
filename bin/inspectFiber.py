#!/usr/bin/env python
"""
Given a fiberid, perform analysis and make plots, allowing us to
learn more about the fiber.

Input:
    - Fiberid
    - Plot directory
    - Path to write summary statistics


Output:
    -
    -
"""
import os, optparse

from multiplicity.prepfiber import inspect, prepfiber


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-f', '--fiberid', dest='fiberid', action='store', type=str, default=None, help='fiberid to inspect')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='Base dir for writing plots. (subdir with FID will be made)')
    parser.add_option('-o', '--outDir', dest='outDir', action='store', type=str, default=None, help='dir to write out summary information (use screen if not specified)')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='(optional) sqlite database with additional information')
    parser.add_option('-s', '--ssppPath', dest='ssppPath', action='store', type=str, default=None, help='(optional) sspp FITS file with stellar params')
    parser.add_option('-i', '--ignoreCR', dest='ignoreCR', action='store_true', default=False, help='ignore the CRSDSS mask bit when cleaning spectra')
    parser.add_option('-b', '--ignoreBADSKYCHI', dest='ignoreBADSKYCHI', action='store_true', default=False, help='ignore the BADSKYCHI mask bit when cleaning spectra')
    args = parser.parse_args()[0]

    # Load fiber FITS
    thisFiber = prepfiber.get_Fiber(args.fiberid)
    if thisFiber is None:
        raise Exception("%s not found." % args.fiberid)
    
    # Summary information from SSPP and RV SQL
    if args.ssppPath is not None:
        inspect.sspp_summary(args.ssppPath, args.fiberid, outDir=args.outDir)
    if args.dbPath is not None:
        inspect.measurement_summary(args.dbPath, args.fiberid, outDir=args.outDir)

    # Plots
    if args.plotDir is not None:
        if not os.path.isdir(args.plotDir):
            raise Exception("Plot dir %s not found." % args.plotDir)
        thisPlotDir = os.path.join(args.plotDir, '%s' % args.fiberid)
        if not os.path.isdir(thisPlotDir):
            os.mkdir(thisPlotDir)

        # Plot spectra
        if args.ignoreCR or args.ignoreBADSKYCHI:
            names = []
            if args.ignoreCR:
                names.append('CRSDSS')        
            if args.ignoreBADSKYCHI:
                names.append('BADSKYCHI')
            fig = inspect.plot_spectra(thisFiber, ignoreMaskNames=names)
        else:
            fig = inspect.plot_spectra(thisFiber)
        plotPath = os.path.join(thisPlotDir, '%s_spectra.png' % args.fiberid)
        fig.savefig(plotPath)


if __name__ == "__main__":
    main()

