#!/usr/bin/env python
"""
Calculate empirical errors for fibers as a function of metallicity
and signal-to-noise.

Uses the Maximum Absolute Deviation (MAD) for a group of 'similar' measurements
to determine the standard deviations for that group.  'Similar' measurements have
similar metallicity and similar signal-to-noise.

This yields a grid of empirical errors sigma(FeH,SNR).  The grid is smoothed by
calculating a 1/x fit to sigma(SNR) for each FeH group.  Then, the 1/x solutions
are solved together to find the change w.r.t. FeH.  This yields the sigma(FeH,SNR)
function.

This function is then applied to all fibers and the 'empiricalErrs' column is 
updated for each fiber in the database.

Input:
    - fiber RV database
    - metallicity bin threshold (plus or minus)
    - SNR bin threshold (plus or minus)
    - minimum number of RVs per bin
    - minimum number of similar exposures per fiber if either is to be used in a FeH-SNR bin
    - update flag to specify updating db
    - (optional) plot directory.

Output:
    - fiber RV database is updated with empiricalErrs
"""

import optparse

from multiplicity.sqlwrapper.Database import RVDatabase
from multiplicity.radialvelocity import rvinout
from multiplicity.empiricalerrors import errors


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='path to RV Database')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='dir for plots')
    parser.add_option('-f', '--fehThresh', dest='fehThresh', action='store', type=float, default=0.25, help='threshhold in Fe/H for choosing "like" exposures.')
    parser.add_option('-s', '--snrThresh', dest='snrThresh', action='store', type=float, default=2.0, help='threshhold in SNR for choosing "like" exposures.')
    parser.add_option('-m', '--minRVCount', dest='minRVCount', action='store', type=int, default=250, help='min number of RVs to include a (FeH,SNR) bin')
    parser.add_option('-e', '--minExp', dest='minExp', action='store', type=int, default=2, help='min number of similar exposures per fiber if either is to be used in a bin')
    parser.add_option('-u', '--updateDB', dest='updateDB', action='store_true', default=False, help='set true if you want the db to be updated with errors')
    args = parser.parse_args()[0]

    # Read in all of the fibers
    print 'Loading fibers...'
    tableDict = rvinout.load_data(args.dbPath, returnAsDict=True, cleanExposuresOnly=False)

    # Define groups
    fehGroups = [-1.50, -1.25, -1.00, -0.75, -0.5, -0.25, 0.00, 0.25]
    snrGroups = [21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 
                 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
                 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 
                 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
                 61, 62, 663, 64, 65, 66, 67, 68, 69, 70]

    # Solve the equation of empirical errors
    print 'Solving equations...'
    p = errors.calc_empirical_errors(tableDict, fehGroups, snrGroups, fehThresh=args.fehThresh, snrThresh=args.snrThresh, minRVCount=args.minRVCount, minExpPerFiber=args.minExp, plotDir=args.plotDir)
    print 'sigma = (%.2f*[Fe/H] + %.2f) / SNR + %.2f' % p

    # For each fiber, caclulate errors and update database
    if args.updateDB:
        print 'Updating errors...'
        calc_error = lambda feh, snr: (p[0]*feh + p[1]) / snr + p[2]
        fullTableDict = rvinout.load_data(args.dbPath, returnAsDict=True, cleanExposuresOnly=False)
        db = RVDatabase(args.dbPath)
        for fiberid, params in fullTableDict.iteritems():
            feh = params['feh']
            exposureSNR = params['exposureSNR']
            theseSigma = []
            for snr in exposureSNR:
                theseSigma.append(calc_error(feh, snr))
            db.set_value(fiberid, 'empiricalErrs', str(theseSigma))
        del db


if __name__ == "__main__":
    main()
