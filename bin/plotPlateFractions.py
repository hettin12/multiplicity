#!/usr/bin/env python
"""
Produce a plot showing the binary fraction as a function of metallicity group
for each choice of -2lnK cutoff.
"""

import optparse

from numpy import log

from multiplicity.markov import mcinout
from multiplicity.radialvelocity import rvinout
from multiplicity.figures import figs_bayesFactor


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-i', '--isBinaryPath', dest='isBinaryPath', action='store', type=str, default=None, help='file with isBinary values in it')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='sqlite3 file with rvs and stellar params')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='directory for writing plots')
    parser.add_option('--fehCut1', dest='fehCut1', action='store', type=float, default=None, help='first cut in metallicity')
    parser.add_option('--fehCut2', dest='fehCut2', action='store', type=float, default=None, help='second cut in metallicity. must be > first cut.')
    parser.add_option('-l', '--limit', dest='limit', action='store', type=float, default=0.65, help='binary threshold for isBinary percentage.')
    args = parser.parse_args()[0]

    # Read in isBinary, metallicities and empirical errs
    dataDict = rvinout.load_data(args.dbPath, silent=False, returnAsDict=True)
    isBinaryDict = mcinout.load_isBinary_datafile(args.isBinaryPath)

    # Append isBinary fraction to the dataDict.
    for pmf, data in dataDict.iteritems():
        fraction = isBinaryDict[pmf][2]
        dataDict[pmf]['isBinFrac'] = fraction

    # Split up the sample by Fe/H.
    if args.fehCut1 is not None:
        dataDictionaryList, vlines = rvinout.split_into_groups(dataDict, args.fehCut1, args.fehCut2, method='feh')
    else:
        dataDictionaryList = [dataDict]
        vlines = None

    # Create a plate-mjd dictionary with a list of F-type pmfs for each plate
    plateDict = {}
    for pmf in dataDict:
        plate, mjd, fiber = pmf.split('-')
        pm = '%s-%s' % (plate, mjd)
        if pm in plateDict:
            plateDict[pm].append(pmf)
        else:
            plateDict[pm] = [pmf]
    print len(plateDict), 'plates identified.'

    # For each plate-mjd in dictionary, calculate the number of binaries and the fraction of binaries
    plateCounts, plateFractions = [], []
    for pm, pmfList in plateDict.iteritems():
        binaryCount = 0
        for pmf in pmfList:
            if dataDict[pmf]['isBinFrac'] >= args.limit:
                binaryCount += 1
        plateCounts.append(binaryCount)
        plateFractions.append(float(binaryCount) / float(len(pmfList)))
    import numpy as np
    print np.mean(plateCounts)

    # Make figures
    figs_bayesFactor.plot_plate_fractions(plateCounts, plateFractions, args.limit, args.plotDir)


if __name__ == "__main__":
    main()
