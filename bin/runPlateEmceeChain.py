#!/usr/bin/env python
"""
Run an MCMC chain using the 'emcee' module.  

For a single fiber, set up the chain and run it.

Input:
    fiberid
    database with rv information
    directory for writing trace files
    path to a config file with (walkers, steps, burn, thin, priorLimits)   See: multiplicity.markov.mcinout
    optional flag to specify a single-star fun only, default is binary star.
    (optional) directory for writing plots
    (optional) verbosity in output to screen

Output:
    trace file containing one step per line and the parameters tried for that step
    summary file with run information
    (optional) plots of the trace history, triangle plot of traces, rv_curves

"""

import os, sys, optparse

import emcee
import numpy as np

from multiplicity.markov import mcescher, mcinout, plate_mcmc
from multiplicity.sqlwrapper import Database
from multiplicity.figures import figs_plateRVs

OUTLIER_THRESH = 600.0
STD_THRESH = 20.0
MEAN_RVS = False
SHIFTS = False
BINARY_OUTLIERS = False
SHIFT_ONLY = True
priorLimits = {'pBin':(0.0, 0.25), 'dy':(-50.0, 50.0), 'v0':(-600.0, 600.0), 'varBin':(100.0, 10000.0)}


def aggregate_exposures(args):
    """Gather exposures from various fibers on the plate and perform cuts as necessary.
    """
    db = Database.RVDatabase(args.dbPath, silent=(not args.verbose))
    big_fiberlist = db.get_uniqueIDs()
    del db

    fiberlist, fibersused, masterObs, rvMatrix, errMatrix = [], [], [], [], []
    for fiberid in big_fiberlist:
        if fiberid[:10] != args.plateid:
            continue
        fiberlist.append(fiberid)
        taimids, rvs, errs = mcinout.load_fiber(args.dbPath, fiberid)
        # Don't use fibers with less than two exposures below V_esc.
        reasonable_rvs = [val for val in rvs if abs(val) < OUTLIER_THRESH]
        if len(reasonable_rvs) < 2:
            continue
        # Don't use fibers with a large standard deviation.
        print fiberid, np.std(rvs)
        if np.std(rvs) > STD_THRESH:
            continue
        if not len(masterObs):
            masterObs.extend(taimids)
        else:
            assert list(taimids) == masterObs, '%s has a different set of observations' % fiberid
        rvMatrix.append(rvs)
        errMatrix.append(errs)
        fibersused.append(fiberid)

    if len(fibersused) < 3:
        er = ValueError('Less than 3 fibers in plate %s.' % args.plateid)
        er.args += (fiberlist,)
        raise er

    return fiberlist, fibersused, masterObs, rvMatrix, errMatrix


def create_labels(expCount, fiberCount):
    """Depending on the type of run, return a list of param labels."""
    labels = []
    if MEAN_RVS:
        for j in range(fiberCount):
            labels.append('v0_%d' % j)
    elif SHIFTS:
        for i in range(expCount-1):
            labels.append('dy_%d' % (i+1))
        for j in range(fiberCount):
            labels.append('v0_%d' % j)
    elif SHIFT_ONLY:
        for i in range(expCount):
            labels.append('dy_%d' % i)
    elif BINARY_OUTLIERS:
        labels.append('pBin')
        for i in range(expCount-1):
            labels.append('dy_%d' % (i+1))
        for j in range(fiberCount):
            labels.append('v0_%d' % j)
        for j in range(fiberCount):
            labels.append('varBin_%d' % j)
    return labels


def write_data_to_files(sampler, burned_traces, labels, args, nwalkers, nsteps, nthin, nburn, timeElapsed, masterObs, offsets, baseName, expCount):
    """Write data to text files."""
    try:
        acor = sampler.acor
    except:
        acor = [-9999 for i in range(ndim)]
    # Write traces.
    headline = '#'
    for label in labels:
        headline += '%s  ' % label
    headline += '\n'
    outPath = os.path.join(args.outDir, '%s-plate-traces.dat' % baseName)
    mcinout.write_trace(burned_traces, outPath, headline=headline)
    # Write summary.
    outPath = os.path.join(args.outDir, '%s-plate-summary.dat' % baseName)
    mcinout.write_summary(outPath, nwalkers, nsteps, nthin, nburn, len(burned_traces), np.mean(sampler.acceptance_fraction), acor, timeElapsed, labels=labels)
    # Write shifts to text file
    outPath = os.path.join(args.outDir, '%s-plate-shifts.dat' % baseName)
    with open(outPath, 'w') as outFile:
        outFile.write('@%s\n' % args.plateid)
        outFile.write('#TAIMID  measured_shift_in_kmps\n')
        for i in range(expCount):
            outFile.write('%d  %.2f\n' % (masterObs[i], offsets[masterObs[i]]))


def write_rvs_to_db(dbPath, writeDbPath, fiberlist, offsets):
    """Write new RVs to writeDB.  If offsets is None (because no offsets were
    calculated, update the new database to have the same RVs as the old database.
    """
    outDB = Database.RVDatabase(writeDbPath)
    for fiberid in fiberlist:
        taimids, absRVs, errs = mcinout.load_fiber(dbPath, fiberid)
        xyTuples = zip(taimids, absRVs)
        if offsets is None:
            newRVs = [rv for t, rv in xyTuples]
        else:
            newRVs = [rv - offsets[t] for t, rv in xyTuples]
        outDB.set_value(fiberid, 'absRVs', str(newRVs))
    del outDB


def calculate_rv_offsets(burned_traces, expCount, masterObs):
    """Calculate the measured shifts. Return a dictionary of shifts keyed by TAIMID.
    """
    paramResultTuple = burned_traces.T
    offsets = {}
    for i in range(expCount):
        if SHIFTS:
            if i == 0:
                mean_dy = 0.0
            else:
                mean_dy = np.mean(paramResultTuple[i-1])
        elif SHIFT_ONLY:
            mean_dy = np.mean(paramResultTuple[i])
        elif BINARY_OUTLIERS:
            if i == 0:
                mean_dy = 0.0
            else:
                mean_dy = np.mean(paramResultTuple[i])
        offsets[masterObs[i]] = mean_dy
    return offsets


def make_plots(args, sampler, burned_traces, baseName, nburn, labels, offsets):
    """Make chain, triangle, etc. plots."""
    plotDir = os.path.join(args.plotDir, args.plateid)
    if not os.path.isdir(plotDir):
        os.mkdir(plotDir)
    mcescher.plot_chain_traces(sampler.chain, plotDir=plotDir, baseName=baseName, nburn=nburn, labels=labels)
    mcescher.plot_parameter_triangle(burned_traces, plotDir=plotDir, baseName=baseName, labels=labels)
    figs_plateRVs.plot_plate(args.dbPath, args.plateid, plotDir)
    figs_plateRVs.plot_plate(args.dbPath, args.plateid, plotDir, offsets=offsets)


def main():
    parser = optparse.OptionParser(description='emcee runner. Written by Thomas Hettinger.')
    parser.add_option('-f', '--plateid', dest='plateid', action='store', type=str, help='name of plate to run test on')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, help='path to file with fiber data')
    parser.add_option('-w', '--writeDbPath', dest='writeDbPath', action='store', type=str, default=None, help='db to write new RVs to')
    parser.add_option('-o', '--outDir', dest='outDir', action='store', type=str, default=None, help='dir to write traces to')
    parser.add_option('-c', '--configPath', dest='configPath', action='store', type=str, default=None, help='config file with parameter priors')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='base dir for plots')
    parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='output more data to terminal')
    args = parser.parse_args()[0]

    # Collect exposures and RV data from sqlite3 file.
    try:
        fiberlist, fibersused, masterObs, rvMatrix, errMatrix = aggregate_exposures(args)
    except ValueError, e:
        message = e.args[0]
        if message.startswith('Less than 3'):
            sys.stderr.write('Exception: %s' % message)
            sys.stderr.write('  Using 0.0 km/s shifts for this plate.\n')
            if (args.writeDbPath is not None) and (not MEAN_RVS):
                fiberlist = e.args[1]
                write_rvs_to_db(args.dbPath, args.writeDbPath, fiberlist, offsets=None)
            if args.plotDir is not None:
                plotDir = os.path.join(args.plotDir, args.plateid)
                if not os.path.isdir(plotDir):
                    os.mkdir(plotDir)
                figs_plateRVs.plot_plate(args.dbPath, args.plateid, plotDir)
            return
        else:
            print 'Unexpected error %s.' % message
            raise

    # Read config file
    configDict = mcinout.load_chainconfig(args.configPath)
    nwalkers = configDict['walkers']
    nsteps = configDict['steps']
    nburn = configDict['burn']
    nthin = configDict['thin']

    # Set the number of dimensions
    expCount = len(masterObs)
    fiberCount = len(rvMatrix)
    if MEAN_RVS:
        ndim = fiberCount
    elif SHIFTS:
        ndim = fiberCount + expCount-1
    elif SHIFT_ONLY:
        ndim = expCount
    elif BINARY_OUTLIERS:
        ndim = fiberCount*2 + expCount-1 + 1

    # Set up the sampler and get walkers.
    sampler = emcee.EnsembleSampler(nwalkers, ndim, plate_mcmc.lnprob, args=(masterObs, rvMatrix, errMatrix, priorLimits))
    pos = plate_mcmc.get_walkers(rvMatrix, expCount, fiberCount, nwalkers, ndim)    

    # Clear and run the production chain.
    timeElapsed = plate_mcmc.run_chain(sampler, pos, nwalkers, nsteps, nthin, args.verbose)

    # Get traces
    burned_traces = sampler.chain[:, nburn:, :].reshape((-1, ndim)) #all_traces = sampler.chain[:, :, :].reshape((-1, ndim))
    totalTraceCount = len(burned_traces)

    # Create labels
    baseName = "%s_%d_%d_%d" % (args.plateid, nwalkers, nsteps, nthin)
    labels = create_labels(expCount, fiberCount)

    # Calculate rv offsets.
    if not MEAN_RVS:
        offsets = calculate_rv_offsets(burned_traces, expCount, masterObs)

    # Write data to files.
    if args.outDir is not None:
        write_data_to_files(sampler, burned_traces, labels, args, nwalkers, nsteps, nthin, nburn, timeElapsed, masterObs, offsets, baseName, expCount)

    # Write new RVs to writeDB.
    if (args.writeDbPath is not None) and (not MEAN_RVS):
        write_rvs_to_db(args.dbPath, args.writeDbPath, fiberlist, offsets)

    # Make plots if necessary
    if args.plotDir is not None and ndim <= 20:
        make_plots(args, sampler, burned_traces, baseName, nburn, labels, offsets)


if __name__ == "__main__":
    main()

