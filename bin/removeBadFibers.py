#!/usr/bin/env python
"""
Inputs:
    - PMF list of fiberids to remove 
    - PMF list of current fiberids in database.
    - Database to be updated. (These actions are not reversable,
                               so make a backup first)

Outputs:
    - pruned_primary.pmf
    - pruned_all.pmf
    - prunedManifest.csv
"""
import os, optparse

from multiplicity.sqlwrapper.Database import RVDatabase
from multiplicity.markov import mcinout


def main():
    parser = optparse.OptionParser(description='emcee runner. Written by Thomas Hettinger.')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, help='db to be updated (not reversable, make backup)')
    parser.add_option('-r', '--removePath', dest='removePath', action='store', type=str, help='PMF list of fibers to remove')
    parser.add_option('-o', '--outDir', dest='outDir', action='store', type=str, default='./', help='dir to write data to')
    parser.add_option('-m', '--manifest', dest='manifest', action='store', type=str, default=None, help='original manifest of stars and fibers')
    args = parser.parse_args()[0]

    # Read in removal list
    removeList = mcinout.load_fiberlist(args.removePath)

    # Open db, remove entries
    db = RVDatabase(args.dbPath)
    counter = 0
    skip_counter = 0
    for i, pmf in enumerate(removeList):
        if db.id_in_table(pmf) is not None:
            db.remove_entry(pmf)
            counter += 1
        else:
            print 'Skipping %s.  Not found in database.' % pmf
            skip_counter += 1
    print 'Skipped %s entries from %s' % (skip_counter, args.dbPath)
    print 'Removed %d entries from %s' % (counter, args.dbPath)
    del db

    # Create pruned_primary.pmf, pruned_all.pmf, prunedManifest.csv
    if args.manifest is not None:
        newManifestFile = open(os.path.join(args.outDir, 'prunedManifest.csv'), 'w')
        newPrimaryFile = open(os.path.join(args.outDir, 'pruned_primary.pmf'), 'w')
        newAllFile = open(os.path.join(args.outDir, 'pruned_all.pmf'), 'w')

        with open(args.manifest, 'r') as inFile:
            for line in inFile.readlines():
                fibers = line.strip().split(',')
                primary = fibers[0]
                # Ignore if on remove list
                if primary in removeList:
                    continue
                # Write to disk
                newManifestFile.write('%s\n' % line.strip())
                newPrimaryFile.write('%s\n' % primary)
                for pmf in fibers:
                    newAllFile.write('%s\n' % pmf)

if __name__ == "__main__":
    main()

