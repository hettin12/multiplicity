#!/usr/bin/env python2.6
"""
Go through an SSPP FITS file and select stars based on input PMF.
Make a smaller SSPP FITS containing params for the selected stars.

Input:  
    - the SSPP DRX parameters from FITS file.
    - outpath for writing new fits file
    - path with list of fiber PMFs

Output: 
    - a new FITS file containing entries of all F type stars.
"""

import optparse

from multiplicity.fits import fitsIO
from multiplicity.markov import mcinout


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-s', '--ssppFits', dest='ssppFits', action='store', type=str, default=None, help='path to FITS for opening and selecting fibers.')
    parser.add_option('-o', '--outFits', dest='outFits', action='store', type=str, default=None, help='path to create new smaller FITS file with only selected fibers (optional)')
    parser.add_option('-f', '--fiberlistPath', dest='fiberlistPath', action='store', type=str, default=None, help='path to read a list of fiber PMFs')
    args = parser.parse_args()[0]

    # Open FITS and read in data
    cols, data = fitsIO.open_fits(args.ssppFits, printCols=True)

    # Open fiberlistFile and get list of PMFs
    pmfList = mcinout.load_fiberlist(args.fiberlistPath)

    # Select F-type stars
    index = fitsIO.get_index_from_pmf(data, pmfList)

    # Write new, smaller FITS file
    if args.outFits is not None:
        fitsIO.write_FITS(index, data, args.outFits)


if __name__ == "__main__":
    main()
