#!/usr/bin/env python
"""
Run an analysis on the Bayes Factors, K, calculated for all fibers.  Identify likely
binaries and optionally make plots.

Plots include a histogram of K values, and DBIC vs. K.

Input:
    Bayes Factors data file (fiberid, L_binary, L_single, K)
    threshold for K value to determine if it is a binary
    (optional) upper limit for the K value to cut out (usefully for probing a region in K-space)
    (optional) file for writing file with list of 'likely binaries'
    (optional) maximum likelihood data file (fiberid, oribtalParams[4], L, lnL, BIC, ..., DBIC)
    (optional) plot directory for plotting
    (optional) directory containing fiber dirs/plots.  If specified, copy fibers with K>thresh to plotDir


Output:
    list of likely binaries
    histogram plot of K values
    scatter plot of DBIC and K


K-value Interpretations:
    Harold Jeffreys
    K-value      Strength of evidence
    ----------   --------------------
    < 1          supports single-star
      1 -- 3     barely worth mentioning binary
      3 -- 10    substantial for binary
     10 -- 30    strong for binary
     30 -- 100   very strong for binary
        > 100    decisive


    Kass and Raftery (1995)
    2lnK     K       Strength of Evidence
    ----   -----   ------------------------
    0-2     1-3      Not worth more than bare mention
    2-6     3-20     Positive
    6-10   20-150    Strong
    > 10   > 150     Very Strong

"""

import os, sys, optparse, shutil

import numpy as np

from multiplicity.markov import mcinout, mcescher


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-b', '--bayesPath', dest='bayesPath', action='store', type=str, default=None, help='file with bayes factors in it')
    parser.add_option('-t', '--thresh', dest='thresh', action='store', type=float, default=10.0, help='threshhold for K value to determine if likely binary (default 10)')
    parser.add_option('-u', '--upperlimit', dest='upperlimit', action='store', type=float, default=None, help='optional upper limit for K.')
    parser.add_option('-o', '--outPath', dest='outPath', action='store', type=str, default=None, help='file to write fibers that are likely binaries')
    parser.add_option('-m', '--maxLPath', dest='maxLPath', action='store', type=str, default=None, help='file with likelihoods for best binary orbits')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='directory for writing plots')
    parser.add_option('-c', '--copyDir', dest='copyDir', action='store', type=str, default=None, help='dir contaning fiber sub_dir and plots to be coppied (optional)')
    args = parser.parse_args()[0]

    # open Bayes Factor file and read data into dictionary
    bayesFactorDict = mcinout.load_bayesfactor_datafile(args.bayesPath)

    # determine which are likely binaries
    likelyBinaries = []
    for fid, tup in bayesFactorDict.iteritems():
        if tup[2] > args.thresh:
            if args.upperlimit is not None:
                if tup[2] <= args.upperlimit:
                    likelyBinaries.append( (fid,) + tup )
            else:
                likelyBinaries.append( (fid,) + tup )
    if len(likelyBinaries) == 0:
        print 'No likely binaries with thresh=%.1f' % args.thresh
        sys.exit()
    likelyBinaries.sort(key=lambda x: x[3], reverse=True)    

    # write out list
    if args.outPath is not None:
        with open(args.outPath, 'w') as outFile:
            for tup in likelyBinaries:
                outFile.write('%s  %.3f\n' % (tup[0], tup[3]))
    else:
        for tup in likelyBinaries:
            print '%s  %.3f' % (tup[0], tup[3])


    # make plots
    if args.plotDir is not None:
        # make histogram
        if args.copyDir is None:
            mcescher.plot_param_hist([np.log10(tup[2]) for key,tup in bayesFactorDict.iteritems() if tup[2] < 1e50], label='logK', plotDir=args.plotDir, ylog=True)

        # make k-DBIC scatter
        if args.maxLPath is not None and args.copyDir is None:
            maxLDict = mcinout.load_likelihood_datafile(args.maxLPath)
            tupleList = []
            for fid in maxLDict:
                tupleList.append((bayesFactorDict[fid][2], maxLDict[fid][-1]))
            mcescher.plot_DBIC_vs_bayesFactor(tupleList, plotDir=args.plotDir)
            pass

        # copy fiber plots for just those passing threshhold test
        if args.copyDir is not None:
            for tup in likelyBinaries:
                pmf = tup[0]

                thisTargetDir = os.path.join(args.copyDir, pmf)
                if not os.path.isdir(thisTargetDir):
                    print '%s not found.' % thisTargetDir
                    continue

                src1 = os.path.join(thisTargetDir, '%s-triangle.png' % pmf)
                src2 = os.path.join(thisTargetDir, 'orbit_comparison.png')
                src3 = os.path.join(thisTargetDir, 'orbit_comparison_folded.png')

                dest1 = os.path.join(args.plotDir, '%s-triangle.png' % pmf)
                dest2 = os.path.join(args.plotDir, '%s-orbit_comparison.png' % pmf)
                dest3 = os.path.join(args.plotDir, '%s-orbit_comparison_folded.png' % pmf)

                try:
                    shutil.copy(src1, dest1)
                except:
                    print 'failed to copy %s --> %s' % (src1, dest1)
                try:
                    shutil.copy(src2, dest2)
                except:
                    print 'failed to copy %s --> %s' % (src2, dest2)
                try:
                    shutil.copy(src3, dest3)
                except:
                    print 'failed to copy %s --> %s' % (src3, dest3)



if __name__ == "__main__":
    main()
