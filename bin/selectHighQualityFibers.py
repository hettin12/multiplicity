#!/usr/bin/env python2.6
"""
Go through F-star SSPP FITS file and select stars based on quality criteria.
Write the final list to a PMF file and make a smaller SSPP FITS 
containing params for the selected stars.

Input:  
    - the F-type SSPP parameters from FITS file.
    - path for writing list of PMFs
    - outpath for writing new fits file

Output: 
    - a new FITS file containing entries of all F type stars.
    - PMF text file with list of all F-type PMF
"""

import optparse

from multiplicity.fits import fitsIO


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-s', '--ssppFits', dest='ssppFits', action='store', type=str, default=None, help='path to FITS for opening and selecting fibers.')
    parser.add_option('-o', '--outFits', dest='outFits', action='store', type=str, default=None, help='path to create new smaller FITS file with only selected fibers (optional)')
    parser.add_option('-f', '--fiberlistPath', dest='fiberlistPath', action='store', type=str, default=None, help='path to write a list of fiber PMFs (optional)')
    parser.add_option('-n', '--noiseMin', dest='noiseMin', action='store', type=float, default=25.0, help='minimum signal-to-noise ratio for exposuure to be considered good')
    parser.add_option('-p', '--pixelMin', dest='pixelMin', action='store', type=int, default=3000, help='minimum number of unflagged pixels for exposure to be considered good')
    parser.add_option('-e', '--expMin', dest='expMin', action='store', type=int, default=3, help='minimum number of good exposures to keep fiber')

    args = parser.parse_args()[0]

    # Open FITS and read in data
    cols, data = fitsIO.open_fits(args.ssppFits)

    # Select F-type stars
    qualityIndex = fitsIO.quality_cut(data, snrMin=args.noiseMin, pixelMin=args.pixelMin, expMin=args.expMin)

    # Write new, smaller FITS file
    if args.outFits is not None:
        fitsIO.write_FITS(qualityIndex, data, args.outFits)

    # Write fiberlist PMF file
    if args.fiberlistPath is not None:
        fitsIO.write_fiberlist(qualityIndex, data, args.fiberlistPath)


if __name__ == "__main__":
    main()
