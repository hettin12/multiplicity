#!/usr/bin/env python
"""
Open a RV database and caclulate e/i for each star.  e/i is the standard deviation in 
radial velocity measurements divided by the mean measurement error. Update the RV
database 'eoveri' column with values.



Input
    - RV database to update
    - 'u' flag to confirm database update

Output
    - Updates the rv database

"""
import optparse

import numpy as np

from multiplicity.sqlwrapper.Database import RVDatabase
from multiplicity.radialvelocity import rvinout, absoluterv

def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='path to sqlite3 with RV data')
    parser.add_option('-u', '--update', dest='update', action='store_true', default=False, help='update database')
    args = parser.parse_args()[0]

    if args.update:
        db = RVDatabase(args.dbPath)

    dataDict = rvinout.load_data(args.dbPath, silent=True, returnAsDict=True)
    for pmf, data in dataDict.iteritems():
        rvs = data['absRVs']
        errs = data['empiricalErrs']
        eoi = absoluterv.calc_eoveri(rvs, errs)
        if args.update:
            db.set_value(pmf, 'eoveri', eoi)
        else:
            print eoi

    if args.update:
        del db


if __name__ == "__main__":
    main()
