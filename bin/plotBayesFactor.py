#!/usr/bin/env python
"""
Read in Bayes Factors for fibers and plot against metallicity.
Makes a surface plot as well as counting the number of likeley 
binaries (given threshold K value).

Input:
    data file with bayes factors
    databas with RV info and metallicity
    lower limit on k-value for binary
    plotdir
    

Output:
    surface plot Fe/H vs. K-value (with histograms)
    number of binaries per metallicity group
    

"""
import os, optparse

from numpy import log, log10
import numpy as np
import matplotlib.pyplot as pl

from multiplicity.markov import mcescher, mcinout
from multiplicity.radialvelocity import rvinout, absoluterv
from multiplicity.figures import figures, figs_bayesFactor


def plot_cumulative_BayesFactor(tupleList, plotDir, err, errThresh, feh_cut_1, feh_cut_2):
    """Plot a cumulative fraction of stars by increasing Bayes Factor.
    TupleList should be (-2lnK, [Fe/H], mean(sigma))."""
    plotPathDict = figures.setup_plot_directory(plotDir, 'bayes-cumulative_err%.1f' % err)

    # Split the groups up with cuts in FeH and sigma
    bayesFactors1 = [tup[0] for tup in tupleList if tup[1] < feh_cut_1 and abs(tup[2] - err) < errThresh]
    bayesFactors2 = [tup[0] for tup in tupleList if tup[1] >= feh_cut_1 and tup[1] < feh_cut_2 and abs(tup[2] - err) < errThresh]
    bayesFactors3 = [tup[0] for tup in tupleList if tup[1] >= feh_cut_2 and abs(tup[2] - err) < errThresh]
    bayesFactors = [bayesFactors1, bayesFactors2, bayesFactors3]
    label1 = '[Fe/H] < %.1f' % feh_cut_1
    label2 = '%.1f <= [Fe/H] < %.1f' % (feh_cut_1, feh_cut_2)
    label3 = '%.1f <= [Fe/H]' % feh_cut_2
    labels = [label1, label2, label3]
    print len(bayesFactors1), len(bayesFactors2), len(bayesFactors3)
    styles = ['-', '--', '-.']

    # Plot
    fig = pl.figure()
    for m in range(3):
        x, y = [], []
        N = float(len(bayesFactors[m]))
        for i, bf in enumerate(sorted(bayesFactors[m])):
            if bf == -np.inf:
                bf = -1E9 # set infinite values to something really small.
            x.append(bf)
            y.append((i+1)/N)
        pl.plot(x, y, label=labels[m], ls=styles[m])

    pl.xlim(-1400, 20)
    pl.xlabel('-2lnK')
    pl.ylabel('Cumulative PDF')
    pl.yscale('log')
    pl.legend(loc=2)
    figures.save_figures(fig, plotPathDict)


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-b', '--bayesPath', dest='bayesPath', action='store', type=str, default=None, help='file with bayes factors in it')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='sqlite3 file with rvs and stellar params')
    parser.add_option('-l', '--lowerlimit', dest='lowerlimit', action='store', type=float, default=10.0, help='lower limit for K-value for binarity')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='directory for writing plots')
    parser.add_option('--fehCut1', dest='fehCut1', action='store', type=float, default=None, help='first cut in metallicity')
    parser.add_option('--fehCut2', dest='fehCut2', action='store', type=float, default=None, help='second cut in metallicity. must be > first cut.')
    parser.add_option('--teffCut1', dest='teffCut1', action='store', type=float, default=None, help='first cut in temperature')
    parser.add_option('--teffCut2', dest='teffCut2', action='store', type=float, default=None, help='second cut in temperature. must be > first cut.')
    args = parser.parse_args()[0]

    # Read in bayes factors, metallicities and empirical errs
    bayesFactorDict = mcinout.load_bayesfactor_datafile(args.bayesPath)
    dataDict = rvinout.load_data(args.dbPath, silent=False, returnAsDict=True)

    # Append K, -2lnK, i to the dataDict.
    for pmf, data in dataDict.iteritems():
        K = bayesFactorDict[pmf][2]
        dataDict[pmf]['K'] = K
        lnK = -2*log(K)
        dataDict[pmf]['-2lnK'] = lnK
        meanError = absoluterv.calc_i(data['empiricalErrs'])
        dataDict[pmf]['meanError'] = meanError

    # Split up the sample by Fe/H or Teff.
    if args.teffCut1 is not None:
        dataDictionaryList, vlines = rvinout.split_into_groups(dataDict, args.teffCut1, args.teffCut2, method='teff')
        dataDictionaryList = dataDictionaryList[::-1]
        vlines = None
    elif args.fehCut1 is not None:
        dataDictionaryList, vlines = rvinout.split_into_groups(dataDict, args.fehCut1, args.fehCut2, method='feh')
    else:
        dataDictionaryList = [dataDict]
        vlines = None

    # Make figures
    #figs_bayesFactor.plot_multiple_difference(dataDictionaryList, args.plotDir)
    #figs_bayesFactor.plot_cumulative_bayes_xlog(dataDictionaryList, args.plotDir)
    figs_bayesFactor.plot_multiple_bayes_histogram(dataDictionaryList, args.plotDir)
    #figs_bayesFactor.plot_cumulative_bayes(dataDictionaryList, args.plotDir)

if __name__ == "__main__":
    main()
