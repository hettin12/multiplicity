#!/usr/bin/env python
"""
Read in a likelihood file and grab best parameters for 
fibers identified by a fiberlist.
"""

import optparse

from numpy import log

from multiplicity.figures import figures
from multiplicity.radialvelocity import rvinout
from multiplicity.markov import mcinout


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='path to sqlite3 with RV data')
    parser.add_option('-f', '--fiberlistPath', dest='fiberlistPath', action='store', type=str, default=None, help='path to list of fibers to be included')
    parser.add_option('-l', '--likelihoodPath', dest='likelihoodPath', action='store', type=str, default=None, help='path to likelihood traces')
    parser.add_option('-b', '--bayesPath', dest='bayesPath', action='store', type=str, default=None, help='path to bayesFactor values')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='directory for writing plots to')
    parser.add_option('-t', '--bayesThresh', dest='bayesThresh', action='store', type=float, default=-50.0, help='threshhold for bayesfactor')
    parser.add_option('--fehCut1', dest='fehCut1', action='store', type=float, default=None, help='first cut in metallicity')
    parser.add_option('--fehCut2', dest='fehCut2', action='store', type=float, default=None, help='second cut in metallicity. must be > first cut.')
    parser.add_option('-x', '--logp4', dest='logp4', action='store_true', default=False, help='if flagged, print list of fibers with logP close to 4.0')
    parser.add_option('-a', '--loga', dest='loga', action='store_true', default=False, help='if flagged, plot amplitudes instead')
    args = parser.parse_args()[0]

    bigDict = rvinout.load_data(args.dbPath, silent=True, returnAsDict=True)

    # Shorten the list
    if args.fiberlistPath is not None:
        fiberlist = mcinout.load_fiberlist(args.fiberlistPath)
        dataDict = {}
        for fiber in fiberlist:
            dataDict[fiber] = bigDict[fiber]
    else:
        bayesFactorDict = mcinout.load_bayesfactor_datafile(args.bayesPath)
        dataDict = {}
        for fiber, data in bayesFactorDict.iteritems():
            K = data[2]
            lnK = -2*log(K)
            if lnK <= args.bayesThresh:
                if fiber in bigDict:
                    dataDict[fiber] = bigDict[fiber]

    # Add in best logP values
    with open(args.likelihoodPath,'r') as inFile:
        for line in inFile.readlines():
            tup = line.strip().split()
            pmf = tup[0]
            if pmf in dataDict:
                logP = float(tup[2])
                logA = float(tup[1])
                dataDict[pmf]['logP'] = logP
                dataDict[pmf]['logA'] = logA
                if args.logp4:
                    if logP <= 4.2:
                        print pmf, logP

    # Split up the group by Fe/H if necessary.
    if args.fehCut1 is not None:
        groupADict, groupBDict, tempDict = {}, {}, {}

        for pmf, data in dataDict.iteritems():
            if data['feh'] < args.fehCut1:
                groupADict[pmf] = data
            else:
                tempDict[pmf] = data

        if args.fehCut2 is None:
            groupBDict = tempDict
            dataDictionaryList = [groupADict, groupBDict]
            vlines = [args.fehCut1]

        else:
            groupCDict = {}
            if args.fehCut2 <= args.fehCut1:
                raise Exception('Invalid Fe/H cuts.')

            for pmf, data in tempDict.iteritems():
                if data['feh'] < args.fehCut2:
                    groupBDict[pmf] = data
                else:
                    groupCDict[pmf] = data
            dataDictionaryList = [groupADict, groupBDict, groupCDict]
            vlines = [args.fehCut1, args.fehCut2]
            print '%d low, %d mid, %d high' % (len(groupADict), len(groupBDict), len(groupCDict))

    else:
        dataDictionaryList = [dataDict]
        vlines = None

    # Plot
    figures.plot_best_periods(dataDictionaryList, args.plotDir, args.bayesThresh, plotLogA=args.loga)


if __name__ == "__main__":
    main()

