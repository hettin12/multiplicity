#!/usr/bin/env python
""" 
Update a Database with the values of Fe/H, logg, Teff, ang gMag
given a FITS file of stellar parameters and an optional list of fibers,
If no list is given, all fibers in RV database will be updated if possible.


Input:
    - SSPP fits file
    - RVDatabase sql file
    - (optional) list of fibers to update

Output:
    - The input RVDatabase will be updated
"""

import sys, optparse

from multiplicity.sqlwrapper.Database import RVDatabase
from multiplicity.markov import mcinout
from multiplicity.fits import fitsIO


def update_values(db, fiberlist, paramDict):
    """Update the stellar params in the db for all fibers in fiberlist.
    Insert fiber if not already in database."""
    for fiberid in fiberlist:
        if fiberid in paramDict:
            if db.id_in_table(fiberid) is None:
                db.insert_entry(fiberid)
            thisDict = paramDict[fiberid]
            db.set_many_values(fiberid, feh=float(thisDict['feh']), teff=float(thisDict['teff']), logg=float(thisDict['logg']), gmag=float(thisDict['gmag']))
        else:
            sys.stderr.write('%s not found in sspp file.\n' % fiberid)
    

def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-s', '--ssppPath', dest='ssppPath', action='store', type=str, default=None, help='path to FITS file with stellar params')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='path to SQL file to update')
    parser.add_option('-f', '--fiberlistPath', dest='fiberlistPath', action='store', type=str, default=None, help='(optional) pmf file with fibers to update')
    args = parser.parse_args()[0]

    # Read in SSPP params
    print 'Reading SSPP FITS...'
    paramDict = fitsIO.get_stellar_param_dictionary(args.ssppPath)

    # Open RV database
    db = RVDatabase(args.dbPath)

    # Get fiberlist
    print 'Making fiberlist...'
    if args.fiberlistPath is not None:
        fiberlist = mcinout.load_fiberlist(args.fiberlistPath)
    else:
        result = db.get_rows_with_fields('fiberid')
        fiberlist = [str(tup[0]) for tup in result]

    # Update values
    print 'Updating database...'
    update_values(db, fiberlist, paramDict)

    # Close database
    del db


if __name__ == "__main__":
    main()

