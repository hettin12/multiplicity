#!/usr/bin/env python
import os, optparse

from multiplicity.montecarlo import population
from multiplicity.radialvelocity import rvinout
from multiplicity.markov import mcinout
from multiplicity.sqlwrapper import Database

def main():
    parser = optparse.OptionParser(description='Monte-Carlo simulation of stellar systems with companions. Written by Thomas Hettinger.')
    parser.add_option('-o', '--outDir', dest='outDir', action='store', type=str, default=None, help='top level directory for all mc output data')
    parser.add_option('-c', '--configPath', dest='configPath', action='store', type=str, default=None, help='path to config file with population parameters for all set numbers')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='path to sqlite3 with RV data')
    parser.add_option('-f', '--fiberlistPath', dest='fiberlistPath', action='store', type=str, default=None, help='list of fibers to model')
    parser.add_option('-s', '--setNumber', dest='setNumber', action='store', type=int, default=None, help='set number defining hyper parameters')
    parser.add_option('-r', '--runNumber', dest='runNumber', action='store', type=int, default=None, help='run number for for these hyperParams')
    #parser.add_option('-r', '--rlocheck', dest='rlocheck', action='store_true', default=False, help='if flagged, will set amin = RLO for that system.')
    args = parser.parse_args()[0]

    # Make hyperparam set directory and db file if necessary
    if not os.path.isdir(args.outDir):
        raise Exception('%s is not a directory.' % args.outDir)
    setDir = 'hyperparam_set_%03d' % int(args.setNumber)
    fullSetDir = os.path.join(args.outDir, setDir)
    if not os.path.isdir(fullSetDir):
        os.mkdir(fullSetDir)
    runDatabase = 'run_%04d.sqlite3' % int(args.runNumber)
    fullDatabasePath = os.path.join(fullSetDir, runDatabase)
    if not os.path.exists(fullDatabasePath):
        outDB = Database.MCDatabase(fullDatabasePath, create=True, silent=False)
    else:
        outDB = Database.MCDatabase(fullDatabasePath)

    # Read in fiberlist, config file, rv information
    fiberlist = mcinout.load_fiberlist(args.fiberlistPath)
    configDict = population.load_population_config(args.configPath, args.setNumber)
    dataDict = rvinout.load_data(args.dbPath, silent=True, returnAsDict=True)

    # Simulate orbits
    for fiberid in fiberlist:
        population.simulate_system(fiberid, dataDict[fiberid], configDict, outDB)


if __name__ == "__main__":
    main()
