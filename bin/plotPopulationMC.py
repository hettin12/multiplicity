#!/usr/bin/env python

import os, optparse

from multiplicity.montecarlo import popanalysis
from multiplicity.radialvelocity import rvinout

def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='path to sqlite3 with RV data')
    parser.add_option('-m', '--modelDir', dest='modelDir', action='store', type=str, default=None, help='directory with the mc runs for this param set')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='directory for writing plots to')
    parser.add_option('-n', '--plotName', dest='plotName', action='store', type=str, default=None, help='filename, ignoring full path, for the plot')
    args = parser.parse_args()[0]

    # Read in rv db file, get real EOI
    rvDict = rvinout.load_data(args.dbPath, silent=True, returnAsDict=True)
    realEOI = [fiber['eoveri'] for fiber in rvDict.values()]
    print 'Retrieved database e/i.'

    # Create the base plot for e/i data to be placed.
    fig = popanalysis.create_fig()

    # Get the list of model db files to read in.
    pathList = popanalysis.get_model_paths(args.modelDir)
    print '%d models to read in.' % len(pathList)

    for path in pathList[:1000]:
        # Read in data to dictionary
        thisMCDict = rvinout.load_data(path, silent=True, returnAsDict=True)
        thisMCEOI = [fiber['eoveri'] for fiber in thisMCDict.values()]

        # Compute likelihood of MC e/i to real e/i

        # Overplot this e/o histogram
        popanalysis.overplot_eoi_hist(thisMCEOI, fig.gca())
    print 'Finished plotting models.'

    # Overplot the real e/i data.
    popanalysis.overplot_eoi_hist(realEOI, fig.gca(), color='DodgerBlue', alpha=1.0)
    print 'Finished plotting real e/i.'

    # Save figure
    plotPath = os.path.join(args.plotDir, args.plotName)
    popanalysis.save_figure(fig, plotPath)
    print 'Done.'


if __name__ == "__main__":
    main()
