#!/usr/bin/env python
"""
Move MCMC plots from one directory to another.  Great for selecting 
specific fibers to look at.
"""

import os, shutil, optparse

from multiplicity.markov import mcinout


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='directory for writing plots')
    parser.add_option('-c', '--copyDir', dest='copyDir', action='store', type=str, default=None, help='dir contaning fiber sub_dir and plots to be coppied (optional)')
    parser.add_option('-f', '--fiberlist', dest='fiberlist', action='store', type=str, default=None, help='path containing list of fibers to move plots for')
    args = parser.parse_args()[0]


    fiberlist = mcinout.load_fiberlist(args.fiberlist)

    if not os.path.isdir(args.plotDir):
        raise Exception('%s not found.' % args.plotDir)
        
    for pmf in fiberlist:
        thisTargetDir = os.path.join(args.copyDir, pmf)
        if not os.path.isdir(thisTargetDir):
            print '%s not found.' % thisTargetDir
            continue

        thisPlotDir = os.path.join(args.plotDir, pmf)
        if not os.path.isdir(thisPlotDir):
            os.mkdir(thisPlotDir)

        src1 = os.path.join(thisTargetDir, '%s-triangle.png' % pmf)
        src2 = os.path.join(thisTargetDir, 'orbit_comparison.png')
        src3 = os.path.join(thisTargetDir, 'orbit_comparison_folded.png')

        dest1 = os.path.join(thisPlotDir, '%s-triangle.png' % pmf)
        dest2 = os.path.join(thisPlotDir, '%s-orbit_comparison.png' % pmf)
        dest3 = os.path.join(thisPlotDir, '%s-orbit_comparison_folded.png' % pmf)

        try:
            shutil.copy(src1, dest1)
        except:
            print 'failed to copy %s --> %s' % (src1, dest1)
        try:
            shutil.copy(src2, dest2)
        except:
            print 'failed to copy %s --> %s' % (src2, dest2)
        try:
            shutil.copy(src3, dest3)
        except:
            print 'failed to copy %s --> %s' % (src3, dest3)


if __name__ == "__main__":
    main()
