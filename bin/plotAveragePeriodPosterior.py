#!/usr/bin/env python
"""
Read in a isBinaryFraction file and trace file to create a histogram
of the average logP posterior for binaries in our sample.
"""

import optparse

from numpy import log

from multiplicity.figures import figures
from multiplicity.radialvelocity import rvinout
from multiplicity.markov import mcinout

USE_BINARY_TRACE_ONLY = True

def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='path to sqlite3 with RV and [Fe/H] data')
    parser.add_option('-f', '--fiberlistPath', dest='fiberlistPath', action='store', type=str, default=None, help='path to list of fibers to be considered')
    parser.add_option('-i', '--isBinaryPath', dest='isBinaryPath', action='store', type=str, default=None, help='path to isBinaryFraction values')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='directory for writing plots to')
    parser.add_option('-t', '--traceDir', dest='traceDir', action='store', type=str, default=None, help='directory containing traces')
    parser.add_option('-b', '--binaryThresh', dest='binaryThresh', action='store', type=float, default=None, help='threshhold for determining if star is binary')
    parser.add_option('--fehCut1', dest='fehCut1', action='store', type=float, default=None, help='first cut in metallicity')
    parser.add_option('--fehCut2', dest='fehCut2', action='store', type=float, default=None, help='second cut in metallicity. must be > first cut.')
    #parser.add_option('-a', '--loga', dest='loga', action='store_true', default=False, help='if flagged, plot amplitudes instead')
    args = parser.parse_args()[0]

    # Read in RV data, isBin data, and fiberlist
    dataDict = rvinout.load_data(args.dbPath, silent=True, returnAsDict=True)
    isBinaryDict = mcinout.load_isBinary_datafile(args.isBinaryPath)
    fiberlist = mcinout.load_fiberlist(args.fiberlistPath)

    thisDict = {}
    for i, fiberid in enumerate(fiberlist):
        if i % 1000 == 0:
            print '%d / %d' % (i, len(fiberlist))
        fraction = isBinaryDict[fiberid][-1]
        # Identify binaries
        if args.binaryThresh is not None:
            if fraction <= args.binaryThresh:
                continue
        thisDict[fiberid] = dataDict[fiberid]
        thisDict[fiberid]['isBinaryFraction'] = fraction
        # Grab traces
        tracePath = mcinout.find_path(args.traceDir, fiberid)
        traces = mcinout.load_trace(tracePath)
        if USE_BINARY_TRACE_ONLY:
            logPtrace = [t[1] for t in traces if t[4] > 0.5]
        else:
            logPtrace = [t[1] for t in traces]
        thisDict[fiberid]['logPtrace'] = logPtrace

    # Split up the group by Fe/H if necessary.
    if args.fehCut1 is not None:
        groupADict, groupBDict, tempDict = {}, {}, {}
        for pmf, data in thisDict.iteritems():
            if data['feh'] < args.fehCut1:
                groupADict[pmf] = data
            else:
                tempDict[pmf] = data
        if args.fehCut2 is None:
            groupBDict = tempDict
            dataDictionaryList = [groupADict, groupBDict]
            vlines = [args.fehCut1]
        else:
            groupCDict = {}
            if args.fehCut2 <= args.fehCut1:
                raise Exception('Invalid Fe/H cuts.')
            for pmf, data in tempDict.iteritems():
                if data['feh'] < args.fehCut2:
                    groupBDict[pmf] = data
                else:
                    groupCDict[pmf] = data
            dataDictionaryList = [groupADict, groupBDict, groupCDict]
            vlines = [args.fehCut1, args.fehCut2]
            print '%d low, %d mid, %d high' % (len(groupADict), len(groupBDict), len(groupCDict))
    else:
        dataDictionaryList = [dataDict]
        vlines = None

    # Plot
    figures.plot_isBinary_periods(dataDictionaryList, args.plotDir)


if __name__ == "__main__":
    main()

