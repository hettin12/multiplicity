#!/usr/bin/env python
"""
Produce a plot showing the binary fraction as a function of metallicity group
for each choice of -2lnK cutoff.
"""

import optparse

from numpy import log

from multiplicity.markov import mcinout
from multiplicity.radialvelocity import rvinout
from multiplicity.figures import figs_bayesFactor


def main():
    pass
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-b', '--bayesPath', dest='bayesPath', action='store', type=str, default=None, help='file with bayes factors in it')
    parser.add_option('-i', '--isBinaryPath', dest='isBinaryPath', action='store', type=str, default=None, help='file with isBinary values in it')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='sqlite3 file with rvs and stellar params')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='directory for writing plots')
    parser.add_option('--fehCut1', dest='fehCut1', action='store', type=float, default=None, help='first cut in metallicity')
    parser.add_option('--fehCut2', dest='fehCut2', action='store', type=float, default=None, help='second cut in metallicity. must be > first cut.')
    parser.add_option('--teffCut1', dest='teffCut1', action='store', type=float, default=None, help='first cut in temperature')
    parser.add_option('--teffCut2', dest='teffCut2', action='store', type=float, default=None, help='second cut in temperature. must be > first cut.')
    args = parser.parse_args()[0]

    # Read in bayes factors (or isBinary), metallicities and empirical errs
    dataDict = rvinout.load_data(args.dbPath, silent=False, returnAsDict=True)
    if args.bayesPath is not None:
        bayesFactorDict = mcinout.load_bayesfactor_datafile(args.bayesPath)
    if args.isBinaryPath is not None:
        isBinaryDict = mcinout.load_isBinary_datafile(args.isBinaryPath)

    # Append K, -2lnK, to the dataDict.
    if args.bayesPath is not None:
        for pmf, data in dataDict.iteritems():
            K = bayesFactorDict[pmf][2]
            dataDict[pmf]['K'] = K
            dataDict[pmf]['-2lnK'] = -2*log(K)
    if args.isBinaryPath is not None:
        for pmf, data in dataDict.iteritems():
            fraction = isBinaryDict[pmf][-1]
            dataDict[pmf]['isBinFrac'] = fraction

    # Split up the sample by Fe/H or Teff.
    if args.teffCut1 is not None:
        dataDictionaryList, vlines = rvinout.split_into_groups(dataDict, args.teffCut1, args.teffCut2, method='teff')
        dataDictionaryList = dataDictionaryList[::-1]
        vlines = None
    elif args.fehCut1 is not None:
        dataDictionaryList, vlines = rvinout.split_into_groups(dataDict, args.fehCut1, args.fehCut2, method='feh')
    else:
        dataDictionaryList = [dataDict]
        vlines = None

    # Make figures
    if args.teffCut1 is not None and args.isBinaryPath is not None:
        figs_bayesFactor.plot_binary_fractions_by_teff(dataDictionaryList, args.plotDir, cutoffList=[0.95, 0.80, 0.65], normalize=True)
    elif args.isBinaryPath is not None: 
        figs_bayesFactor.plot_binary_fractions(dataDictionaryList, args.plotDir, cutoffList=[0.95, 0.80, 0.65], normalize=True)


if __name__ == "__main__":
    main()
