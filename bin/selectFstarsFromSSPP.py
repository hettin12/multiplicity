#!/usr/bin/env python2.6
"""
Go through an SSPP FITS file and select F-type stars based on a criteria.
Write the final list to a PMF file and make a smaller SSPP FITS 
containing params for the selected stars.  F-type stars must also have valid
stellar parameters {Teff, logg, [Fe/H]}.

Input:  
    - the SSPP DRX parameters from FITS file.
    - path for writing list of PMFs
    - outpath for writing new fits file
    - flags specifing Hammer and/or Elodie method for identification

Output: 
    - a new FITS file containing entries of all F type stars.
    - PMF text file with list of all F-type PMF
"""

import optparse

from multiplicity.fits import fitsIO


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-s', '--ssppFits', dest='ssppFits', action='store', type=str, default=None, help='path to FITS for opening and selecting fibers.')
    parser.add_option('-o', '--outFits', dest='outFits', action='store', type=str, default=None, help='path to create new smaller FITS file with only selected fibers (optional)')
    parser.add_option('-f', '--fiberlistPath', dest='fiberlistPath', action='store', type=str, default=None, help='path to write a list of fiber PMFs (optional)')
    args = parser.parse_args()[0]

    # Open FITS and read in data
    cols, data = fitsIO.open_fits(args.ssppFits, printCols=True)

    # Select F-type stars
    FtypeIndex = fitsIO.select_Ftype(data, checkHammer=True, checkElodie=False, checkSubclass=True)

    # Write new, smaller FITS file
    if args.outFits is not None:
        fitsIO.write_FITS(FtypeIndex, data, args.outFits)

    # Write fiberlist PMF file
    if args.fiberlistPath is not None:
        fitsIO.write_fiberlist(FtypeIndex, data, args.fiberlistPath)


if __name__ == "__main__":
    main()
