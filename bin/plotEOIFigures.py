#!/usr/bin/env python
"""
Make various plots showing the e/i distributions for various fibers.  There are multiple figures.  Each figure is
a slice of stars with similar mean(errs).  In other words, all fibers in a given figure have i=I +/- dI.  Stars are
also divided equally into 3 groups by cuts in metallicity, so that each figure has 3 histograms.  Plots are made in 
both 'png' and 'eps' format, in both small (single column) and large sizes.

Input
    - RV database containing quality sample stars and the measurements.
    - Directory for plots

Output
    - Histogram plots for e/i
        - each plot has 3 histograms (cuts in Fe/H)
        - each plot contains a specific range in 'i' values

"""

import optparse

from multiplicity.radialvelocity import rvinout
from multiplicity.figures import figures


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='path to sqlite3 with RV data')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='directory for writing plots to')
    parser.add_option('--fehCut1', dest='fehCut1', action='store', type=float, default=None, help='cut1')
    parser.add_option('--fehCut2', dest='fehCut2', action='store', type=float, default=None, help='cut2')
    args = parser.parse_args()[0]

    dataDict = rvinout.load_data(args.dbPath, silent=True, returnAsDict=True)

    errList = [2.0, 3.0, 4.0, 5.0, 6.0, 7.0] # km/s
    errThresh = 1.0 # km/s 
    for err in errList:
        figures.plot_eoveri_by_i(dataDict, args.plotDir, err, errThresh)
        figures.plot_cumulative_eoveri(dataDict, args.plotDir, err, errThresh, args.fehCut1, args.fehCut2)
    figures.plot_cumulative_eoveri(dataDict, args.plotDir, 0.0, 20.0, args.fehCut1, args.fehCut2)


if __name__ == "__main__":
    main()

