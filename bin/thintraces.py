#!/usr/bin/env python
""" Given a list of fibers (and a trace directory), read in all traces,
randomly sample them to a specific size, and rewrite a new set of N traces.

Input:
    fiberid
    directory with original traces
    directory to write new trace to
    size of new trace
    (optional) headline for the column descriptions in the out file

Output:
    trace file for a fiber

"""

import os, sys, optparse

from multiplicity.markov import mcinout


def main():
    parser = optparse.OptionParser()
    parser.add_option('-f', '--fiberid', dest='fiberid', action='store', type=str, default=None, help='specific fiberid (overrides fiberlist)')
    parser.add_option('-t', '--traceDir', dest='traceDir', action='store', type=str, default=None, help='dir to find traces in')
    parser.add_option('-o', '--outDir', dest='outDir', action='store', type=str, default=None, help='dir to write new traces to')
    parser.add_option('-s', '--size', dest='size', action='store', type=int, default=50, help='number of samples written for new trace')
    parser.add_option('-H', '--headline', dest='headline', action='store', type=str, default=None, help='first line written to each trace file (col descriptions)')
    parser.add_option('-b', '--binaries', dest='binaries', action='store_true', default=False, help='only keep isBinary==True traces.')
    args = parser.parse_args()[0]
    fiberid = args.fiberid

    # Check for valid directories and file
    if not os.path.exists(args.outDir):
        raise Exception('Specify a valid outDir.  %s is not valid.' % args.outDir)
    if not os.path.exists(args.traceDir):
        raise Exception('Specify a valid traceDir.  %s is not valid.' % args.outDir)
    tracePath = mcinout.find_path(args.traceDir, fiberid)
    if tracePath is None:
        raise Exception('Could not find trace file for fiber %s' % fiberid)

    # Read orig tracefile and write new one
    traces = mcinout.load_trace(tracePath)
    if args.binaries:
        traces = [t for t in traces if t[4] > 0.5]
    outPath = os.path.join(args.outDir, '%s_%d_thinned-traces.dat' % (fiberid, args.size))
    mcinout.write_trace(traces, outPath, headline=args.headline, size=args.size)
    

if __name__ == "__main__":
    main()
