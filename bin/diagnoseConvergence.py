#!/usr/bin/env python
"""Run a Geweke diagnostic on a trace file to determine if it has converged.
If there is convergence, the z-scores should oscillate between -1 and 1.

Input:
    fiberid
    directory where trace is located
Output:
    zscores for a series of points along the chain
"""
import os, optparse

from multiplicity.markov import geweke, mcinout
from multiplicity.figures import figs_diagnostics

def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-f', '--fiberid', dest='fiberid', action='store', type=str, default=None, help='specific fiber to run')
    parser.add_option('-t', '--traceDir', dest='traceDir', action='store', type=str, default=None, help='dirrectory containing trace files for fibers')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='dir for plotting')
    args = parser.parse_args()[0]

    # Read in the chain traces
    tracePath = mcinout.find_path(args.traceDir, args.fiberid)
    traces = mcinout.load_trace(tracePath)

    # Remove the 'single' star traces
    cleanedTraces = [t[0:4] for t in traces if t[4] > 0.5]
    isBinaryTraces = [[t[4]] for t in traces]

    # Calculate zscores and plot
    zscores = geweke.geweke(cleanedTraces, first=.1, last=.5, intervals=20)
    plotPath = os.path.join(args.plotDir, '%s_geweke.png' % args.fiberid)
    figs_diagnostics.plot_geweke(zscores, plotPath, labels=['logA', 'logP', 'phi', 'offset'])

    zscores = geweke.geweke(isBinaryTraces, first=.1, last=.5, intervals=20)
    plotPath = os.path.join(args.plotDir, '%s_geweke_isBinary.png' % args.fiberid)
    figs_diagnostics.plot_geweke(zscores, plotPath, labels=['isBinary'])

if __name__ == "__main__":
    main()
