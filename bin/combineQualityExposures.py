#!/usr/bin/env python
"""
Select only quality exposures (SNR, pixel count) and make a new sqlite3
database file.  Take secondary fibers and place the exposure information into the
primary entry, so that there is only one row per star.

Input
    - Original sqlite RVDatabase with RV information
    - fiber manifest in the format
        # primary_pmf, pmf_2, pmf_3, ...
    - SNR cutoff
    - pixel count cutoff

Output
    - New sqlite RVDatabase with one row per star, only containing clean exposures.
    - PMF file with a list of all fibers still being used
    - a manifest file containing the list of fibers used for each star    
        # primary_pmf, pmf_2, pmf_3, ...
    - another manifest also containing the number of exposures used per fiber per star
        # primary_pmf, expUsed, pmf_2, expUsed_2, pmf_3, expUsed_3, ...
    - another manifest containing the taimids and plateid for each exposure
        # primary_pmf, taimid_1;pm_1, taimid_2;pm_1, taimid_3;pm_2, taimid4;pm_2
        # primary_pmf, pm_1;taimid_1;taimid_2;taimid_3, pm_2;taimid_4;taimid_5, ...
"""
import optparse

from multiplicity.radialvelocity import absoluterv


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='path to SQL file to read from')
    parser.add_option('-f', '--fiberManifestPath', dest='fiberManifestPath', action='store', type=str, default=None, help='file with a list of pmfs used per star (first pmf is primary)')
    parser.add_option('-o', '--outDir', dest='outDir', action='store', type=str, default=None, help='directory to write files to (new DB, pmf, directories)')
    parser.add_option('-s', '--snrMin', dest='snrMin', action='store', type=float, default=20.0, help='min SNR to keep exposures')
    parser.add_option('-p', '--pixelMin', dest='pixelMin', action='store', type=int, default=3000, help='min pixel count to keep exposures')
    args = parser.parse_args()[0]

    absoluterv.quality_combine(args.dbPath, args.outDir, args.fiberManifestPath, args.snrMin, args.pixelMin)


if __name__ == "__main__":
    main()
