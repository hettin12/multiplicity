#!/usr/bin/env python
"""
Creates a list of PMFs, with one line per unique star.  Input required is a CSV
of fiber information in the following format:

#starID, primary_specobjid, primary_plate, primary_mjd, primary_fiberid, this_specobjid, this_plate, this_mjd, this_fiberid

Input:
    -CSV of fiber information
    -filename for output

Output:
    -CSV with rows of variable length with format:
        # primary_pmf, pmf_2, pmf_3, pmf_4, ...
"""

import optparse


def read_csv(inPath):
    """Read in the CSV and make a dictionary with primary PMF as the key and a list
    of PMFs as the value.  Return the dictionary."""
    inFile = open(inPath, 'r')

    fiberDict = {}
    for line in inFile.readlines():
        if line[0] == '#': continue
        tup = line.split(',')

        prim_plate = int(tup[2])
        prim_mjd = int(tup[3])
        prim_fiber = int(tup[4])
        prim_pmf = '%04d-%05d-%03d' % (prim_plate, prim_mjd, prim_fiber)

        plate = int(tup[6])
        mjd = int(tup[7])
        fiber = int(tup[8])
        pmf = '%04d-%05d-%03d' % (plate, mjd, fiber)

        if prim_pmf in fiberDict:
            fiberDict[prim_pmf].append(pmf)
        else:
            fiberDict[prim_pmf] = [pmf]

    inFile.close()

    return fiberDict


def write_csv(outPath, fiberDict):
    """Write the PMF lists to a file with each fiber separated by a
    comma and each star separated by a line."""
    outFile = open(outPath, 'w')

    for prim_pmf, pmfList in fiberDict.iteritems():
        if prim_pmf not in pmfList:
            continue
        outFile.write('%s' % prim_pmf)
        for pmf in pmfList:
            if pmf == prim_pmf:
                continue
            outFile.write(',%s' % pmf)
        outFile.write('\n')

    outFile.close()


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-o', '--outPath', dest='outPath', action='store', type=str, default=None, help='path to create output of specobjid list')
    parser.add_option('-i', '--inPath', dest='inPath', action='store', type=str, default=None, help='path with list of pmfs')
    args = parser.parse_args()[0]

    fiberDict = read_csv(args.inPath)
    write_csv(args.outPath, fiberDict)


if __name__ == "__main__":
    main()
