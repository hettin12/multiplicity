#!/usr/bin/env python
"""
Given a list of fiber PMFs, create a file containing the list of objects that are on found on the disk.

Input:
    - list of pmfs
    - filename for output

Output:
    - a list of fiber PMFs (subset of input) that are found on the disk.

"""
import optparse

from multiplicity.fits import fitsIO

def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-o', '--outPath', dest='outPath', action='store', type=str, default=None, help='path to create output of specobjid list')
    parser.add_option('-i', '--inPath', dest='inPath', action='store', type=str, default=None, help='path with list of pmfs')
    args = parser.parse_args()[0]

    outFile = open(args.outPath, 'w')
    with open(args.inPath, 'r') as inFile:
        for line in inFile.readlines():
            if line[0] == '#':
                continue
            plate, mjd, fiber = line.strip().split(',')

            if fitsIO.fiber_path_exists(int(plate), int(mjd), int(fiber)):
                outFile.write('%s\n' % line.strip())
            else:
                print '%s not found' % line.strip()
    outFile.close()


if __name__ == "__main__":
    main()
