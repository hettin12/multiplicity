#!/usr/bin/env python
"""
Given a list of fiberids, clean the spectra and coadd them together,
to create a single template to be used as a correlation template.  Write
the wavelength, flux, variance to a text file.

Input
    - fiberlist PMF file with list of input fibers
    - filename for writing new template file
    - (optional) plot dir for plotting the template

Output
    - data file containing spectral information:  Wavelength  Flux  Variance
    - (optional) plot of template spectrum

"""
import os, optparse

from multiplicity.templates import templategen
from multiplicity.markov import mcinout


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-f', '--fiberlistPath', dest='fiberlistPath', action='store', type=str, default=None, help='list of fibers used as input to template')
    parser.add_option('-o', '--outPath', dest='outPath', action='store', type=str, default=None, help='specify filename to write new template file')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='specify plot dir to make plots (optional)')
    parser.add_option('-s', '--silent', dest='silent', action='store_true', default=False, help='silece printing to screen.')
    parser.add_option('-r', '--regionSet', dest='regionSet', action='store', type=str, default=None, help='set of wavelength regions to use for patching')
    args = parser.parse_args()[0]

    # Read fiber list
    if not args.silent:
        print 'Reading fiberlist...'
    fiberlist = mcinout.load_fiberlist(args.fiberlistPath)

    # Create template
    templateLamb, templateFlux, templateVar = templategen.generate_template(fiberlist, verbose=(not args.silent), regionSet=args.regionSet)

    # Write template to file
    if not args.silent:
        print 'Writing...'
    with open(args.outPath, 'w') as outFile:
        for tup in zip(templateLamb, templateFlux, templateVar):
            outFile.write('%.6f   %.6f   %.8f\n' % (tup[0], tup[1], tup[2]))

    # Plotting
    if args.plotDir is not None:
        if not args.silent:
            print 'Plotting...'
        plotName = os.path.basename(args.outPath) + '.png'
        plotPath = os.path.join(args.plotDir, plotName)
        fig = templategen.plot_template(templateLamb, templateFlux, templateVar)
        fig.savefig(plotPath)


if __name__ == "__main__":
    main()
