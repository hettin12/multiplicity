#!/usr/bin/env python
"""
Plot population statistics (e.g. triangle plot) for the fiber population,
looking at the maximum likelihood trace for each fiber.

Input
    File containing 'best binary' and 'best single' orbit information for each fiber.  Use the format:
            # fiberid   logA   logP   phi   b_binary   L_binary   lnL_binary   BIC_binary   offset   L_single   lnL_single   BIC_single   (BIC_binary-BIC_single)
    Database file with metallicities and other stellar parameters
    Plot directory
    (optional) upper limit on (BIC_binary-BIC_single) to allow fiber in analysis
    (optional) lower limit on (BIC_binary-BIC_single) to allow fiber in analysis

Output
    Triangle plot for orbital parameters, likelihoods, DBIC, and metallicity.

"""

import os, optparse

from multiplicity.markov import mcinout, mcescher
from multiplicity.radialvelocity import rvinout

IGNORE_L = True  # After making enough cuts, all single_likelihoods have the 'same' value of nearly 0.0.

def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-b', '--bestModelsPath', dest='bestModelsPath', action='store', type=str, default=None, help='path to file for containing best orbits')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='database to read RV/stellar params from')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='dir for plotting orbit comparison')
    parser.add_option('-u', '--upperLimit', dest='upperLimit', action='store', type=float, default=None, help='largest allowed DBIC value to include fiber (optional)')
    parser.add_option('-l', '--lowerLimit', dest='lowerLimit', action='store', type=float, default=None, help='smallest allowed DBIC value to include fiber (optional)')
    args = parser.parse_args()[0]

    uL = args.upperLimit
    lL = args.lowerLimit

    # Read in best binary/single orbit
    dataDict = mcinout.load_likelihood_datafile(args.bestModelsPath)

    # Get db info for feh
    rvDataDict = rvinout.load_data(args.dbPath, silent=True, returnAsDict=True)

    # Append the metallicity and remove fibers based on DBIC cuts
    cleanedDataDict = {}
    for fid, tup in dataDict.iteritems():
        if uL is not None or lL is not None:
            thisDBIC = tup[11]
            if uL is not None:
                if thisDBIC > uL:
                    continue
            if lL is not None:
                if thisDBIC < lL:
                    continue
        if IGNORE_L:
            tup = tuple([tup[i] for i in range(len(tup)) if i != 8])

        feh = rvDataDict[fid]['feh']
        cleanedDataDict[fid] = tup + (feh,)

    # Make plots
    valueTupleList = [cleanedDataDict[k] for k in cleanedDataDict]
    if IGNORE_L:
        labels = ['logA', 'logP', '$\phi$', 'b', 'L_binary', 'lnL_binary', 'BIC_binary', 'offset', 'lnL_single','BIC_single', 'DBIC(binary-single)', 'feh']
        extents = [1.0, 1.0, 1.0, 1.0, (0.0,0.004), (-40,0), (0,60), 1.0, (-40,0), (0,60), (-50,20), 1.0]
    else:
        labels = ['logA', 'logP', '$\phi$', 'b', 'L_binary', 'lnL_binary', 'BIC_binary', 'offset', 'L_single', 'lnL_single','BIC_single', 'DBIC(binary-single)', 'feh']
        extents = [1.0, 1.0, 1.0, 1.0, (0.0,0.004), (-40,0), (0,60), 1.0, (0.0,0.004), (-40,0), (0,60), (-50,20), 1.0]

    mcescher.plot_parameter_triangle(valueTupleList, plotDir=args.plotDir, baseName='DBIC_%s_%s_All' % (lL, uL), labels=labels, truths=None, quantiles=[], extents=None)
    mcescher.plot_parameter_triangle(valueTupleList, plotDir=args.plotDir, baseName='DBIC_%s_%s_cropped' % (lL, uL), labels=labels, truths=None, quantiles=[], extents=extents)


if __name__ == "__main__":
    main()
