#!/usr/bin/env python
"""
Make various plots summarizing the sample.  Plots are made in both 'png' and 'eps' format,
in both small (single column) and large sizes.

Input
    - RV database containing quality sample stars and the measurements.
    - Directory for plots

Output
    - Histogram plots for
        - Stellar params ([Fe/H], Teff, logg, g-Magnitude)
        - Exposure info (mean(SNR), std(SNR), (t_f - t_i), nExp) 
        - Measurements (mean(RV), std(RV), mean(err), std(err))
        - e/i

    - Scatter plots for
        - e/i vs. [Fe/H]
        - nExp vs. [Fe/H]

    - Triangle plot showing Y vs. X for all variables
"""

import optparse

from multiplicity.radialvelocity import rvinout
from multiplicity.figures import figures


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='path to sqlite3 with RV data')
    parser.add_option('-m', '--multi', dest='multi', action='store_true', default=False, help='whether to plot data split by groups or not')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='directory for writing plots to')
    parser.add_option('--fehCut1', dest='fehCut1', action='store', type=float, default=None, help='first cut in metallicity')
    parser.add_option('--fehCut2', dest='fehCut2', action='store', type=float, default=None, help='second cut in metallicity. must be > first cut.')
    parser.add_option('--teffCut1', dest='teffCut1', action='store', type=float, default=None, help='first cut in temperature')
    parser.add_option('--teffCut2', dest='teffCut2', action='store', type=float, default=None, help='second cut in temperature. must be > first cut.')
    args = parser.parse_args()[0]

    dataDict = rvinout.load_data(args.dbPath, silent=True, returnAsDict=True)

    # Split up the group by Fe/H or Teff if necessary.
    if args.teffCut1 is not None:
        dataDictionaryList, vlines = rvinout.split_into_groups(dataDict, args.teffCut1, args.teffCut2, method='teff')
        dataDictionaryList = dataDictionaryList[::-1]
        vlines = None
    elif args.fehCut1 is not None:
        dataDictionaryList, vlines = rvinout.split_into_groups(dataDict, args.fehCut1, args.fehCut2, method='feh')
    else:
        dataDictionaryList = [dataDict]
        vlines = None

    # Make figures
    if args.multi:
        figures.plot_all_stellar_params(dataDictionaryList, args.plotDir)
        figures.plot_all_exposure_info(dataDictionaryList, args.plotDir)
        figures.plot_all_measurement_info(dataDictionaryList, args.plotDir)
        figures.plot_all_scatter(dataDictionaryList, args.plotDir)
        figures.plot_paper_figs(dataDictionaryList, args.plotDir)
    else:
        figures.plot_all_stellar_params([dataDict], args.plotDir, vlines=vlines)
        figures.plot_all_exposure_info([dataDict], args.plotDir)
        figures.plot_all_measurement_info([dataDict], args.plotDir)
        figures.plot_all_scatter([dataDict], args.plotDir)
        figures.plot_triangle(dataDict, args.plotDir)

if __name__ == "__main__":
    main()
