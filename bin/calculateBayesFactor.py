#!/usr/bin/env python
"""
Read in the trace likelihoods for a fiber (both single and binary), then compute the 
model likelihood by summing over all traces.

Take the ratio of the Bayes factor for the binary model against
the single-star model.

This assumes the sum over traces is equivalent to the integration over parameter space.
Also, for now this assumes the length of the traces are equal.

Input:
    fiberid
    tracedir for single-star traces (with likelihoods)
    tracedir for binary traces (with likelihoods)
    
Output:
    the posterior probability for single P(M_s | D)
    the posterior probability for binary P(M_b | D)
    Bayes factor, K  =  P(M_b | D)  /  P(M_s | D)
    (optional) outpath for appending these values to


K-value         Strength of evidence
----------      --------------------
< 1             supports single-star
  1 -- 3        barely worth mentioning binary
  3 -- 10       substantial for binary
 10 -- 30       strong for binary
 30 -- 100      very strong for binary
    > 100       decisive

"""

import os, optparse

from multiplicity.markov import mcinout, likelihood


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-f', '--fiberid', dest='fiberid', action='store', type=str, default=None, help='specific fiber to run')
    parser.add_option('-s', '--singleTraceDir', dest='singleTraceDir', action='store', type=str, default=None, help='dir with likelihood trace files for single')
    parser.add_option('-b', '--binaryTraceDir', dest='binaryTraceDir', action='store', type=str, default=None, help='dir with likelihood trace files for binary')
    parser.add_option('-o', '--outPath', dest='outPath', action='store', type=str, default=None, help='file to append fiber bayes factors')
    args = parser.parse_args()[0]

    # locate and open single trace file
    singleTracePath = mcinout.find_path(args.singleTraceDir, args.fiberid)
    singleTraces = mcinout.load_trace(singleTracePath)
    #singleLikelihoods = [t[1] for t in singleTraces]
    singlelnL = [t[2] for t in singleTraces]

    # locate and open binary trace file
    binaryTracePath = mcinout.find_path(args.binaryTraceDir, args.fiberid)
    binaryTraces = mcinout.load_trace(binaryTracePath)
    #binaryLikelihoods = [t[4] for t in binaryTraces]
    binarylnL = [t[5] for t in binaryTraces]

    #if len(singleLikelihoods) != len(binaryLikelihoods):
    if len(singlelnL) != len(binarylnL):
        raise Exception('Please be sure the size of each likelihood file is equal.')

    # calculate posterior for each model and the Bayes factor, K
    singlePosterior = likelihood.calc_posterior(singlelnL)
    binaryPosterior = likelihood.calc_posterior(binarylnL)
    bayesFactor = likelihood.calc_bayes_factor(binaryPosterior, singlePosterior)

    # append to file
    if args.outPath is not None:
        with open(args.outPath, 'a') as outFile:
            outFile.write('%s  %e  %e  %.4f\n' % (args.fiberid, binaryPosterior, singlePosterior, bayesFactor))
    else:
        print 'Pr(binary) / Pr(single) = %e / %e = %.5f' % (binaryPosterior, singlePosterior, bayesFactor)

if __name__ == "__main__":
    main()

