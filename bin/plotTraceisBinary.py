#!/usr/bin/env python
"""
Read in isBinary Fractions for fibers and plot against metallicity.
"""
import os, optparse

from numpy import log, log10, isfinite

from multiplicity.markov import  mcinout
from multiplicity.radialvelocity import rvinout
from multiplicity.figures import figs_bayesFactor, figs_scatter_all

def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-i', '--isBinaryPath', dest='isBinaryPath', action='store', type=str, default=None, help='file with isBinary data in it')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='sqlite3 file with rvs and stellar params')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='directory for writing plots')
    parser.add_option('-b', '--bayesPath', dest='bayesPath', action='store', type=str, default=None, help='file with bayes factors in it (optional)')
    parser.add_option('--fehCut1', dest='fehCut1', action='store', type=float, default=None, help='first cut in metallicity')
    parser.add_option('--fehCut2', dest='fehCut2', action='store', type=float, default=None, help='second cut in metallicity. must be > first cut.')
    args = parser.parse_args()[0]

    # Read in isBinary fractions, metallicities and empirical errs
    isBinaryDict = mcinout.load_isBinary_datafile(args.isBinaryPath)
    dataDict = rvinout.load_data(args.dbPath, silent=False, returnAsDict=True)

    # Append isBinary values to dataDict.
    for pmf, data in dataDict.iteritems():
        isBinaryFraction = isBinaryDict[pmf][-1]
        dataDict[pmf]['isBinaryFraction'] = isBinaryFraction

    # Append bayesFactor values to dataDict if necessary.
    if args.bayesPath is not None:
        bayesFactorDict = mcinout.load_bayesfactor_datafile(args.bayesPath)
        for pmf, data in dataDict.iteritems():
            K = bayesFactorDict[pmf][2]
            dataDict[pmf]['K'] = K
            lnK = -2*log(K)
            if not isfinite(lnK):
                lnK = -3500.
            lnK -= 10
            dataDict[pmf]['-2lnK'] = lnK

    # Split up the sample by Fe/H or Teff.
    if args.fehCut1 is not None:
        dataDictionaryList, vlines = rvinout.split_into_groups(dataDict, args.fehCut1, args.fehCut2, method='feh')
    else:
        dataDictionaryList = [dataDict]
        vlines = None

    # Make figures
    figs_bayesFactor.plot_multiple_isBinary_histogram(dataDictionaryList, args.plotDir)
    if args.bayesPath is not None:
        figs_scatter_all.plot_lnk_vs_isBinaryFraction_scatter(dataDict, args.plotDir)

if __name__ == "__main__":
    main()
