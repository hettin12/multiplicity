#!/usr/bin/env python
"""
Locate the fibers that are likely to be binary by finding the fibers that have Delta_BIC values
in favor of the binary orbit of the single orbit.

Input:
    datafile containing all fibers and their likelihood values
        #   key      0       1     2        3         4           5            6          7         8           9           10                 11
        # fiberid   logA   logP   phi   b_binary   L_binary   lnL_binary   BIC_binary   offset   L_single   lnL_single   BIC_single   (BIC_binary-BIC_single)
    Delta_BIC threshold limits, below wich fibers are chosen. (More negative is more likely binary)
    outPath for fiber PMFs to be written to
    (optional) directory containing fiber plots
    (optional) directory to make copies of fiber plots

Ouput
    one file containing a list of all fiber (PMF,DBIC) that meet the Delta_BIC criteria

"""
import os, optparse, shutil

from multiplicity.markov import mcinout


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-b', '--bestModelsPath', dest='bestModelsPath', action='store', type=str, default=None, help='path to file for containing best orbits and likelihoods')
    parser.add_option('-o', '--outPath', dest='outPath', action='store', type=str, default=None, help='path to outfile containing all pmf values that fit')
    parser.add_option('-u', '--upperLimit', dest='upperLimit', action='store', type=float, default=0.0, help='largest allowed DBIC value to include fiber')
    parser.add_option('-l', '--lowerLimit', dest='lowerLimit', action='store', type=float, default=-1E10, help='smallest allowed DBIC value to include fiber (optional)')
    parser.add_option('-c', '--copyDir', dest='copyDir', action='store', type=str, default=None, help='dir contaning fiber sub_dir and plots to be coppied (optional)')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='dir for moving fiber plots to (optional)')
    args = parser.parse_args()[0]

    # Read likelihood data file
    dataDict = mcinout.load_likelihood_datafile(args.bestModelsPath)
    
    # Find binaries based on thresh
    binaryList = []
    for fid, tup in dataDict.iteritems():
        if tup[11] <= args.upperLimit and tup[11] > args.lowerLimit:
            binaryList.append((fid, tup[11]))
    binaryList.sort(key=lambda x: x[0])

    # Write list of PMF to outfile
    with open(args.outPath, 'w') as outFile:
        for tup in binaryList:
            outFile.write('%s, %f\n' % (tup[0], tup[1]))

    # Copy relavent plots to a new directory
    if args.copyDir is not None and args.plotDir is not None:
        if not os.path.isdir(args.plotDir):
            os.mkdir(args.plotDir)

        for tup in binaryList:
            pmf = tup[0]

            thisTargetDir = os.path.join(args.copyDir, pmf)
            if not os.path.isdir(thisTargetDir):
                print '%s not found.' % thisTargetDir
                continue

            src1 = os.path.join(thisTargetDir, '%s-triangle.png' % pmf)
            src2 = os.path.join(thisTargetDir, 'orbit_comparison.png')
            src3 = os.path.join(thisTargetDir, 'orbit_comparison_folded.png')

            dest1 = os.path.join(args.plotDir, '%s-triangle.png' % pmf)
            dest2 = os.path.join(args.plotDir, '%s-orbit_comparison.png' % pmf)
            dest3 = os.path.join(args.plotDir, '%s-orbit_comparison_folded.png' % pmf)

            try:
                shutil.copy(src1, dest1)
            except:
                print 'failed to copy %s --> %s' % (src1, dest1)
            try:
                shutil.copy(src2, dest2)
            except:
                print 'failed to copy %s --> %s' % (src2, dest2)
            try:
                shutil.copy(src3, dest3)
            except:
                print 'failed to copy %s --> %s' % (src3, dest3)


if __name__ == "__main__":
    main()
