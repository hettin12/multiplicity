#!/usr/bin/env python
"""
Run an MCMC chain using the 'emcee' module.  

Input:
    database with rv information
    directory for writing trace files
    list of fibers to include in the population
    path to a config file with (walkers, steps, burn, thin, priorLimits)   See: multiplicity.markov.mcinout
    (optional) list of which fibers are on which plates (for reducing systematics)
    (optional) directory for writing plots
    (optional) verbosity in output to screen

Output:
    trace file containing one step per line and the parameters tried for that step
    summary file with run information
    (optional) plots of the trace history, triangle plot of traces, rv_curves

"""
import os, optparse

import emcee
from matplotlib.pyplot import hist
import numpy as np

from multiplicity.markov import mcinout, popMCMC, mcescher

THREADS = 50


def main():
    parser = optparse.OptionParser(description='emcee runner. Written by Thomas Hettinger.')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, help='path to file with fiber data')
    parser.add_option('-o', '--outDir', dest='outDir', action='store', type=str, default=None, help='dir to write traces to')
    parser.add_option('-f', '--fiberlistPath', dest='fiberlistPath', action='store', type=str, default=None, help='list of fibers to model')
    parser.add_option('-c', '--configPath', dest='configPath', action='store', type=str, default=None, help='config file with parameter priors')
    parser.add_option('-m', '--method', dest='method', action='store', type=str, default='continuous', help='method for likelihood comparison (binned, continuous)')
    parser.add_option('-i', '--plateidPath', dest='plateidPath', action='store', type=str, default=None, help='file with plateid indicies')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='base dir for plots')
    parser.add_option('-s', '--starCount', dest='starCount', action='store', type=int, default=None, help='select first s stars in the fiberlist.')
    parser.add_option('-e', '--eoiBinCutoff', dest='eoiBinCutoff', action='store', type=int, default=None, help='number of bins to ignore in lnL calculation')
    parser.add_option('--progPath', dest='progPath', action='store', type=str, default=None, help='path to file to output current progress of chain.')
    parser.add_option('--binCount', dest='binCount', action='store', type=int, default=20, help='number of bins for the binned lnL method.')
    parser.add_option('--histRange_low', dest='histRange_low', action='store', type=float, default=0.0, help='e/i range for the binned lnL method.')
    parser.add_option('--histRange_high', dest='histRange_high', action='store', type=float, default=20.0, help='e/i range for the binned lnL method.')
    args = parser.parse_args()[0]

    # Read config files
    configDict = mcinout.load_chainconfig(args.configPath)
    nwalkers = configDict['walkers']
    nsteps = configDict['steps']
    nburn = configDict['burn']
    nthin = configDict['thin']
    multiplier = configDict['modelMultiplier']
    baseName = "%dw_%ds_%dt_%db_%dm_%dc" % (nwalkers, nsteps, nthin, nburn, multiplier, args.eoiBinCutoff)

    # Create prior limits
    priorLimits = {'fbin':configDict['fbin'],
                   'alpha':configDict['alpha'],}
    ndim = 2

    # Get the RV data from sqlite3 file
    tobs, errs, EOIs = mcinout.load_pop_exp_details(args.dbPath, args.fiberlistPath)

    # TEMPORARILY MAKE THE DATA SIZE SMALLER
    if args.starCount is not None:
        tobs = tobs[:args.starCount]
        errs = errs[:args.starCount]
        EOIs = EOIs[:args.starCount]

    # Set up the sampler and get walkers
    if args.method == 'binned':
        histRange = [args.histRange_low, args.histRange_high]
        binHeights, __, __ = hist(EOIs, args.binCount, histRange)
        exposureInfo = zip(tobs, errs)
        sampler = emcee.EnsembleSampler(nwalkers, ndim, popMCMC.lnprob_binned, args=(exposureInfo, args.binCount, histRange, binHeights, priorLimits, multiplier, args.eoiBinCutoff), threads=THREADS)
    elif args.method == 'continuous':
        sampler = emcee.EnsembleSampler(nwalkers, ndim, popMCMC.lnprob_continuous, args=(tobs, errs, EOIs, priorLimits))
    pos = popMCMC.get_walkers(nwalkers, priorLimits)

    # Run the chain
    print 'Running the chain (%d walkers, %d steps, %d burn, %d thin) for %d stars...' % (nwalkers, nsteps, nburn, nthin, len(EOIs))
    timeElapsed = popMCMC.run_chain(sampler, pos, nwalkers, nsteps, nthin, verbose=False, progPath=args.progPath)
    print 'Time Elapsed = %f' % timeElapsed

    # Get the traces
    burned_traces = sampler.chain[:, nburn:, :].reshape((-1, ndim))
    totalTraceCount = len(burned_traces)
    #all_traces = sampler.chain[:, :, :].reshape((-1, ndim))

    # Create labels
    labels = ['fbin', 'alpha']
    headline = '#'
    for label in labels:
        headline += '  %s' % label
    headline += '\n'

    # Write the traces
    if args.outDir is not None:
        try:
            acor = sampler.acor
        except:
            acor = [-9999 for i in range(ndim)]
        outPath = os.path.join(args.outDir, '%s-traces.dat' % baseName)
        mcinout.write_trace(burned_traces, outPath, headline=headline)
        outPath = os.path.join(args.outDir, '%s-summary.dat' % baseName)
        mcinout.write_summary(outPath, nwalkers, nsteps, nthin, nburn, totalTraceCount, np.mean(sampler.acceptance_fraction), acor, timeElapsed, labels=labels)

    # Make plots
    if args.plotDir is not None:
        # Plot traces, triangle plot, sample plot
        mcescher.plot_popMCMC_samples(burned_traces, exposureInfo, EOIs, args.binCount, histRange, plotDir=args.plotDir, baseName=baseName, size=100, modelMultiplier=multiplier, eoiBinCutoff=args.eoiBinCutoff)
        mcescher.plot_chain_traces(sampler.chain, plotDir=args.plotDir, baseName=baseName, nburn=nburn, labels=labels)
        mcescher.plot_parameter_triangle(burned_traces, plotDir=args.plotDir, baseName=baseName, labels=labels)


if __name__ == "__main__":
    main()
