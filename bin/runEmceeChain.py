#!/usr/bin/env python
"""
Run an MCMC chain using the 'emcee' module.  

For a single fiber, set up the chain and run it.

Input:
    fiberid
    database with rv information
    directory for writing trace files
    path to a config file with (walkers, steps, burn, thin, priorLimits)   See: multiplicity.markov.mcinout
    optional flag to specify a single-star fun only, default is binary star.
    (optional) directory for writing plots
    (optional) verbosity in output to screen

Output:
    trace file containing one step per line and the parameters tried for that step
    summary file with run information
    (optional) plots of the trace history, triangle plot of traces, rv_curves

"""

import os, optparse

import emcee
import numpy as np

from multiplicity.markov import mcescher, mcinout, mcmc


def main():
    parser = optparse.OptionParser(description='emcee runner. Written by Thomas Hettinger.')
    parser.add_option('-f', '--fiberid', dest='fiberid', action='store', type=str, help='name of fiber to run test on')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, help='path to file with fiber data')
    parser.add_option('-o', '--outDir', dest='outDir', action='store', type=str, default=None, help='dir to write traces to')
    parser.add_option('-c', '--configPath', dest='configPath', action='store', type=str, default=None, help='config file with parameter priors')
    parser.add_option('-i', '--plateidPath', dest='plateidPath', action='store', type=str, default=None, help='file with plateid indicies')
    parser.add_option('-m', '--mixture', dest='mixture', action='store_true', default=False, help='run mixture model with outlier params')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='base dir for plots')
    parser.add_option('-v', '--verbose', dest='verbose', action='store_true', default=False, help='output more data to terminal')
    args = parser.parse_args()[0]

    mcmc.OUTLIER_DETECTION = args.mixture

    # Read config files
    configDict = mcinout.load_chainconfig(args.configPath)
    nwalkers = configDict['walkers']
    nsteps = configDict['steps']
    nburn = configDict['burn']
    nthin = configDict['thin']
    baseName = "%s_%d_%d_%d" % (args.fiberid, nwalkers, nsteps, nthin)

    if args.plateidPath is not None:
        plateidData = mcinout.load_plateids(args.plateidPath, args.fiberid)
        #print args.fiberid, plateidData
        plateCount = len(plateidData)
    else:
        plateCount = 1

    # Create prior limits
    priorLimits = {'logA':configDict['logA'], 'logP':configDict['logP'],
                   'phi':configDict['phi'], 'b':configDict['b'],
                   'isBinary':configDict['isBinary'], 'ps':(-20.0, 20.0), }
    if args.mixture:
        priorLimits['Pb'] = configDict['Pb']
        priorLimits['Yb'] = configDict['Yb']
        priorLimits['Vb'] = configDict['Vb']

    # Calculate number of dimensions
    ndim = 5
    if args.mixture:
        ndim += 3
    ndim += plateCount-1

    # Get rv data from sqlite3 file
    all_tobs, all_rvs, all_errs = mcinout.load_fiber(args.dbPath, args.fiberid)
    all_tobs = all_tobs - all_tobs[0]

    if plateCount == 1:
        tobs = np.array(all_tobs)
        tobs = tobs.reshape([1, len(tobs)])
        rvs = np.array(all_rvs)
        rvs = rvs.reshape([1, len(rvs)])
        errs = np.array(all_errs)
        errs = errs.reshape([1, len(errs)])
    else:
        plate_tobs, plate_rvs, plate_errs = [], [], []
        for pm, idx in plateidData:
            plate_tobs.append(all_tobs[idx])
            plate_rvs.append(all_rvs[idx])
            plate_errs.append(all_errs[idx])
        tobs = np.array(plate_tobs)
        rvs = np.array(plate_rvs)
        errs = np.array(plate_errs)
    yvars = errs*errs

    # Set up the sampler and get walkers.
    sampler = emcee.EnsembleSampler(nwalkers, ndim, mcmc.lnprob, args=(tobs, rvs, yvars, priorLimits))
    pos = mcmc.get_walkers(rvs, ndim, nwalkers, priorLimits)

    # Clear and run the production chain.
    timeElapsed = mcmc.run_chain(sampler, pos, nwalkers, nsteps, nthin, args.verbose)

    # Get traces
    burned_traces = sampler.chain[:, nburn:, :].reshape((-1, ndim))
    totalTraceCount = len(burned_traces)
    #all_traces = sampler.chain[:, :, :].reshape((-1, ndim))

    # Create labels
    labels = ['logA', 'logP', 'phi', 'b', 'isBinary']
    if args.mixture:
        labels.extend(['Pb', 'Yb', 'Vb'])
    for p in range(plateCount-1):
        labels.append('ps_%d' % (p+1))
    headline = '#'
    for label in labels:
        headline += '  %s' % label
    headline += '\n'

    # Write traces.
    if args.outDir is not None:
        try:
            acor = sampler.acor
        except:
            acor = [-9999 for i in range(ndim)]
        nBinary = len(np.where(burned_traces.T[4] > 0.5)[0])
        binaryPercentage = float(nBinary) / float(totalTraceCount)

        outPath = os.path.join(args.outDir, '%s-traces.dat' % baseName)
        mcinout.write_trace(burned_traces, outPath, headline=headline)
        outPath = os.path.join(args.outDir, '%s-summary.dat' % baseName)
        mcinout.write_summary(outPath, nwalkers, nsteps, nthin, nburn, totalTraceCount, np.mean(sampler.acceptance_fraction), acor, timeElapsed, binaryPercentage, labels=labels)

    # Make plots if necessary
    if args.plotDir is not None:
        plotDir = os.path.join(args.plotDir, args.fiberid)
        if not os.path.isdir(plotDir):
            os.mkdir(plotDir)

        # Plot rv curve, traces, triangle plot, sample plot
        if args.plateidPath is not None:
            mcescher.plot_rvcurve(tobs, rvs, errs, plotDir, baseName)
        if plateCount == 1:
            mcescher.plot_sinusoid_samples(burned_traces, tobs, rvs, errs, plotDir=plotDir, baseName=baseName, dualModel=True)
        mcescher.plot_chain_traces(sampler.chain, plotDir=plotDir, baseName=baseName, nburn=nburn, labels=labels)
        mcescher.plot_parameter_triangle(burned_traces, plotDir=plotDir, baseName=baseName, labels=labels)
        mcescher.plot_parameter_triangle(burned_traces, plotDir=plotDir, baseName=baseName, labels=labels, include='binary')
        mcescher.plot_parameter_triangle(burned_traces, plotDir=plotDir, baseName=baseName, labels=labels, include='single')


if __name__ == "__main__":
    main()

