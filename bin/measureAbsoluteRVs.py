#!/usr/bin/env python
""" Execute the radial velocity testing for a fiber.  Templates 
    are selected based on the stellar parameters.  An SQL database
    is required for reading in data and writing back data.
"""

import os, optparse

from multiplicity.prepfiber import prepfiber
from multiplicity.sqlwrapper.Database import RVDatabase
from multiplicity.templates.Template import Template
from multiplicity.radialvelocity import absoluterv

PATCHES = {'F':prepfiber.F_STAR_REGIONS, 'G':prepfiber.G_STAR_REGIONS, 'K':prepfiber.K_STAR_REGIONS, 'M':prepfiber.M_STAR_REGIONS}


def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-f', '--fiberid', dest='fiberid', action='store', type=str, default=None, help='fiberid to examine')
    parser.add_option('-t', '--templatePath', dest='templatePath', action='store', type=str, help='path to template file with wave,flux,var') 
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='name of Database file to read/write to')
    parser.add_option('-r', '--regionSet', dest='regionSet', action='store', type=str, default=None, help='set of wavelength regions to use for patching')
    parser.add_option('-i', '--ignoreCR', dest='ignoreCR', action='store_true', default=False, help='ignore the CRSDSS mask bit when cleaning spectra')
    parser.add_option('-b', '--ignoreBADSKYCHI', dest='ignoreBADSKYCHI', action='store_true', default=False, help='ignore the BADSKYCHI mask bit when cleaning spectra')
    args = parser.parse_args()[0]

    # Load fiber
    fiber = prepfiber.get_Fiber(args.fiberid)
    if fiber is None:
        raise Exception('Fiber not found %s.' % args.fiberid)

    # Ignore bits if required, get clean spectra
    if args.ignoreCR or args.ignoreBADSKYCHI:
        names = []
        if args.ignoreCR:
            names.append('CRSDSS')        
        if args.ignoreBADSKYCHI:
            names.append('BADSKYCHI')
        if args.regionSet is None:
            cleanSpectra = prepfiber.clean_spectra(fiber.raw, trimSize=50, ignoreMaskNames=names)
        else:
            cleanSpectra = prepfiber.clean_spectra(fiber.raw, trimSize=50, ignoreMaskNames=names, patchRegions=PATCHES[args.regionSet])
    else:
        if args.regionSet is None:
            cleanSpectra = prepfiber.clean_spectra(fiber.raw, trimSize=50)
        else:
            cleanSpectra = prepfiber.clean_spectra(fiber.raw, trimSize=50, patchRegions=PATCHES[args.regionSet])

    # Prepare the template
    template = Template(templatePath=args.templatePath)
    resampledTemplate = template.resample(cleanSpectra[0].lamb)

    # Correlate
    absRVs, flags = absoluterv.calculate_RV(cleanSpectra, resampledTemplate)

    # Update SQL database.
    if args.dbPath is not None:
        # Check if SQL database file exists.  Create if necessary.
        if not os.path.exists(args.dbPath):
            db = RVDatabase(args.dbPath, create=True)
            del db
        goodPixelCounts = prepfiber.get_goodPixCount_after_normalizing(fiber.raw)
        absoluterv.update_database(args.fiberid, args.dbPath, fiber, absRVs, flags, goodPixelCounts)

    else:
        print 'RVs ', absRVs
        print 'errs ', absRV_errs
        print 'flags ', flags
        print '\n[] Finished.\n'


if __name__ == "__main__":
    main()

