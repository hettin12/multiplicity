#!/usr/bin/env python
""" 
Update a Database with the 'cleanExposureCount' and 'cleanExposureIndex'
based on a minimum pixel and snr value.  Record these in the database.
All rows in the database are checked.


Input:
    - RVDatabase sql file
    - min pixels per exposure (default 3000)
    - min snr per exposure (default 20)

Output:
    - The input RVDatabase will be updated
"""

import optparse

from multiplicity.radialvelocity import absoluterv

def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='path to SQL file to update')
    parser.add_option('-p', '--minPix', dest='minPix', action='store', type=int, default=3000, help='minimum pixels per good exposure')
    parser.add_option('-s', '--minSNR', dest='minSNR', action='store', type=float, default=20.0, help='minimum signal-to-noise per good exposure')
    args = parser.parse_args()[0]

    absoluterv.identify_clean_exposures(args.dbPath, minPix=args.minPix, minSNR=args.minSNR)


if __name__ == "__main__":
    main()

