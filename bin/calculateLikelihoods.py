#!/usr/bin/env python
"""
For a single fiber, calculate the likelihood for each trace and 
write out to a file.  Keep trace of the trace with the maximum likelihood.

Inputs:
    fiberid
    directory containing trace files
    database with RV info
    directory to write traces with likelihoods appended
    (optional) filename to append this fiber's best binary trace and best single trace (w/ likelihoods)
            # fiberid   logA   logP   phi   b_binary   L_binary   lnL_binary   BIC_binary   offset   L_single   lnL_single   BIC_single   (BIC_binary-BIC_single)
    (optional) plot directory to make orbit comaprison plot
    (optional) run on single star traces

Outputs:
    Trace file for this fiber with likelihoods appended
    (optional) appended "BIC.dat" file with best binary orbit and best single orbit, along with their likelihoods.
    (optional) plot for this fiber showing best binary and best single orbit comparison.
"""
import os, optparse

from multiplicity.markov import mcinout, mcescher, likelihood
from multiplicity.radialvelocity import orbit

def main():
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-f', '--fiberid', dest='fiberid', action='store', type=str, default=None, help='specific fiber to run')
    parser.add_option('-t', '--traceDir', dest='traceDir', action='store', type=str, default=None, help='dirrectory containing trace files for fibers')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='database to read RV info from')
    parser.add_option('-o', '--outDir', dest='outDir', action='store', type=str, default=None, help='dir for output traces')
    parser.add_option('-b', '--bestOutPath', dest='bestOutPath', action='store', type=str, default=None, help='path to file for appending best orbits (optional)')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='dir for plotting orbit comparison (optional)')
    parser.add_option('-s', '--single', dest='single', action='store_true', default=False, help='used for calculating single-star systems')
    args = parser.parse_args()[0]

    fiberid = args.fiberid
    traceDir = args.traceDir
    dbPath = args.dbPath
    outDir = args.outDir
    bestOutPath = args.bestOutPath
    plotDir = args.plotDir

    # Find trace file and get traces
    tracePath = mcinout.find_path(traceDir, fiberid)
    traces = mcinout.load_trace(tracePath)

    # Read in the rvs and errors from database   USE mcinout.py INSTEAD
    obs, rvs, errs = mcinout.load_fiber(dbPath, fiberid)  # obs[0] != 0

    # Calculate lnL for every trace and write to file
    if args.single:
        with open(os.path.join(outDir, '%s_single-likelihood-traces.dat' % fiberid), 'w') as outTraceFile:
            outTraceFile.write('#offset  L  lnL  BIC\n')
            bestTrace = (0.0, -1e10, -1e10, 1e10)
            for trace in traces:
                offset = trace[0]
                singleRVs = [offset for rv in rvs]
                L = likelihood.calc_likelihood(rvs, errs, singleRVs)
                lnL = likelihood.calc_lnL(rvs, errs, singleRVs)
                BIC = likelihood.calc_BIC(rvs, errs, singleRVs, 1)
                BIC_check = likelihood.lnL_to_BIC(lnL, 1, len(rvs))
                assert BIC == BIC_check
                outTraceFile.write('%f  %f  %f  %f\n' % (offset, L, lnL, BIC))
                # Keep trace of most likely
                if L > bestTrace[1]:
                    bestTrace = (offset, L, lnL, BIC)

    else:
        with open(os.path.join(outDir, '%s_likelihood-traces.dat' % fiberid), 'w') as outTraceFile:
            outTraceFile.write('#logA  logP  phi  b  L  lnL  BIC\n')
            bestTrace = (0.0, 0.0, 0.0, 0.0, -1e10, -1e10, 1e10)
            for trace in traces:
                logA = trace[0]
                logP = trace[1]
                phi = trace[2]
                b = trace[3]
                binaryOrbit = orbit.sine_wave_from_params(logA, logP, phi, b)
                binaryRVs = binaryOrbit(obs)
                L = likelihood.calc_likelihood(rvs, errs, binaryRVs)
                lnL = likelihood.calc_lnL(rvs, errs, binaryRVs)
                BIC = likelihood.calc_BIC(rvs, errs, binaryRVs, 4)
                BIC_check = likelihood.lnL_to_BIC(lnL, 4, len(rvs))
                assert BIC == BIC_check
                outTraceFile.write('%f  %f  %f  %f  %f  %f  %f\n' % (logA, logP, phi, b, L, lnL, BIC))
                # Keep trace of most likely
                if L > bestTrace[4]:
                    bestTrace = (logA, logP, phi, b, L, lnL, BIC)


    # (optional)  append best orbits to a master list.
    if bestOutPath is not None:
        if args.single:
            with open(bestOutPath, 'a') as bestOrbitFile:
                outString = '%s  %f  %f  %f  %f\n' % (fiberid, bestTrace[0], bestTrace[1], bestTrace[2], bestTrace[3])
                bestOrbitFile.write(outString)

        else:
            # Calculate single orbit likelihoods
            offset, spread = orbit.calc_systemic_velocity(rvs, errs)
            singleOrbit = orbit.single_star_orbit(offset)
            singleRVs = singleOrbit(obs)
            L_single = likelihood.calc_likelihood(rvs, errs, singleRVs)
            lnL_single = likelihood.calc_lnL(rvs, errs, singleRVs)
            BIC_single = likelihood.calc_BIC(rvs, errs, singleRVs, 1)
            DBIC = bestTrace[6] - BIC_single
            # Write data to file
            with open(bestOutPath, 'a') as bestOrbitFile:
                outString = '%s  %f  %f  %f  %f  %f  %f  %f  %f  %f  %f  %f  %f\n' % (fiberid, bestTrace[0], bestTrace[1], bestTrace[2], bestTrace[3], bestTrace[4], bestTrace[5], bestTrace[6], offset, L_single, lnL_single, BIC_single, DBIC)
                bestOrbitFile.write(outString)


    # (optional) plot comparison plot for this fiber
    if plotDir is not None and not args.single:
        thisPlotDir = os.path.join(plotDir, fiberid)
        if not os.path.isdir(thisPlotDir):
            os.mkdir(thisPlotDir)
        bestBinaryOrbit = orbit.sine_wave_from_params(bestTrace[0], bestTrace[1], bestTrace[2], bestTrace[3])
        mcescher.plot_orbit_comparison(obs, rvs, errs, singleOrbit, bestBinaryOrbit, plotDir=thisPlotDir, fold_logP=None, title=fiberid, single_BIC=BIC_single, binary_BIC=bestTrace[6])
        mcescher.plot_orbit_comparison(obs, rvs, errs, singleOrbit, bestBinaryOrbit, plotDir=thisPlotDir, fold_logP=bestTrace[1], title=fiberid, single_BIC=BIC_single, binary_BIC=bestTrace[6])
        mcescher.plot_parameter_triangle(traces, plotDir=thisPlotDir, baseName=fiberid, truths=[bestTrace[0], bestTrace[1], bestTrace[2], bestTrace[3], 0.0])


if __name__ == "__main__":
    main()


