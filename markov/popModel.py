import numpy as np

from multiplicity.radialvelocity import orbit, absoluterv


def pull_from_powerlaw(index, low, high):
    """ Return a random draw from a power-law 
        distribution given index, low, high.
    """
    if index == -1:
        log_value = np.random.uniform(np.log10(low), np.log10(high))
        return pow(10, log_value)
    else:
        n = index + 1
        uni = np.random.random()
        value = uni*(pow(high,n) - pow(low,n)) + pow(low,n)
        value = pow(value, (1./n))
        return value


def draw_primary_mass():
    """Draw a mass from the Kroup IMF.

    Distribution from Kroupa (2001)
        p(m) = C*m^-a         a = {2.3 for m > 0.5 Msun
    This uses the inverse method to map x = [0.0, 1.0) onto p(m)
        F^-1 = ((1-a)*x/C + low^(1-a))^ (1/ 1-a))
    C is the normalization constant
        C = 1.0 / integral(m^-a * dm) from lo to hi

    The masses returned are for F0-G0 stars ~ 1.0 Msun to 1.8 Msun
    """
    return pow((-1.3*0.4110*np.random.random() + 1.0), (-0.7692))


def simulate_system(tobs, errs, fbin, alpha, beta, amin=0.01, amax=0.16, qlow=0.1):
    """Simulate a single system and compute e/i, given (errs, obs, etc.)."""
    binary = np.random.random() < fbin
    if binary:
        m1 = draw_primary_mass()
        q = pull_from_powerlaw(beta, qlow, 1.0)
        a = pull_from_powerlaw(alpha, amin, amax)
        incl = np.random.random() * (2.*np.pi)        # Random inclination [0.0, 2pi).
        nu = np.random.random() * (2.*np.pi)          # Random mean anomoly [0.0, 2pi) at t0.
        rv0 = 0.0
        cleanRVs = orbit.calculate_velocities(tobs, m1, q, a, rv0, incl, nu)
    else:
        m1, q, a, = 0.0, 0.0, 0.0
        cleanRVs = [0.0 for t in tobs]
    dirtyRVs = []
    for i, rv in enumerate(cleanRVs):
        dirtyRVs.append(rv + np.random.normal(0.0, scale=errs[i]))
    eoveri = absoluterv.calc_eoveri(dirtyRVs, errs)
    return eoveri


def parallelized_simulate_system(arguments):
    """Unpack the arguments and run call simulate_system().  Return the EOI for a single simulated
    star system.  The last value in arguments must be a multiplier, specifying the number of times
    to simulate the star.  The length of eoi returned will be equal to that."""
    #tobs, err, fbin, alpha, beta = arguments
    multiplier = arguments[-1]
    eoi = []
    for i in range(multiplier):
        eoi.append(simulate_system(*arguments[:-1]))
    return eoi


def model_eoveri(tobsList, errsList, fbin, alpha, beta, amin=0.01, amax=0.16, qlow=0.1):
    """Model a population of stars given hyperparameters.  Compute the e/i
    values for each system, and return the list of e/i values."""
    #print "Modelling e/i for entire population..."
    assert len(tobsList) == len(errsList)
    allEOI = []
    for tobs, errs in zip(tobsList, errsList):
        assert len(tobs) == len(errs)
        thisEOI = simulate_system(tobs, errs, fbin, alpha, beta, amin, amax, qlow)
        allEOI.append(thisEOI)
    #print "Finished model."
    return allEOI
