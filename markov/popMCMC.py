"""
Functions for running the pop-wide MCMC.
Includes likelihood calculations for the prior and posterior.
Includes functions for initializing walker balls.
"""
import time
import multiprocessing

import numpy as np
from scipy.stats import gaussian_kde

from multiplicity.markov import popModel
from matplotlib.pyplot import hist


def run_chain(sampler, pos, nwalkers, nsteps=500, thin=1, verbose=False, progPath=None, progFrequency=2):
    """Run the sampler chain."""
    timeBegin = time.time()

    if progPath is not None:
        outFile = open(progPath, 'a', buffering=1)
        outFile.write("[%s] Starting %d walkers taking %d steps each (%d total)...\n" % (time.strftime('%Y-%m-%d %H:%M:%S'), nwalkers, nsteps, nwalkers*nsteps))
        current_pos, current_rstate = pos, np.random.get_state()
        if nsteps < progFrequency:
            raise Exception('nsteps < freq')

        loopCount = int(np.ceil(nsteps / float(progFrequency)))
        for loop in range(loopCount):
            clocktime = time.strftime('%Y-%m-%d %H:%M:%S')
            outFile.write('[%s] Working on steps %d -- %d of %d total...\n' % (clocktime, (loop*progFrequency)+1, min((loop+1)*progFrequency, nsteps), nsteps))
            current_pos, current_lnprob, current_rstate = sampler.run_mcmc(current_pos, progFrequency, rstate0=current_rstate, thin=thin)        

        timeElapsed = time.time() - timeBegin
        outFile.write("Finished %d walkers taking %d steps each (%d total) in %f seconds.\n" % (nwalkers, nsteps, nwalkers*nsteps, timeElapsed))
        outFile.close()

    else:
        sampler.run_mcmc(pos, nsteps, rstate0=np.random.get_state(), thin=thin)
        timeElapsed = time.time() - timeBegin

    return timeElapsed


def lnprior(theta, priorLimits):
    """
    ln(prior probability):
    p(theta_i)
    """
    labels = ['fbin', 'alpha']
    for label, val in zip(labels, theta):
        if not (priorLimits[label][0] < val < priorLimits[label][1]):
            return -np.inf
    return 0.0


def lnlike_continuous(theta, tobsList, errsList, EOIs):
    """
    ln(likelihood):
    p(y_i | theta_i)

    Will not include yvar for y == e/i.

    Likelihood is the probability of drawing a value from the continuous e/i PDF.
    This is very slow.
    """
    # Calculate the continuous PDF
    fbin, alpha = theta
    beta = 0.0
    #print 'Modeling stars...'
    modelEOIs = popModel.model_eoveri(tobsList, errsList, fbin, alpha, beta)
    #print 'Calculating KDE'
    kernel = gaussian_kde(modelEOIs)
    #kernelset_bandwidth(scalarValue)   # This might help speed up the code.

    # Add up the log-likelihoods
    #print 'Summing lnL...'
    lnL = np.sum(np.log(kernel(modelEOIs)))
    #print lnL
    return lnL


def lnprob_continuous(theta, tobsList, errsList, EOIs, priorLimits):
    """
    ln(posterior probability):
    p(theta_i | y_i) = p(y_i | theta_i) * p(theta_i)
    posterior = likelihood*prior
    """
    lp = lnprior(theta, priorLimits)
    if not np.isfinite(lp):
    	return -np.inf
    lnl = lnlike_continuous(theta, tobsList, errsList, EOIs)
    if not np.isfinite(lnl):
    	return -np.inf
    return lp + lnl


def lnlike_binned(theta, exposureInfo, binCount, histRange, binHeights, modelMultiplier, eoiBinCutoff, beta=0.0, parallel=False, ):
    """
    ln(likelihood):
    p(y_i | theta_i)

    Likelihood is the Possion probability of the number of real systems in 
    each e/i bin, given the height of the bins.   

    L = mu^y * exp(-mu) / y!
    lnL = yln(mu) - mu - ln(y!)  =  yln(mu) - mu + C

    modelMultiplier specifies how many times to simulate each star (improve statistics for model).
    eoiBinCutoff dictates a minimum e/i bin used in the likelihood calculation, with 1 equal to ignoring the first bin
    """
    fbin, alpha = theta

    if parallel:
        arguments = [(tobs, err, fbin, alpha, beta, modelMultiplier) for tobs, err in exposureInfo]   # Make a list of arguments (one per star)
        availProcCount = multiprocessing.cpu_count() - 14       # Equal to 50 cores on Estraven
        useProcCount = max(availProcCount, 1)
        pool = multiprocessing.Pool(processes=useProcCount)     # Use all cores, or specify the number.
        modelEOIs = pool.map(popModel.parallelized_simulate_system, arguments, chunksize=100)   # send 100 stars to each job. This reduces overhead.    
        allEOIs = [eoi for star in modelEOIs for eoi in star]   # Flatten list-of-lists modelEOIs to a 1D list of floats.

    else:
        tobsList, errsList = zip(*exposureInfo)
        allEOIs = []
        for i in range(modelMultiplier):
            allEOIs.extend(popModel.model_eoveri(tobsList, errsList, fbin, alpha, beta))

    n, bins, patches = hist(allEOIs, binCount, histRange)
    mu = n / float(modelMultiplier)
    mu = [max(m, 1e-10) for m in mu]    # poisson distribtion not valid for expectation mu=0.

    mu = mu[eoiBinCutoff:]
    binHeights = binHeights[eoiBinCutoff:]

    lnL = np.sum(binHeights*np.log(mu)) - np.sum(mu)
    return lnL


def lnprob_binned(theta, exposureInfo, binCount, histRange, binHeights, priorLimits, multiplier=3, eoiBinCutoff=0):
    """
    ln(posterior probability):
    p(theta_i | y_i) = p(y_i | theta_i) * p(theta_i)
    posterior = likelihood*prior
    """
    lp = lnprior(theta, priorLimits)
    if not np.isfinite(lp):
    	return -np.inf
    lnl = lnlike_binned(theta, exposureInfo, binCount, histRange, binHeights, multiplier, eoiBinCutoff)
    if not np.isfinite(lnl):
    	return -np.inf
    return lp + lnl


def get_walkers(nwalkers, priorLimits):
    """Create a ball of walker starting positions. The ball is a ndim ball
    in parameter space, centered around guesses for the true param values."""
    positions = []
    for i in range(nwalkers):
    	thisWalkerPosition = []
    	thisWalkerPosition.append(np.random.uniform(priorLimits['fbin'][0], priorLimits['fbin'][1]))
    	thisWalkerPosition.append(np.random.uniform(priorLimits['alpha'][0], priorLimits['alpha'][1]))
    	#thisWalkerPosition.append()      beta
    	#thisWalkerPosition.append()      amin
    	#thisWalkerPosition.append()      amax

    	#thisWalkerPosition = np.array(thisWalkerPosition)
        positions.append(thisWalkerPosition)
    return positions

