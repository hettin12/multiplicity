#!/usr/bin/env python
"""Calculates the likelihood, lnL, BIC values given data points and model points.

Likelihood:
    L = product(P(x)) for all x
    lnL = sum(ln(P(x)))

BIC is a model selection criterion:
    -2ln(P(x)) ~ BIC = -2lnL^ + k*(ln(N) + ln(2pi))
       k = number of parameters
       N = number of data points in sample x
       L^ = maximum likelihood

Given two models, the model with the lower value of BIC is preferred.  Delta(BIC):
    0 to 2     weak
    2 to 6     positive
    6 to 10    strong
    >10        very strong

Relevant plots in mcescher include:
    mcescher.plot_DBIC_hist(delta_BIC, plotDir)
    mcescher.plot_BIC_scatter(binary_BIC, single_BIC, plotDir)
    mcescher.plot_orbit_comparison(obs, rvs, errs, single_orbit, binary_orbit, plotDir=None, fold_logP=None, title=None, single_BIC=None, binary_BIC=None)
"""

from scipy.stats import norm
import numpy as np


def calc_likelihood(dataPoints, dataErrs, modelPoints):
    """For a set of datapoints/errors and a set of modelPoints,
    calculate and return the likelihood."""
    L = 1.0
    for mu, sigma, modelPoint in zip(dataPoints, dataErrs, modelPoints):
        L *= norm.pdf(modelPoint, loc=mu, scale=sigma)
    return L


def calc_lnL(dataPoints, dataErrs, modelPoints):
    """For a set of datapoints/errors and a set of modelPoints,
    calculate and return the log-likelihood."""
    lnL = 0.0
    for mu, sigma, modelPoint in zip(dataPoints, dataErrs, modelPoints):
        lnL += norm.logpdf(modelPoint, loc=mu, scale=sigma)
    return lnL


def calc_BIC(dataPoints, dataErrs, modelPoints, k):
    """Calculate and return the Bayesian Information Criterion based
    on a set of real data points/errors and a set of model points. Also
    required is the number of free parameters, k."""
    lnL = calc_lnL(dataPoints, dataErrs, modelPoints)
    BIC = lnL_to_BIC(lnL, k, len(dataPoints))
    return BIC


def lnL_to_BIC(lnL, k, n):
    """Given the log-likelihood, the number of parameters k, and
    the number of data points n, return the Bayesian Information Criterion."""
    BIC = -2*lnL + k*(np.log(n) + np.log(2*3.14159))
    return BIC


def calc_posterior(all_lnL):
    """Given every trace log-likelihood for a MCMC run, return the sum of
    likelihoods."""
    return np.sum( [pow(10., lnL) for lnL in all_lnL] )


def calc_bayes_factor(modelPosterior_1, modelPosterior_2):
    """Return the POSTERIOR bayes factor, K for two models, given the posteriror
    likelihood of each model.  This is not the true Bayes factor which should utilize
    the integral over the prior instead."""
    return modelPosterior_1 / modelPosterior_2



