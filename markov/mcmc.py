"""
Functions for running an emcee Markov-Chain Monte-Carlo.

These include the likelihood calculations for the prior and the parameters, as
well as those needed to create the walkers and set up and run the MCMC chain.

Contains functions for the binary model AND the single-star model.

"""
import time

import numpy as np

TWOPI = 2.0*np.pi
OUTLIER_DETECTION = False


def sinusoid(x, logA, logP, phi, b):
    """Simple sine function y = A*sin(2PI* x / P + phi) + b.
    Semi-amplitude and period are given as logs.  If isBin=False, return 
    a flat line y=b."""
    y = np.sin(TWOPI * x / pow(10.0, logP) + phi)
    y *= pow(10.0, logA)
    y += b
    return y


def run_chain(sampler, pos, nwalkers, nsteps=5000, thin=1, verbose=False):
    """Run the sampler chain."""
    if verbose:
        print "Starting %d walkers taking %d steps each (%d total)" % (nwalkers, nsteps, nwalkers*nsteps)
        print "Running MCMC..."
    timeBegin = time.time()
    sampler.run_mcmc(pos, nsteps, rstate0=np.random.get_state(), thin=thin)
    timeEnd = time.time()
    timeElapsed = timeEnd - timeBegin
    if verbose:
        print "Finished %d walkers taking %d steps each (%d total) in %f seconds." % (nwalkers, nsteps, nwalkers*nsteps, timeElapsed)
    return timeElapsed


def theta_to_params(theta):
    """Given all params, return a dictionary keyed by the param label
    and the entry is a 2-tuple with the value and the priorDict key."""
    paramDict = {'logA':(theta[0], 'logA'),
                 'logP':(theta[1], 'logP'),
                 'phi':(theta[2], 'phi'),
                 'b':(theta[3], 'b'),
                 'isBinary':(theta[4], 'isBinary'), }
    remainingTheta = theta[5:]

    if OUTLIER_DETECTION:
        paramDict.update({'Pb':(remainingTheta[0], 'Pb'),
                          'Yb':(remainingTheta[1], 'Yb'),
                          'Vb':(remainingTheta[2], 'Vb')})
        remainingTheta = remainingTheta[3:]                     

    plateCount = 1 + len(remainingTheta)
    for p, param in enumerate(remainingTheta):
        key = 'ps_%d' % (p+1)
        paramDict[key] = (remainingTheta[p], 'ps')

    return paramDict, plateCount


def lnprior(theta, priorLimits):
    """
    ln(prior probability):
    p(theta_i)
    """
    paramDict, plateCount = theta_to_params(theta)
    for param, tup in paramDict.iteritems():
        val, priorKey = tup
        if not (priorLimits[priorKey][0] < val < priorLimits[priorKey][1]):
            return -np.inf
    return 0.0


def lnlike(theta, x, y, yvar):
    """
    ln(likelihood):
    p(y_i | theta_i)
    
    Use the binary model if 'isBinary' parameter is between 0.5 and 1.0.  Use the single for 'isBinary' param < 0.5.
    """
    logA, logP, phi, b, isBinary = theta[:5]
    paramDict, plateCount = theta_to_params(theta)

    # Get plate shifts
    ps = [0.0,]
    for p in range(plateCount-1):
        shift_p = paramDict['ps_%d' % (p+1)][0]
        ps.append(shift_p)

    if isBinary >= 0.5:
        model = [sinusoid(x[p], logA, logP, phi, b) for p in range(plateCount)]
    else:
        model = [0.0*x[p]+b for p in range(plateCount)]

    # Calculate Likelihood
    if OUTLIER_DETECTION:
        Pb = paramDict['Pb'][0]
        Yb = paramDict['Yb'][0]
        Vb = paramDict['Vb'][0]
        total_L = 1.0
        for p in range(plateCount):
            A = (1. - Pb) / np.sqrt(TWOPI * yvar[p])
            B = np.exp(-0.5 * (y[p] - model[p] - ps[p])**2. / yvar[p])
            C = Pb / np.sqrt(TWOPI * (Vb + yvar[p]))
            D = np.exp(-0.5 * (y[p] - Yb - ps[p])**2. / (Vb + yvar[p]))
            individual_Ls = (A*B + C*D)
            plate_L = np.product(individual_Ls)
            if plate_L <= 0:
                return -np.inf
            total_L *= plate_L
        return np.log(total_L)

    else:
        lnL = 0.0
        for p in range(plateCount):
            lnL += np.sum(np.log(yvar[p]) + (y[p] - model[p] - ps[p])**2. / yvar[p])
        return -0.5 * lnL


def lnprob(theta, x, y, yvar, priorLimits):
    """
    ln(posterior probability):
    p(theta_i | y_i) = p(y_i | theta_i) * p(theta_i)
    posterior = likelihood*prior
    """
    lp = lnprior(theta, priorLimits)
    if not np.isfinite(lp):
        return -np.inf
    lnl = lnlike(theta, x, y, yvar)
    if not np.isfinite(lnl):
        return -np.inf
    return lp + lnl


def get_walkers(y, ndim, nwalkers, priorLimits):
    """Create a ball of walker starting positions. The ball is a ndim ball
    in parameter space, centered around guesses for the true param values."""
    # Best starting area for logA
    all_y = []
    for ar in y:
        all_y.extend(ar)
    logA_guess = np.log10(abs(max(all_y) - min(all_y)) / 2.)
    logA_guess = max(logA_guess, 1.0)

    # Create a ball of walkers around the best guess params.
    pos = []
    for i in range(nwalkers):
        thisWalkerPosition = []
        thisWalkerPosition.append(np.random.normal(logA_guess, 0.1))                                   # logA
        thisWalkerPosition.append(np.random.uniform(priorLimits['logP'][0], priorLimits['logP'][1]))   # logP
        thisWalkerPosition.append(np.random.uniform(0.0, TWOPI))                                       # phi
        thisWalkerPosition.append(np.random.normal(np.mean(all_y), 15.0))                              # b
        thisWalkerPosition.append(np.random.uniform())                                                 # isBinary
        if OUTLIER_DETECTION:
            thisWalkerPosition.append(np.random.uniform(priorLimits['Pb'][0], priorLimits['Pb'][1]))   # Prob_bad
            thisWalkerPosition.append(np.random.normal(np.mean(all_y), 15.0))                          # Mean_bad
            thisWalkerPosition.append(np.random.uniform(priorLimits['Vb'][0], priorLimits['Vb'][1]))   # Var_bad
        for p in range(len(y)-1):
            thisWalkerPosition.append(np.random.uniform(priorLimits['ps'][0], priorLimits['ps'][1]))
        np.array(thisWalkerPosition)
        pos.append(thisWalkerPosition)
    return pos
