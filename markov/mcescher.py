#!/usr/bin/env python
import os, sys, optparse
from random import sample
import multiprocessing

import triangle
import numpy as np
import matplotlib.pyplot as pl
from matplotlib.ticker import MaxNLocator

import mcinout, popModel
from multiplicity.markov.mcmc import sinusoid

# This is specific to chainrunner!
def calc_avg_values(fiberlist, traceDir, outPath=None):
    """Load the traces from the fiberlist and return (means, medians)
    where means is an array shape (*,5) containing average values for
    each fiber for each parameter."""
    means, medians = [], []
    if outPath is not None:
        with open(outPath, 'w') as outFile:
            outFile.write('#fiberid logA_mean (median) logP_mean (median) phi_mean (median) b_mean (median) f_mean (median)\n')
    for f, fiberid in enumerate(fiberlist):
        thisPath = os.path.join(traceDir, '%s_1000_3600_3-traces.dat' % fiberid)
        if not os.path.isfile(thisPath):
            print 'Could not find %s. Skipping for now.' % thisPath
            continue

        # THIS NEEDS REWORK after load_trace() now returns load_trace_old().T
        #params = load_trace(thisPath)

        m1 = [np.mean(p) for p in params]
        m2 = [np.median(p) for p in params]
        if outPath is not None:
            with open(outPath, 'a') as outFile:
                outFile.write('%s  %f  %f  %f  %f  %f  %f  %f  %f  %f  %f\n' % (fiberid, m1[0], m2[0], m1[1], m2[1], m1[2], m2[2], m1[3], m2[3], m1[4], m2[4]))
        else:
            means.append(m1)
            medians.append(m2)
        if (f+1) % 1000 == 0:
            sys.stdout.write( 'Completed %d / %d.\n' % ((f+1), len(fiberlist)) )
    if outPath is None:
        means = np.array(means)
        medians = np.array(medians)
        return means, medians
     

## PLOTTING  ##
###############
def plot_traces(traces, plotDir=None, baseName=None, labels=["$logA$", "$logP$", "$\phi$", "$b$", "$f$"]):
    """Given a set of traces, plot each variable.  Plots walkers consecutively 
    instead of in parellel. Traces must be an NxM 2D array.  The smaller axis is used
    as the parameter axis."""
    dim = traces.shape
    if dim[0] > dim[1]:
        traces = traces.T
    ndim = len(traces)
    fig, axes = pl.subplots(ndim, 1, sharex=True, figsize=(8, 3*ndim))
    for i, trace in enumerate(traces):
        axes[i].plot(trace, color="k", alpha=1.0)
        axes[i].yaxis.set_major_locator(MaxNLocator(5))
        axes[i].set_ylabel(labels[i])
        if i+1 == ndim:
            axes[i].set_xlabel("sample number")
    fig.tight_layout(h_pad=0.0)
    if plotDir is not None:
        if baseName is not None:
            fig.savefig(os.path.join(plotDir, "%s-traces.png" % baseName))
        else:
            fig.savefig(os.path.join(plotDir, "traces.png"))


def plot_chain_traces(chain, plotDir=None, baseName=None, nburn=None, labels=["$logA$", "$logP$", "$\phi$", "$b$", "$f$", "$binRoll$"]):
    """Plot a trace of each parameter, with one line per walker per subplot."""
    ndim = chain.shape[2]
    fig, axes = pl.subplots(ndim, 1, sharex=True, figsize=(18, 3*ndim))
    if ndim == 1:
        axes.plot(chain[:, :, 0].T, color="k", alpha=0.2)
        axes.yaxis.set_major_locator(MaxNLocator(5))
        axes.set_ylabel(labels[0])
    else:
        for i in range(ndim):
            axes[i].plot(chain[:, :, i].T, color="k", alpha=0.2)
            axes[i].yaxis.set_major_locator(MaxNLocator(5))
            axes[i].set_ylabel(labels[i])
            if nburn is not None:
                axes[i].axvline(nburn, ls='--', color='red', label='Burn Limit')
            if i+1 == ndim:
                axes[i].set_xlabel("step number (after thinning)")
    fig.tight_layout(h_pad=0.0)
    if plotDir is not None:
        if baseName is not None:
            fig.savefig(os.path.join(plotDir, "%s-chaintraces.png" % baseName))
        else:
            fig.savefig(os.path.join(plotDir, "chaintraces.png"))


def plot_parameter_triangle(valueTupleList, plotDir=None, baseName=None, labels=["$logA$", "$logP$", "$\phi$", "$b$", "$f$"], truths=None, quantiles=[], extents=None, include='all'):
    """Make a triangle plot.  Given a lists of values (e.g. traces) accepted as a tuple,
    plot a histogram for each variable, and a 2D histogram for each combination of variables.
    Data points can be added with 'truths' parameter."""
    if include != 'all':
        singleTuples, binaryTuples = [], []
        for tup in valueTupleList:
            if tup[4] < 0.5:
                singleTuples.append(tup)
            else:
                binaryTuples.append(tup)

    if include == 'binary':
        tupleList = binaryTuples
        filename = "binary-triangle.png"
    elif include == 'single':
        tupleList = singleTuples
        filename = "single-triangle.png"
    else:
        tupleList = valueTupleList
        filename = "triangle.png"

    if truths is None:
            fig = triangle.corner(tupleList, labels=labels, quantiles=quantiles, extents=extents)
    else:
            fig = triangle.corner(tupleList, labels=labels, quantiles=quantiles, extents=extents, truths=truths)
    
    if plotDir is not None:
        if baseName is not None:
            fig.savefig(os.path.join(plotDir, "%s-%s" % (baseName, filename)))
        else:
            fig.savefig(os.path.join(plotDir, "%s" % filename))


def get_ranges(x, y):
    """Find a suitable window for the data."""
    yRange = abs(max(y) - min(y))
    yRange = max(yRange, 10.0)
    xRange = abs(max(x) - min(x))
    y_low = min(y) - yRange*0.6
    y_hi = max(y) + yRange*0.6
    x_low = min(x) - xRange*0.2
    x_hi = max(x) + xRange*0.2
    return x_low, x_hi, y_low, y_hi


def poisson_errors(n, log=False):
    """Pearsons's chi-squared methoed for Poisson errorbars.  
    See: http://www-cdf.fnal.gov/physics/statistics/notes/cdf6438_coverage.pdf by Joel Heinrich.

    Returns the size of the errorbar on either side of the data point.
    If log is true, return the the size of the errorbar in a log-plot. (Assuming n has not been logged already.)"""
    lowerSize = np.sqrt(n+0.25) - 0.5
    upperSize = np.sqrt(n+0.25) + 0.5
    if log:
        if n == 0:
            return None, None
        else:
            lowBound = (n - lowerSize)
            highBound = (n + upperSize)
            logLowBound = np.log10(lowBound)
            logHighBound = np.log10(highBound)
            logN = np.log10(n)
            lowerSize = logN - logLowBound
            upperSize = logHighBound - logN
    return lowerSize, upperSize


def plot_popMCMC_samples(traces, exposureInfo, EOIs, binCount, histRange, plotDir, baseName, size=50, modelMultiplier=3, eoiBinCutoff=0):
    """Plot some samples from the MCMC of the binned distribution of model EOIs."""
    fig, ax = pl.subplots(1, 1, figsize=(12, 6))

    # Sample from the traces, simulate stars, and plot the e/i histograms.
    if len(traces) < size:
        size = len(traces)
    sampleTraces = sample(traces, size)
    sample_N_arrays = []    # these hold the y-values for each sample. Use them to create a shaded contour region with plt.fill_between().
    for theta in sampleTraces:
        fbin, alpha = theta
        beta = 0.0

        # Parallelize with a pool
        arguments = [(tobs, err, fbin, alpha, beta, modelMultiplier) for tobs, err in exposureInfo]
        availProcCount = multiprocessing.cpu_count() - 14
        useProcCount = max(availProcCount, 1)
        pool = multiprocessing.Pool(processes=useProcCount)     # Use all cores, or specify the number.
        modelEOIs = pool.map(popModel.parallelized_simulate_system, arguments, chunksize=100)
        allEOIs = [eoi for star in modelEOIs for eoi in star]

        nBig, binEdges, patches = ax.hist(allEOIs, binCount, histRange, visible=False)
        n = nBig / float(modelMultiplier)
        x = 0.5*(binEdges[1:]+binEdges[:-1])
        #n = [float(k) for k in n]
        for i in range(len(n)):
            if n[i] > 0:
                n[i] = np.log10(n[i])
            else:
                n[i] = -1.0
        sample_N_arrays.append(n)
        #ax.plot(x, n, ls='steps-mid', color='k', alpha='0.1', lw=5)
        ax.plot(x, n, color='DodgerBlue', alpha='0.1', marker='')

    # Hist the data
    n, binEdges, __ = ax.hist(EOIs, binCount, histRange, visible=False)
    x = 0.5*(binEdges[1:]+binEdges[:-1])
    n = [float(k) for k in n]
    # Assign fancy Poisson error bars
    poissonTups = [poisson_errors(i, log=True) for i in n]
    lowErrs = [tup[0] for tup in poissonTups]
    highErrs = [tup[1] for tup in poissonTups]
    # Convert to log-N
    logN = [None] * len(n)
    for i in range(len(n)):
        if n[i] > 0:
            logN[i] = np.log10(n[i])
        else:
            logN[i] = -1.0
            lowErrs[i] = 0
            highErrs[i] = 0.0 - logN[i]
    # Add empty values on either end of the figure
    dx = (x[1] - x[0])
    x = np.insert(x, 0, x[0]-dx)
    x = np.append(x, x[-1]+dx)
    logN = np.insert(logN, 0, -1.0)
    logN = np.append(logN, -1.0)
    lowErrs = np.insert(lowErrs, 0, 0.0)
    lowErrs = np.append(lowErrs, 0.0)
    highErrs = np.insert(highErrs, 0, 0.0)
    highErrs = np.append(highErrs, 0.0)
    # Plot
    ax.plot(x, logN, ls='steps-mid', label="Data", color='k')
    ax.errorbar(x, logN, yerr=[lowErrs, highErrs], fmt='', ls='', ecolor='k', capsize=0)

    if eoiBinCutoff > 0:
        c = binEdges[eoiBinCutoff]
        ax.axvline(c, ls='--', color='k')

    ax.set_xlim(*histRange)
    ax.set_ylim(-0.4, 4)
    ax.set_xlabel('e/i')
    ax.set_ylabel('log(N)')
    fig.tight_layout()
    outPath = os.path.join(plotDir, '%s-samples.png' % baseName)
    fig.savefig(outPath)


def plot_rvcurve(xList, yList, yerrList, plotDir=None, baseName=None):
    """Plot the rvdata for this fiber.  X and Y inputs will be a list of arrays,
    with each plate's data on a single array."""
    pl.figure(figsize=[18, 6])
    all_x, all_y = [], []
    for p in range(len(xList)):
        x = xList[p]
        y = yList[p]
        yerr = yerrList[p]
        pl.errorbar(x, y, yerr=yerr, fmt=".", ms=8)
        all_x.extend(x)
        all_y.extend(y)

    x_min = min(all_x)
    x_max = max(all_x)
    dx = x_max - x_min
    y_min = min(all_y)
    y_max = max(all_y)
    dy = y_max - y_min

    pl.ylim(y_min - dy*0.6, y_max + dy*0.6)
    pl.xlim(x_min - dx*0.2, x_max + dx*0.2)
    pl.xlabel("Time (s)")
    pl.ylabel("Radial Velocity (km/s)")
    pl.tight_layout()
    if plotDir is not None:
        if baseName is not None:
            pl.savefig(os.path.join(plotDir, "%s-rvcurve.png" % baseName))
        else:
            pl.savefig(os.path.join(plotDir, "rvcurve.png" ))


def plot_sinusoid_samples(samples, x, y, yerr, size=200, plotDir=None, baseName=None, dualModel=False):
    """Plot a random selection of samples overtop the data (binary-star model)."""
    if len(x) == 1:
        x = x[0]
        y = y[0]
        yerr = yerr[0]

    fig = pl.figure()
    x_low, x_hi, y_low, y_hi = get_ranges(x, y)
    xl = np.linspace(x_low, x_hi, 1000)
    idx = np.random.randint(len(samples), size=size)

    if dualModel:
        for tup in samples[idx]:
            logA, logP, phi, b, isBinary = tup[0], tup[1], tup[2], tup[3], tup[4]
            if isBinary >= 0.5:
                pl.plot(xl, sinusoid(xl, logA, logP, phi, b), color="k", alpha=0.1)
            else:
                pl.axhline(b, alpha=0.1, color='DodgerBlue')
    else:
        for logA, logP, phi, b in samples[idx]:
            pl.plot(xl, sinusoid(xl, logA, logP, phi, b), color="k", alpha=0.1)

    pl.errorbar(x, y, yerr=yerr, fmt=".", color="LimeGreen")
    pl.ylim(y_low, y_hi)
    pl.xlim(x_low, x_hi)
    pl.xlabel("Time (s)")
    pl.ylabel("Radial Velocity (km/s)")
    pl.tight_layout()
    if plotDir is not None:
        if baseName is not None:
            fig.savefig(os.path.join(plotDir, "%s-samples.png" % baseName))
        else:
            fig.savefig(os.path.join(plotDir, "samples.png"))


def plot_offset_samples(samples, x, y, yerr, size=200, plotDir=None, baseName=None):
    """Plot a random selection of samples overtop the data (single-star model)"""
    fig = pl.figure()
    x_low, x_hi, y_low, y_hi = get_ranges(x, y)
    xl = np.linspace(x_low, x_hi, 1000)
    idx = np.random.randint(len(samples), size=size)
    for offset in samples[idx]:
        pl.axhline(offset, alpha=0.1, color='k')
    pl.errorbar(x, y, yerr=yerr, fmt=".", color="LimeGreen")
    pl.ylim(y_low, y_hi)
    pl.xlim(x_low, x_hi)
    pl.xlabel("Time (s)")
    pl.ylabel("Radial Velocity (km/s)")
    pl.tight_layout()
    if plotDir is not None:
        if baseName is not None:
            fig.savefig(os.path.join(plotDir, "%s-single-samples.png" % baseName))
        else:
            fig.savefig(os.path.join(plotDir, "single-samples.png"))


def plot_DBIC_vs_bayesFactor(xyTupleList, plotDir=None):
    """Plot an x vs. y scatter plot, Delta_BIC vs. bayesFactor for all fibers in the lists."""
    fig = pl.figure()
    K, DBIC = zip(*xyTupleList)
    pl.scatter(np.log10(K), DBIC)
    pl.xlabel('log(Bayes Factor)')
    pl.ylabel('$\Delta$BIC')
    pl.xlim(-5, 70)
    pl.ylim(-150,20)
    pl.tight_layout()
    if plotDir is not None:
        fig.savefig(os.path.join(plotDir, 'DBIC_bayesfactor_cropped.png'))


def plot_param_hist(trace, label, plotDir=None, baseName=None, ylog=False, fig=None):
    """Plot a histogram of a single parameter trace."""
    if fig is None:
        fig = pl.figure()
        alpha = 1.0
    else:
        alpha = 0.5
    pl.hist(trace, 100, log=ylog, alpha=alpha)
    if label is not None:
        pl.xlabel(label)
    pl.ylabel('N')
    pl.tight_layout()
    if plotDir is not None:
        if baseName is not None:
            fig.savefig(os.path.join(plotDir, "%s-%s-paramHist.png" % (baseName, label)))
        else:
            fig.savefig(os.path.join(plotDir, "%s-paramHist.png" % label))
    else:
        return fig


def plot_DBIC_hist(DBICs, plotDir=None):
    """Plot a histogram of (binary_BIC - single_BIC)."""
    pl.figure(figsize=[12,12])
    pl.hist(DBICs, 100, log=True)
    pl.axvline(x=0.0, ls='--', color='black')
    #pl.text(50, 1E4, 'Likely Single')  # y-axis doesn't scale with sample size
    #pl.text(-50, 1E4, 'Likely Binary', horizontalalignment='right')
    pl.xlabel('BIC_binary - BIC_single')
    pl.ylabel('N')
    pl.title('Model Comparison')
    pl.tight_layout()
    if plotDir is not None:
        pl.savefig(os.path.join(plotDir, 'DBIC_histogram.png'))


def plot_BIC_scatter(binary_BIC, single_BIC, plotDir=None):
    """Plot a scatter of single_BIC vs. binary_BIC."""
    pl.figure(figsize=[12,12])
    pl.scatter(binary_BIC, single_BIC)
    high_limit = max(max(x), max(y))
    pl.plot([0, high_limit], [0, high_limit], '--')
    pl.title('Bayesian Information Criterion (BIC)')
    pl.xlabel('Binary Model')
    pl.ylabel('Single Model')
    pl.xlim(0)
    pl.ylim(0)
    pl.tight_layout()
    if plotDir is not None:
        pl.savefig(os.path.join(plotDir, 'BIC_scatter.png'))


def plot_orbit_comparison(obs, rvs, errs, single_orbit, binary_orbit, plotDir=None, fold_logP=None, title=None, single_BIC=None, binary_BIC=None):
    """Plot a fibers RV data, as well as a single_orbit and binary_orbit fit to the data.
    Allows the orbit to be folded into a single orbit given a fold_logP period."""
    obs = np.array([t - obs[0] for t in obs])  # UNEASY ABOUT OBS

    if fold_logP is not None:
        p = pow(10.0, fold_logP)
        obs = obs % p

    x_low, x_hi, y_low, y_hi = get_ranges(obs, rvs)
    xl = np.linspace(x_low, x_hi, 1000)

    pl.figure(figsize=[12,8])
    ax1 = pl.subplot(211)
    ax1.errorbar(obs, rvs, yerr=errs, fmt=".", color="LimeGreen", ms=8)
    ys = [single_orbit(x+obs[0]) for x in xl]  # doesn't this equal x + 0?
    yb = [binary_orbit(x+obs[0]) for x in xl]  # doesn't this equal x + 0?
    sLabel = "single"
    if single_BIC is not None:
        sLabel += " BIC=%.1f" % single_BIC
    bLabel = "binary"
    if binary_BIC is not None:
        bLabel += " BIC=%.1f" % binary_BIC
    ax1.plot(xl, ys, color="DodgerBlue", label=sLabel)
    ax1.plot(xl, yb, color="red", label=bLabel)


    ax1.set_ylim(y_low, y_hi)
    ax1.set_xlim(x_low, x_hi)
    ax1.set_ylabel("Radial Velocity (km/s)")
    pl.setp( ax1.get_xticklabels(), visible=False)
    ax1.legend(prop={'size':9})
    if title is not None:
        ax1.set_title(title)

    ax2 = pl.subplot(212, sharex=ax1)
    ax2.scatter(obs, single_orbit(obs)-rvs, color="DodgerBlue", s=18)
    ax2.scatter(obs, binary_orbit(obs)-rvs, color="red", s=18)
    ax2.errorbar(obs, np.zeros(len(obs)), yerr=errs, ls='none', color="LimeGreen")
    if fold_logP is not None:
        ax2.set_xlabel("Phase Folded Time (s)")
    else:
        ax2.set_xlabel("Time (s)")
    ax2.set_ylabel("Differential (km/s)")

    pl.subplots_adjust(hspace=0.00, bottom=0.1, left=0.1, right=0.9, top=0.9)
    if plotDir is not None:
        if fold_logP is not None:
            pl.savefig(os.path.join(plotDir, "orbit_comparison_folded.eps" ))
            pl.savefig(os.path.join(plotDir, "orbit_comparison_folded.png" ))
        else:
            pl.savefig(os.path.join(plotDir, "orbit_comparison.eps" ))
            pl.savefig(os.path.join(plotDir, "orbit_comparison.png" ))


def main():
    """For one or more fibers, make trace plot, triangle plot of traces, rv_curve, and rv_curve with random selection of traces overplotted."""
    parser = optparse.OptionParser(description='Written by Thomas Hettinger.')
    parser.add_option('-f', '--fiberid', dest='fiberid', action='store', type=str, help='name of fiber to run test on')
    parser.add_option('-l', '--listPath', dest='listPath', action='store', type=str, default=None, help='file containing list of fibers')
    parser.add_option('-t', '--traceDir', dest='traceDir', action='store', type=str, default=None, help='base dir for reading traces')
    parser.add_option('-d', '--dbPath', dest='dbPath', action='store', type=str, default=None, help='path to database with rv information')
    parser.add_option('-p', '--plotDir', dest='plotDir', action='store', type=str, default=None, help='base dir for plots')
    args = parser.parse_args()[0]

    if args.traceDir is None:
        raise Exception('Please specify a trace directory.')

    if args.listPath is not None:
        fiberlist = mcinout.load_fiberlist(args.listPath)
    elif args.fiberid is not None:
        fiberlist = [args.fiberid,]
    else:
        raise Exception('Specify fibers.')

    for	fiberid in fiberlist:
        tracePath = mcinout.find_path(args.traceDir, fiberid)
        if tracePath is None:
            raise Exception('Could not find fiber trace file.')
        traces = mcinout.load_trace(tracePath)

        thisPlotDir = os.path.join(args.plotDir, fiberid)
        if not os.path.isdir(thisPlotDir):
            os.mkdir(thisPlotDir)

        plot_traces(traces, plotDir=thisPlotDir, baseName=fiberid)
        plot_parameter_triangle(traces, plotDir=thisPlotDir, baseName=fiberid)

        if args.dbPath is not None:
            tobs, rvs, errs = mcinout.load_fiber(args.dbPath, fiberid)
            tobs = tobs - tobs[0]    
            plot_rvcurve(tobs, rvs, errs, thisPlotDir, baseName=fiberid)
            plot_sinusoid_samples(traces, tobs, rvs, errs, plotDir=thisPlotDir, baseName=fiberid)


if __name__ == "__main__":
    main()
