"""
Functions for running an emcee Markov-Chain Monte-Carlo.

These include the likelihood calculations for the prior and the parameters, as
well as those needed to create the walkers and set up and run the MCMC chain.

Contains functions for the plate wide MCMC chain.

"""
import time

import numpy as np

from multiplicity.radialvelocity import absoluterv

TWOPI = 2.0*np.pi
OUTLIER_THRESH = 600.0
MEAN_RVS = False
SHIFTS = False
BINARY_OUTLIERS = False
SHIFT_ONLY = True


def run_chain(sampler, pos, nwalkers, nsteps=5000, thin=1, verbose=False):
    """Run the sampler chain."""
    if verbose:
        print "Starting %d walkers taking %d steps each (%d total)" % (nwalkers, nsteps, nwalkers*nsteps)
        print "Running MCMC..."
    timeBegin = time.time()
    sampler.run_mcmc(pos, nsteps, rstate0=np.random.get_state(), thin=thin)
    timeEnd = time.time()
    timeElapsed = timeEnd - timeBegin
    if verbose:
        print "Finished %d walkers taking %d steps each (%d total) in %f seconds." % (nwalkers, nsteps, nwalkers*nsteps, timeElapsed)
    return timeElapsed


def theta_to_params(theta, fiberCount, expCount):
    """Take a 1D array of params and return the appropriate sized
    arrays for each param."""
    pBin = theta[0]
    dy = theta[1:expCount]
    foo = theta[expCount:]
    try:
        bar = np.reshape(foo, [2, fiberCount])
    except:
        print foo
    v0 = bar[0]
    varBin = bar[1]
    return pBin, dy, v0, varBin


def lnprior(theta, priorLimits, fiberCount, expCount):
    """
    These are the priors on my parameters.
    prob(pBin, [dy], [v0], [varBin])

    ln(prior probability)
    p(theta_i)
    """
    if MEAN_RVS:
        v0 = theta
        for j in range(fiberCount):
            if not (priorLimits['v0'][0] < v0[j] < priorLimits['v0'][1]):
                return -np.inf
        return 0.0
    elif SHIFTS:
        dy = theta[:expCount-1]
        v0 = theta[expCount-1:]
        for i in range(expCount-1):
            if not (priorLimits['dy'][0] < dy[i] < priorLimits['dy'][1]):
                return -np.inf
        for j in range(fiberCount):
            if not (priorLimits['v0'][0] < v0[j] < priorLimits['v0'][1]):
                return -np.inf
        return 0.0
    elif SHIFT_ONLY:
        dy = theta
        for i in range(expCount):
            if not (priorLimits['dy'][0] < dy[i] < priorLimits['dy'][1]):
                return -np.inf
        return 0.0
    elif BINARY_OUTLIERS:
        pBin, dy, v0, varBin = theta_to_params(theta, fiberCount, expCount)
        if not (priorLimits['pBin'][0] < pBin < priorLimits['pBin'][1]):
            return -np.inf
        for i in range(expCount-1):
            if not (priorLimits['dy'][0] < dy[i] < priorLimits['dy'][1]):
                return -np.inf
        for j in range(fiberCount):
            if not (priorLimits['v0'][0] < v0[j] < priorLimits['v0'][1]):
                return -np.inf
        for j in range(fiberCount):
            if not(priorLimits['varBin'][0] < varBin[j] < priorLimits['varBin'][1]):
                return -np.inf
        return 0.0


def lnlike(theta, x, y, yerr, fiberCount, expCount):
    """
    These are the Likelihoods for the data given the model parameters in a mixture model.
    prob(RVs | pBin, [dy], [v0], [varBin])

    The stars can either be good, drawn from constant RV model, or bad stars (binaries) with unusually high variances.

    ln(likelihood)
    p(y_i | theta_i)
    """
    if MEAN_RVS:
        v0 = theta
    elif SHIFTS:
        dy = [0.0,]
        dy.extend(theta[:expCount-1])
        v0 = theta[expCount-1:]
    elif SHIFT_ONLY:
        dy = theta
        # Calculate the mean radial velocity
        meanY = []
        for j in range(fiberCount):
            these_y, these_yerr = [], []
            for i in range(len(y[j])):
                if y[j][i] < OUTLIER_THRESH:
                    these_y.append(y[j][i])
                    these_yerr.append(yerr[j][i])
            meanY.append(absoluterv.calc_weighted_mean(these_y, these_yerr))
    elif BINARY_OUTLIERS:
        dy = [0.0,]
        pBin, dy_i, v0, varBin = theta_to_params(theta, fiberCount, expCount)
        dy.extend(dy_i)

    if BINARY_OUTLIERS:
        total_L = 1.0
        for j in range(fiberCount):
            fiber_L = 1.0
            for i in range(expCount):
                yvar = yerr[j][i]**2.
                # Allow for binary 'outliers' (not sure if it is working yet)
                A = (1. - pBin) / np.sqrt(TWOPI * yvar)
                B = np.exp(-0.5 * (y[j][i] - v0[j] - dy[i])**2. / yvar)
                C = pBin / np.sqrt(TWOPI * (varBin[j] + yvar))
                D = np.exp(-0.5 * (y[j][i] - v0[j] - dy[i])**2. / (varBin[j] + yvar))
                fiber_L *= A*B + C*D
            total_L *= fiber_L
        if total_L <= 0:
            return -np.inf
        else:
            return np.log(total_L)

    else:
        total_lnL = 0.0
        for j in range(fiberCount):
            fiber_lnL = 0.0
            for i in range(expCount):
                yvar = yerr[j][i]**2.
                if abs(y[j][i]) > OUTLIER_THRESH:
                    continue
                if MEAN_RVS:
                    fiber_lnL += np.log(yvar) + (y[j][i] - v0[j])**2. / yvar
                elif SHIFTS:
                    fiber_lnL += np.log(yvar) + (y[j][i] - v0[j] - dy[i])**2. / yvar
                elif SHIFT_ONLY:            
                    fiber_lnL += np.log(yvar) + (y[j][i] - meanY[j] - dy[i])**2. / yvar
            total_lnL += fiber_lnL
        total_lnL *= -0.5
        return total_lnL


def lnprob(theta, x, y, yerr, priorLimits):
    """
    These are the posteriors for parameters, given the data.
    prob(pBin, [dy], [v0], [varBin] | RVs)
    The posterior is equal to the likelihood * the prior

    ln(posterior probability)
    p(theta_i | y_i) = p(y_i | theta_i) * p(theta_i)
    """
    fiberCount = len(y)
    expCount = len(x)
    lp = lnprior(theta, priorLimits, fiberCount, expCount)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike(theta, x, y, yerr, fiberCount, expCount)


def get_walkers(rvMatrix, nExp, nFiber, nwalkers, ndim):
    """Create a ball of walker starting positions. The ball is a ndim ball
    in parameter space, centered around guesses for the true param values.
    pBin, [dy], [v0], [varBin]"""
    # Create a ball of walkers around the best guess params.
    if MEAN_RVS:
        params_guess = [np.mean(rvMatrix[j]) for j in range(nFiber)]  # [v0]
    elif SHIFTS:
        params_guess = []
        params_guess.extend([10.0 for i in range(nExp-1)])     # [dy]
        params_guess.extend([np.mean(rvMatrix[j]) for j in range(nFiber)])   # [v0]
    elif SHIFT_ONLY:
        params_guess = [10.0 for i in range(nExp)]  # [dy]
    elif BINARY_OUTLIERS:
        params_guess = [0.1,]                                  # pBin
        params_guess.extend([10.0 for i in range(nExp-1)])     # [dy]
        params_guess.extend([np.mean(rvMatrix[j]) for j in range(nFiber)])   # [v0]
        params_guess.extend([300.0 for j in range(nFiber)]) # [varBin]

    pos = [params_guess * (1 + 1E-1*np.random.randn(ndim)) for i in range(nwalkers)]
    return pos


