"""
Input and output functions related to MCMC and likelihood results.

    * find_path() for trace files in a traceDir
    * obs, rvs, errs = load_fiber() from sqlite3
    * load_trace()
    * etc.

"""

import os, sys, glob

import numpy as np
from random import sample

from multiplicity.sqlwrapper.Database import RVDatabase

## INPUT ##
###########
def find_path(traceDir, fiberid, dataType='traces'):
    """Find the trace file for this fiberid in traceDir.  If multiple,
    choose the first one after sorting. Can also locate summary and shift files."""
    pathname = os.path.join(traceDir, '%s_*-%s.dat' % (fiberid, dataType))
    pathlist = glob.glob(pathname)
    pathlist.sort()
    if pathlist:
        return pathlist[0]
    else:
        return None


def load_pop_exp_details(dbPath, fiberlistPath, verbose=False):
    """Open the database and get all exposure information (tobs, rv_err, e/i) for the fibers
    listed in the pmf file.  Return three lists (tobs, err, eoi), where
    tobs and err are 2D and eoi is 1D."""
    fiberlist = load_fiberlist(fiberlistPath)
    db = RVDatabase(dbPath, silent=(not verbose))
    tobsList = []
    errList = []
    eoiList = []
    for fiberid in fiberlist:
        if db.id_in_table(fiberid) is None:
            print '%s not found in table' % fiberid
        rawTimes = db.get_value(fiberid, 'taimids')        
        obsTimes = [int(float(taimid.strip(','))) for taimid in rawTimes[1:].strip(']').split()]
        rawErrors = db.get_value(fiberid, 'empiricalErrs')
        measurementErrors = [float(err) for err in rawErrors[1:].strip(']').split(',')]
        eoi = db.get_value(fiberid, 'eoveri')
        tobsList.append(np.array(obsTimes))
        errList.append(np.array(measurementErrors))
        eoiList.append(eoi)
    del db
    return tobsList, errList, eoiList


def load_fiber(dbPath, fiberid, verbose=False):
    """Load the obstimes, absolute radial velocities, and associated errors 
    from the sqlite database."""
    db = RVDatabase(dbPath, silent=(not verbose))
    if db.id_in_table(fiberid) is None:
        raise Exception('Fiber %s data not found in %s.' % (fiberid, dbPath))
    rawTimes = db.get_value(fiberid, 'taimids')
    obsTimes = [int(float(taimid.strip(','))) for taimid in rawTimes[1:].strip(']').split()]
    rawRVs = db.get_value(fiberid, 'absRVs')
    fiberVelocities = [float(rv) for rv in rawRVs[1:].strip(']').split(',')]
    rawErrors = db.get_value(fiberid, 'empiricalErrs')
    measurementErrors = [float(err) for err in rawErrors[1:].strip(']').split(',')]
    del db
    return np.array(obsTimes), np.array(fiberVelocities), np.array(measurementErrors)


def load_trace(tracePath):
    """Read contents of trace path and return a ndarray of values
    containing the traces.  Assumes all parameters can be expressed
    as a float. First index chooses parameter, second index is the 
    trace number."""
    if tracePath is None:
        raise Exception('tracePath given is None.')
    if not os.path.isfile(tracePath):
        raise Exception('File %s not found.' % tracePath)
    else:
        traces = []
        with open(tracePath, 'r') as inFile:
            for line in inFile:
                if line[0] == '#':
                    continue
                if len(line.strip()) == 0:
                    continue
                cols = line.split()
                traces.append([float(c) for c in cols])
        return np.array(traces)


def load_all_param_traces(fiberlist, traceDir, paramIndex=0):
    """For all fibers in the list, get the logP traces and
    return results in a 2D numpy array.  Specify index for parameter
    to be returned."""
    sys.stdout.write('Reading trace files...\n')
    results = []
    for fiberid in fiberlist:
        tracePath = find_path(traceDir, fiberid)
        if tracePath is None:
            sys.stderr.write('Warning: Could not find fiber trace file. Skipping for now. %s\n' % tracePath)
            continue
        traces = load_trace(tracePath)
        if traces.shape[0] > traces.shape[1]:
            traces = traces.T
        results.append(traces[paramIndex])
    return np.array(results)


def load_summary_old(dataPath, paramKeys=['logA', 'logP', 'phi', 'b', 'f', 'binRoll']):
    """Read the summary.dat file and return summary information for
    the parameters. Deprecated."""
    if not os.path.isfile(dataPath):
        raise Exception('File %s not found.' % dataPath)
    else:
        resultDict = {}
        with open(dataPath, 'r') as inFile:
            for line in inFile:
                firstWord = line.split()[0]
                if firstWord in paramKeys:
                    cols = line.split()
                    resultDict[firstWord] = (float(cols[2]), float(cols[3]), -1.*float(cols[4]))
        return resultDict


def load_summary(dataPath, paramKeys=['logA', 'logP', 'phi', 'b', 'f', 'binRoll']):
    """Read the summary.dat file and return summary information, including
    mcmc results for any parameters specificed in paramKeys.  Returns a
    dictionary of keys and values."""
    generalKeys = ['fiberid', 'nwalkers', 'nsteps', 'nthin', 'nburn', 'totalTraceCount', 'mean_acceptance', 'acorTimes', 'timeElapsed']
    if not os.path.isfile(dataPath):
        raise Exception('File %s not found.' % dataPath)
    resultDict = {}
    with open(dataPath, 'r') as inFile:
        for line in inFile:
            if line[0] == '#':
                continue
            if len(line.strip()) == 0:
                continue
            firstWord = line.split()[0][:-1]
            if (firstWord in generalKeys) or (firstWord in paramKeys):
                resultDict[firstWord] = line.split()[1:]
    return resultDict


def load_averages(dataPath):
    """Read the .dat file containing fiber averages and return them."""
    if not os.path.isfile(dataPath):
        raise Exception('File %s not found.' % dataPath)
    fids, fiberMeans, fiberMedians = [], [], []
    with open(dataPath, 'r') as inFile:
        for line in inFile:
            if line[0] == '#':
                continue
            cols = line.split()
            thisFID = cols[0]
            averages = cols[1:]
            theseMeans = tuple([float(averages[i]) for i in range(len(averages)) if i % 2 == 0]) # even columns 0,2,4
            theseMedians = tuple([float(averages[i]) for i in range(len(averages)) if i % 2 == 1]) # odd columns 1,3,5
            fids.append(thisFID)
            fiberMeans.append(theseMeans)
            fiberMedians.append(theseMedians)
    return np.array(fids), np.array(fiberMeans), np.array(fiberMedians)


def load_fiberlist(listPath):
    """Open a fiberlist file and make a list of PMFs to return."""
    if not os.path.isfile(listPath):
        raise Exception('File %s not found.' % listPath)
    with open(listPath, 'r') as inFile:
        return [line.strip() for line in inFile.readlines() if len(line.strip()) > 0]


def load_likelihood_datafile(dataPath):
    """Load a file containing 'best' binary and single orbits as well as their
    likelihood values (one fiber per line) and return a dictionary {fiberid:tuple}."""
    dataDict = {}
    with open(dataPath, 'r') as inFile:
        for line in inFile.readlines():
            if line[0] == '#': continue
            tup = line.split()
            fiberid = tup[0]
            data = tup[1:]
            data = [float(d) for d in data]
            dataDict[fiberid] = tuple(data)
    return dataDict


def load_bayesfactor_datafile(dataPath):
    """Load a file containing the posterior values for the binary and single models, 
    and the Bayes factor for the two models.  Return a dictionary {fiberid:tuple}."""
    return load_likelihood_datafile(dataPath)


def load_isBinary_datafile(dataPath):
    """Load a file containing the fractions of traces that are isBinary == True, 
    Return a dictionary {fiberid:tuple}."""
    return load_likelihood_datafile(dataPath)


def load_chainconfig(configPath):
    """Load the chainrunner config file containing keywords and values.
    Must contain {walkers, steps, burn, thin} each followed by a value.
    Must contain {logA, logP, phi, b} each followed by 2 values (lower
    and upper limit on the flat prior).  Returns a dictionary."""
    configDict = {}
    with open(configPath, 'r') as inFile:
        for line in inFile.readlines():
            if len(line.strip()) == 0: continue
            if line[0] == '#': continue
            tup = line.split()
            key = tup[0]
            if key == 'walkers':
                configDict['walkers'] = int(tup[1])
            elif key == 'steps':
                configDict['steps'] = int(tup[1])
            elif key == 'burn':
                configDict['burn'] = int(tup[1])
            elif key == 'thin':
                configDict['thin'] = int(tup[1])
            elif key == 'logA':
                configDict['logA'] = ( float(tup[1]), float(tup[2]) )
            elif key == 'logP':
                configDict['logP'] = ( float(tup[1]), float(tup[2]) )
            elif key == 'phi':
                configDict['phi'] = ( float(tup[1]), float(tup[2]) )
            elif key == 'b':
                configDict['b'] = ( float(tup[1]), float(tup[2]) )
            elif key == 'offset':
                configDict['offset'] = ( float(tup[1]), float(tup[2]) )
            elif key == 'isBinary':
                configDict['isBinary'] = ( float(tup[1]), float(tup[2]) )
            elif key == 'Pb':
                configDict['Pb'] = ( float(tup[1]), float(tup[2]) )
            elif key == 'Yb':
                configDict['Yb'] = ( float(tup[1]), float(tup[2]) )
            elif key == 'Vb':
                configDict['Vb'] = ( float(tup[1]), float(tup[2]) )
            elif key == 'alpha':
                configDict['alpha'] = ( float(tup[1]), float(tup[2]) )
            elif key == 'fbin':
                configDict['fbin'] = ( float(tup[1]), float(tup[2]) )
            elif key == 'beta':
                configDict['beta'] = ( float(tup[1]), float(tup[2]) )
            elif key == 'modelMultiplier':
                configDict['modelMultiplier'] = int(tup[1])
    return configDict


def load_plateids(plateidPath, fiberid):
    """Load a plateid CSV file with the list of indicies corresponding to plate ids.
    Return the plateids and indicies for the fiberid specified."""
    with open(plateidPath, 'r') as inFile:
        for line in inFile.readlines():
            if len(line.strip()) == 0 or line[0] == '#':
                continue
            tup = line.split(',')
            if tup[0] == fiberid:
                plateTupList = []
                plateStrings = tup[1:]
                for plateString in plateStrings:
                    pm = plateString.split(';')[0]
                    these_idx = plateString.split(';')[1:]
                    these_idx = [int(idx) for idx in these_idx]
                    plateTupList.append((pm, these_idx))
                return plateTupList
        raise Exception('%s not found in %s' % (fiberid, plateidPath))


def load_shifts(dataPath):
    """Load file containing calculated shifts. Return a dictionary with
    the measured shifts, keyed by TAIMID."""
    if not os.path.isfile(dataPath):
        raise Exception('File %s not found.' % dataPath)
    shiftDict = {}
    with open(dataPath) as inFile:
        for line in inFile.readlines():
            if (line[0] == '@') or (line[0] == '#'):
                continue
            taimid, shift = line.strip().split()
            shiftDict[int(taimid)] = float(shift)
    return shiftDict


## OUTPUT ##
############
def write_trace(traces, outPath, headline=None, size=0):
    """Write the traces of a fiber to a data file (space delimited).
    If an integer is specified for subset, write that many (randomly
    selected) traces."""
    with open(outPath, 'w') as outFile:
        if headline is not None:
            outFile.write(headline)
            outFile.write('\n')
        if size > 0 and size < len(traces):
            subset = sample(traces, size)
        else:
            subset = traces
        for t in subset:
            for p in range(len(t)):
                outFile.write("%.4f  " % t[p])
            outFile.write("\n")


def write_summary(outPath, nwalkers, nsteps, nthin, nburn, totalTraceCount, mean_acceptance=None, acorTimes=None, timeElapsed=None, binaryPercentage=None, quantiles=None, labels=None):
    """After chain is finished, write a small file for this fiber with a 
    summary of the results (best values, time to run, acor times, etc.)."""
    with open(outPath, 'w') as outFile:
        outFile.write('nwalkers: %d\n' % nwalkers)
        outFile.write('nsteps: %d\n' % nsteps)
        outFile.write('nthin: %d\n' % nthin)
        outFile.write('nburn: %d\n' % nburn)
        outFile.write('totalTraceCount: %d\n' % totalTraceCount)

        if mean_acceptance is not None:
            outFile.write('mean_acceptance: %.2f\n' % mean_acceptance)
        if acorTimes is not None:
            outFile.write('acorTimes:  ')
            for time in acorTimes:
                outFile.write('%.2f  ' % time)
            outFile.write('\n')
            outFile.write('effective_samples:  ')
            for time in acorTimes:
                outFile.write('%.2f  ' % (totalTraceCount/time))
            outFile.write('\n')
        if timeElapsed is not None:
            outFile.write('timeElapsed: %d\n' % timeElapsed)
        if binaryPercentage is not None:
            outFile.write('isBinPer: %f\n' % binaryPercentage)
        if (quantiles is not None) and (labels is not None):
            for i,param in enumerate(quantiles):
                outFile.write("%s:  %.3f  %.3f  %.3f\n" % (labels[i], param[0], param[1], param[2]))

        outFile.write('#%d walkers were used\n' % nwalkers)
        if mean_acceptance is not None:
            outFile.write('#    with a mean acceptance ratio of %.2f\n' % mean_acceptance)
        outFile.write('#Each walker took %d steps, was thinned by %d then burned by %d\n' % (nsteps, nthin, nburn))
        outFile.write('#  resulting in %d steps per walker (%d total) \n' % (nsteps/nthin - nburn, totalTraceCount))


def write_fiberlist(outPath, fiberlist):
    """Write a list of fiberid's that can be found."""
    if os.path.exists(outPath):
        sys.stderr.write('File %s already exists. Not writing fiberlist.\n' % outPath)
    else:
        with open(outPath, 'w') as outFile:
            for fiberid in fiberlist:
                outFile.write('%s\n' % fiberid)

