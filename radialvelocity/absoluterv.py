"""
Funtions for making absolute radial velocity measurements on Fiber (all sub-spectra)
using a template spectrum.  Includes functions for updating an SQL database with measurments.
Also includes functions for calulating e/i properly.

"""
import os

import numpy as np
from scipy.special import gamma

from multiplicity.sqlwrapper.Database import RVDatabase
from multiplicity.radialvelocity import correlation, rvinout

SPEED_OF_LIGHT = 299792.458 # km/s


def c4(N):
    """Returns the scale mean of the chi distribution with 
    N-1 degrees of freedom (known as c4) for further
    correcting the 'corrected sample standard deviation'.
        
    Returns 1.0 if N > 1000."""
    if N > 300:
        return 1.0
    else:
        return np.sqrt(2.0 / (N-1.0)) * gamma(N/2.0) / gamma((N-1.0)/2.0)


def calc_unbiased_sample_standard_deviation(measurements):
    """Use Bessel's correction and c4 to estimate
    the standard deviation of the population from 
    which the RV sample was pulled.  This is unbiansed
    and corrected.  This is not weighted through weights."""
    correctedSampleSTD = np.std(measurements, ddof=1)
    unbiasedSampleSTD = correctedSampleSTD / c4(len(measurements))
    return unbiasedSampleSTD


def calc_weighted_mean(measurements, errors):
    """Calculate and return a weighted mean measurement."""
    weights = [1.0/err/err for err in errors]
    return np.average(measurements, weights=weights)


def calc_weighted_standard_error(measurements, errors):
    """Calculate the standard error of the mean using a weighted
    standard deviation."""
    weights = [1.0/err/err for err in errors]
    sampleVar = calc_weighted_variance(measurements, errors)
    effectiveBase = sum(weights)*sum(weights) / sum([w*w for w in weights])
    SVM = sampleVar / effectiveBase
    return np.sqrt(SVM)


def calc_weighted_variance(measurements, errors):
    """Calculate the corrected standard devation using a weighted average.
    Assumes errors are non-zero."""
    weightedMean = calc_weighted_mean(measurements, errors)
    M = float(len(errors))
    weights = [1./err/err for err in errors]
    numerator = sum([(x - weightedMean)**2 * w for x,w in zip(measurements, weights)])
    denominator = (M-1.) / M * sum(weights)
    sigmaSquared = numerator / denominator
    return sigmaSquared


def calc_weighted_std(measurements, errors):  # a.k.a 'e'
    """Return the weighted standard deviation of the measurments
    corrected, but still biased."""
    s2 = calc_weighted_variance(measurements, errors)
    return np.sqrt(s2)


def calc_iSquared(errors):
    """Calculate the average measurement variance."""
    M = float(len(errors))
    iSquared = sum([err*err for err in errors]) / M
    return iSquared


def calc_i(errors):
    """Calculate the average measurement uncertainty."""
    return np.sqrt(calc_iSquared(errors))


def calc_eoveri(measurements, errors):
    """Calculate the ratio of the external deviations in measurments
    to the internal measurement errors."""
    e = calc_weighted_std(measurements, errors)
    i = calc_i(errors)
    return e / i


def identify_clean_exposures(dbPath, minPix=3000, minSNR=20.0):
    """For each row in the database, identify the exposures that meet the
    minimum requirements and update 'cleanExposureCount' and 'cleanExposureIndex'"""
    db = RVDatabase(dbPath)
    rows = db.get_rows_with_fields('fiberid', 'exposureGoodPix', 'exposureSNR')
    for row in rows:
        pixTuple = row[1][1:-1].split(',')
        pixList = [int(tup) for tup in pixTuple]
        snrTuple = row[2][1:-1].split(',')
        snrList = [float(tup) for tup in snrTuple]
        assert len(pixList) == len(snrList)

        qualityExpIndex = []
        for i in range(len(pixList)):
            if pixList[i] >= minPix and snrList[i] >= minSNR:
                qualityExpIndex.append(i)

        db.set_value(row[0], 'cleanExposureCount', len(qualityExpIndex))
        db.set_value(row[0], 'cleanExposureIndex', str(qualityExpIndex))

    del db


def quality_combine(dbPath, outDir, fiberManifestPath, snrMin=20.0, pixMin=3000, expMin=3):
    """Using the fiber manifest, go through the input dbPath and identify all exposures that
    meet the standards for each star.  Combine rv data into a single entry with the primary_pmf
    and the primary SSPP values.  Update exposureCount, etc to match new values.  Output
    to a new database.  Also write out a new manifests (with and without nExp) for fibers 
    that still make the cut."""
    newDBPath = os.path.join(outDir, 'new_combinedQuality.sqlite3')
    if not os.path.exists(newDBPath):
        newDB = RVDatabase(newDBPath, create=True)
    else:
        newDB = RVDatabase(newDBPath, create=False)
    fiberManifestDict = rvinout.load_fiber_manifest(fiberManifestPath)
    rvDataDict = rvinout.load_data(dbPath, silent=True, returnAsDict=True)

    allPMFsUsed = []
    newManifestDict = {}            # {star1:[(pmf_1, nQualityExp_1), (...),],   star2:[...], ...}
    # For each Star
    for prim_pmf, pmfList in fiberManifestDict.iteritems():
        # Get primary_pmf params for new database
        newFeh = rvDataDict[prim_pmf]['feh']
        newLogg = rvDataDict[prim_pmf]['logg'] 
        newTeff = rvDataDict[prim_pmf]['teff']
        newGmag = rvDataDict[prim_pmf]['gmag']

        thisStarFiberTups = []      # [(pmf_1, nQualityExp_1), (...), ...]
        thisStarExposureTups = []   # [(taimid, exptime, rv, snr, goodPix, plateid), (...), ...]
        # For each Fiber
        for pmf in pmfList:
            # Grab this fibers exposure information
            thisData = rvDataDict[pmf]
            taimids = thisData['taimids']
            exptimes = thisData['exptimes']
            absRVs = thisData['absRVs']
            exposureSNR = thisData['exposureSNR']
            exposureGoodPix = thisData['exposureGoodPix']

            # Locate quality exposures
            qualityExpIndex = []
            for i in range(len(exposureSNR)):
                if exposureGoodPix[i] >= pixMin and exposureSNR[i] >= snrMin:
                    qualityExpIndex.append(i)

            # If no quality exposures, go to next Fiber
            if len(qualityExpIndex) == 0:
                continue
            else:
                # Add this fiberid and the number of quality exposures to this stars manifest
                thisStarFiberTups.append( (pmf, len(qualityExpIndex)) )
                # Add the exposure information to this stars list of exposure info
                for idx in qualityExpIndex:
                    thisStarExposureTups.append( (taimids[idx], exptimes[idx], absRVs[idx], exposureSNR[idx], exposureGoodPix[idx], pmf[:10]) )

        # If no quality Fibers or if the Primary is not included, 
        #    or if not enough exposures, then go to next star
        if len(thisStarFiberTups) == 0:
            continue
        elif prim_pmf not in [t[0] for t in thisStarFiberTups]:
            continue
        elif len(thisStarExposureTups) < expMin:
            continue

        # Add fibers used to the full list of fibers
        for pmf in [t[0] for t in thisStarFiberTups]:
            allPMFsUsed.append(pmf)

        # Add star and list of (Fibers,expCount) to manifest dictionary
        newManifestDict[prim_pmf] = thisStarFiberTups

        # Sort exposure information in order of taimids then zip into individual variables
        thisStarExposureTups.sort(key=lambda t: t[0])
        newTaimids, newExptimes, newAbsRVs, newExposureSNR, newExposureGoodPix, newPlateIDs = zip(*thisStarExposureTups)
        newExposureCount = len(newTaimids)

        if newDB.id_in_table(prim_pmf) is None:
            newDB.insert_entry(prim_pmf)
        newDB.set_many_values(prim_pmf, exposureCount=newExposureCount, exposureSNR=str(list(newExposureSNR)), exposureGoodPix=str(list(newExposureGoodPix)), 
                              taimids=str(list(newTaimids)), exptimes=str(list(newExptimes)), absRVs=str(list(newAbsRVs)), feh=newFeh, logg=newLogg, teff=newTeff, gmag=newGmag )

        # Write out manifest with plateids
        rvinout.write_plateid_manifest(prim_pmf, newPlateIDs, os.path.join(outDir, 'new_plateidManifest.csv'))

    # Write out full list of fibers
    with open(os.path.join(outDir, 'qualityFibers.pmf'), 'w') as outFile:
        for pmf in allPMFsUsed:
            outFile.write('%s\n' % pmf)

    # Write out manifest with and without exposure counts
    rvinout.write_fiber_mainifest(newManifestDict, os.path.join(outDir, 'new_fiberManifest.csv'), includeExpCount=False)
    rvinout.write_fiber_mainifest(newManifestDict, os.path.join(outDir, 'new_fiberManifest_withExp.csv'), includeExpCount=True)
    print '%d total stars made of %d total fibers made it to quality list' % (len(newManifestDict), len(allPMFsUsed))
    print 'with up to %d fibers per star.' % max([len(value) for key,value in newManifestDict.iteritems()])



    del newDB


def update_database(fiberid, dbPath, fiber, absRVs, flags, goodPixelCounts):
    """Open the database and set the values for this fiberid."""
    db = RVDatabase(dbPath)
    if db.id_in_table(fiberid) is None:
        db.insert_entry(fiberid)
    db.set_many_values(fiberid, exposureCount=fiber.nExp, exposureSNR=str(fiber.snr), exposureGoodPix=str(goodPixelCounts), 
                       taimids=str(fiber.taimids), exptimes=str(fiber.exptimes), absRVs=str(absRVs), flags=str(flags))
    del db


def calculate_RV(cleanSpectra, template):
    """Run the calculate_lags() function from the correlation module then
    convert the the lag into km/s and return the radial velocities."""
    # Correlate
    lags, lag_errs, flags = [], [], []
    for spec in cleanSpectra:
        thisLag, thisLag_err, thisFlags = correlation.calculate_lag(spec.flux, spec.var, template.flux, template.var)
        lags.append(thisLag)
        lag_errs.append(thisLag_err)
        flags.append(thisFlags)

    # Convert to km/s
    angstromsPerPixel = (template.lamb[-1] - template.lamb[0]) / len(template.lamb)
    midWave = template.lamb[template.n / 2]
    velocityPerAngstrom = SPEED_OF_LIGHT / float(midWave)
    velocityPerPixel = velocityPerAngstrom * angstromsPerPixel    
    absRVs = [l*velocityPerPixel for l in lags]
    absRV_errs = [e*velocityPerPixel for e in lag_errs]

    return absRVs, flags

