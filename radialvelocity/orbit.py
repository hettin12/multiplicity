#!/usr/bin/env python
"""Contains functions regarding orbits and sin waves. 

* Make a sin wave or return values sampled from a sin wave.
* Calculate a weighted average of RVs to return a systemic velocity.

Future:
* Uses Kepler's laws to find unknowns from an orbit given the
other variables.  
* Calculates radial velocities from a binary orbit.  
"""
import numpy as np

G = 3.96425039E-14         # Gravitational constant in units [AU^3 / s^2 / Msun]
PI = np.pi
AU_TO_KM = 1.49598E8
SEC_TO_DAY = 1.15740741E-5
RSUN_TO_AU = 4.6491E-3
DEG_TO_RAD = np.pi / 180.0


def sine_wave(semiamplitude, period, phase, offset):
    """Return a function for a sine wave."""
    return lambda t: semiamplitude*np.sin(2*np.pi*t/period + phase) + offset


def sine_wave_from_params(logA, logP, phase, offset):
    """Return a function for a sine wave.  Here, the arguments
    are in terms of trace parameters."""
    semiamp = pow(10.0, logA)
    period = pow(10.0, logP)
    return sine_wave(semiamp, period, phase, offset)


def single_star_orbit(systemicVelocity):
    """Return a flat RV curve function given a systemic velocity."""
    return lambda t: 0.0*t + systemicVelocity


def calc_systemic_velocity(rvs, errs):
    """Return a systemic velocity assuming a single star system.
    Uses a weighted average of radial velocities."""
    weights = [1./sigma/sigma for sigma in errs]
    offset, invar = np.average(rvs, weights=weights, returned=True)
    spread = np.sqrt(1. / invar)
    return offset, spread


# KEPLER FUNCTIONS
def calculate_period(m1, m2, a):
    """Use Keppler's law to return a period. G is in AU, sec, Msun.
        
    Expect masses in Msun and separation in AU.
    m1, m2, a should be numpy arrays or single values.

    Return the period in seconds."""
    period = np.sqrt( pow(a,3)*4.*PI*PI / (G*(m1+m2)) )       # Calculate the period in seconds.
    return period


def calculate_amplitude(m1, m2, a, incl=90.0*DEG_TO_RAD, e=0):
    """Given orbital parameters, determine the (primay mass) amplitude of the system.
    Return the value in km/s.  Input should be Msun, AU, radians."""
    M = m1 + m2
    mu = (m1 * m2) / M
    r = (a*(1.0 - e*e)) / (1.0 + e)
    v = np.sqrt(G*M*(2./r - 1./a))
    v_rad = -1.0 * v * np.sin(incl)
    amplitude = (mu * v_rad / m1 * AU_TO_KM)
    return np.abs(amplitude)


def calculate_velocities(tobs, m1, q, a, rv0, incl, nu, e=0., phi=0.):
    """Use algorithm from Carroll & Ostlie to calculate RV
    given the orbital parameters.

    tobs = [t1, t2, t3, ...] All observation times for this fiber in seconds.
    Masses are in Msun, separation in AU.

    incl, phi, nu are in radian units.

    rv0 is in km/s.

    Returns absolute RVs in km/s."""
    # Grab specific values for shortcuts
    m2 = q*m1
    M = m1 + m2                                     # Total mass
    mu = (m1 * m2) / M                              # Reduced mass

    # Initialize values
    radVel1 = []
    r = 0.0
    t = 0.0                                         # Time in seconds
    dt = 0.0
    dnu = 0.0
    L = mu * np.sqrt(G*M*a*(1.0 - e*e))             # Angular momentum
    dAdt = L / (2.*mu)                              # Keplerian area

    # Calculate radial velocity for each exposure, then take a step based on time until next observation.
    for exp in range(len(tobs)):
        if exp != 0: 
            dt = tobs[exp] - tobs[exp-1]
            dnu = 2. * dAdt * dt / (r*r)
        t = t + dt
        nu = nu + dnu

        r = (a*(1.0 - e*e)) / (1.0 + e*np.cos(nu))
        v = np.sqrt(G*M*(2./r - 1./a))
        v_rad = -1.0 * v * np.sin(incl) * np.sin(nu+phi)  # AU/s
        radVel1.append((mu * v_rad / m1 * AU_TO_KM) + rv0) # km/s   Radial velocity of the primary
        #radVel2.append((-mu * v_rad / m2 * AU_TO_KM) + rv0) # km/s   Radial velocity of the secondary

    return np.array(radVel1)


