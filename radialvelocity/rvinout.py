"""
Input and output functions related to radial velocity measurements and RVDatabases.

    * load_data() to convert RVDatabase info into a python list (or ditionary).

"""

import os
 
from multiplicity.sqlwrapper.Database import RVDatabase

def to_int_list(dbString):
    """Convert a str(List) value from the database into
    a tuple of ints."""
    if dbString == '[]':
        return []
    return [int(x) for x in dbString[1:-1].split(', ')]


def to_float_list(dbString):
    """Convert a str(List) value from the database into
    a tuple of floats."""
    if dbString == '[]':
        return []
    return [float(x) for x in dbString[1:-1].split(', ')]


def load_fiber_manifest(pathToManifest):
    """Return a dictionary of primary fiber PMF keys with values
    returning a list of secondary PMF, including the primary itself. Requires
    the file be in teh format #primary_pmf, pmf_2, pmf_3, ..."""
    manifestDict = {}
    with open(pathToManifest, 'r') as inFile:
        for line in inFile.readlines():
            pmfList = list(line.strip().split(','))
            manifestDict[pmfList[0]] = pmfList
    return manifestDict


def write_fiber_mainifest(manifestDict, outPath, includeExpCount=False):
    """Write out a fiber manifest with one line per star, listing the fibers
    used for this star.  Format is #primary_pmf, nExp, pmf_2, nExp2, pmf_3, nExp3, ...
    where the number of exposures used is optional.  Dictionary must be in the form
    {primary_pmf: [(primary_pmf,nExp), (pmf2,nExp2), (pmf3,nExp3), ...] ,  . . . }."""
    outFile = open(outPath, 'w')
    for primary_pmf, pmfList in manifestDict.iteritems():
        if includeExpCount:
            outFile.write('%s,%d' % (pmfList[0][0], pmfList[0][1]))
        else:
            outFile.write('%s' % pmfList[0][0])
        for pmf, nExp in pmfList:
            if pmf == primary_pmf:
                continue
            if includeExpCount:
                outFile.write(',%s,%d' % (pmf, nExp))
            else:
                outFile.write(',%s' % pmf)
        outFile.write('\n')
    outFile.close()


def write_plateid_manifest(prim_pmf, newPlateIDs, outPath):
    """Append to a file the index values for the exposures corresponding
    to each plateid.
    # primary_pmf, plateid_1;idx_1;idx_2, plateid_2;idx_3;idx_4;idx_5 """
    with open(outPath, 'a') as outFile:
        outFile.write('%s' % prim_pmf)
        currentPM = newPlateIDs[0]
        outFile.write(',%s' % currentPM)
        for i, pm in enumerate(newPlateIDs):
            if pm == currentPM:
                outFile.write(';%d' % i)
            else:
                currentPM = pm
                outFile.write(',%s;%d' % (pm, i))
        outFile.write('\n')


def load_data(dbPath, silent=True, returnAsDict=False, cleanExposuresOnly=False, dbtype='rv'):
    """Open the database and extract the data. Return the data in a way
    that is easy to manipulate.  Return data as a dictionary with the fiberid
    as the key, if retunAsDict is set to True.  Otherwise, a list of dictionaries.

    If cleanExposuresOnly, use the cleanExposureIndex to select only these values in
    all exposure-specific categories.

    Specify dbtype as either 'rv' or 'mc'."""
    # Read in the data
    if dbtype == 'rv':
        db = RVDatabase(dbPath, silent=silent)
    else:
        db = MCDatabase(dbPath, silent=silent)
    rows, columnNames = db.get_all_entries()

    # Make a dictionary for each fiber and append to a list
    if returnAsDict:
        fiberData = {}
    else:
        fiberData = []

    for row in rows:
        thisDict = {}
        for col, key in zip(row, columnNames):
            if col is None:
                datum = None
            elif key == 'fiberid':
                datum = str(col)
                fiberid = datum
            elif key == 'modified':
                datum = str(col)
            elif key in ['feh', 'logg', 'teff', 'gmag', 'eoveri', 'massPrimary', 'massRatio', 'separation', 'period', 'bayesFactor']:
                datum = float(col)
            elif key in ['exposureCount', 'cleanExposureCount']:
                datum = int(col)
            elif key in ['flags', 'cleanExposureIndex', 'exposureGoodPix']:
                datum = to_int_list(col)
            elif key in ['absRVs', 'empiricalErrs', 'exposureSNR']:
                datum = to_float_list(col)
            elif key in ['taimids', 'exptimes']:
                datum = [int(float(x.strip(','))) for x in col[1:-1].split()]
            elif key in ['isBinary']:
                datum = bool(col)
            else:
                raise Exception('Invalid column name: %s' % key)
            thisDict[key] = datum

        if cleanExposuresOnly:
            idx = thisDict['cleanExposureIndex']
            if idx is None:
                raise Exception('Invalid cleanExposureCount')
            if len(idx) == 0:
                continue
            for param in ['flags', 'exposureGoodPix', 'absRVs', 'empiricalErrs', 'exposureSNR', 'taimids', 'exptimes']:
                if thisDict[param] is None:
                    continue
                thisDict[param] = [thisDict[param][i] for i in idx]
            thisDict['cleanExposureIndex'] = range(len(idx))

        if returnAsDict:
            fiberData[fiberid] = thisDict
        else:
            fiberData.append(thisDict)

    return fiberData


def split_into_groups(dataDict, cut1, cut2, method='feh'):
    """Split into groups based on one or two cuts (or none) and return a list of 
    dataDictionaries with the appropriate stars inside."""
    groupADict, groupBDict, tempDict = {}, {}, {}

    for pmf, data in dataDict.iteritems():
        if data[method] < cut1:
            groupADict[pmf] = data
        else:
            tempDict[pmf] = data

    if cut2 is None:
        groupBDict = tempDict
        dataDictionaryList = [groupADict, groupBDict]
        vlines = [cut1]
    else:
        groupCDict = {}
        if cut2 <= cut1:
            raise Exception('Invalid cuts.')
        for pmf, data in tempDict.iteritems():
            if data[method] < cut2:
                groupBDict[pmf] = data
            else:
                groupCDict[pmf] = data
        dataDictionaryList = [groupADict, groupBDict, groupCDict]
        vlines = [cut1, cut2]
        print '%d low, %d mid, %d high' % (len(groupADict), len(groupBDict), len(groupCDict))

    return dataDictionaryList, vlines
