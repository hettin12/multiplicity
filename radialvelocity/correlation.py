#!/usr/bin/env python
""" 
Correlation functions.  Use numpy.correlate at various lags to get a
correlation function.  Interpolate the function to high resolution and locate the maximum
of the correlation function.

"""

import sys

import numpy as np
from scipy import signal, interpolate
from matplotlib.pyplot import *

import dsdt.gaussFit as gaussFit

HALF_WIDTH = 20    # +/- 20 lag ~=  +/- 1400 km/s radial velocity.  Compare to 525 km/s MW escape velocity.
ZOOM_WIDTH = 4


def calculate_lag_error(pzoom, ixCorrZoom, s2Zoom):
    """Error calculation based on quality of Lorentzian zoom fit.
    Returns NaN for singular matrix error or negative error."""
    A, mu, width, lagFit = pzoom[0], pzoom[1], pzoom[2], (pzoom[1]-HALF_WIDTH)

    # Fill a 4x4 covariance vector.
    covar = np.zeros((4,4))
    for f in range(4):
        for g in range(4):
            for k in range(len(ixCorrZoom)):  # from ixCorr = peak-4 to peak+4 (unless near edge of ixCorr)
                # Determine Pf
                x = ixCorrZoom[k]
                if f==0:    Pf = 1.0
                elif f==1:  Pf = (width**2) / ((mu-x)**2 + width**2)
                elif f==2:  Pf = (2.0*A)*(width**2)*(x-mu) / ((mu-x)**2 + width**2)**2
                else:       Pf = (2.0*A*width)*((mu-x)**2) / ((mu-x)**2 + width**2)**2
                # Determine Pg
                if g==0:    Pg = 1.0
                elif g==1:  Pg = (width**2) / ((mu-x)**2 + width**2)
                elif g==2:  Pg = (2.0*A)*(width**2)*(x-mu) / ((mu-x)**2 + width**2)**2
                else:       Pg = (2.0*A*width)*((mu-x)**2) / ((mu-x)**2 + width**2)**2
                # Add and iterate to next point in the fit
                covar[f][g] += (Pf*Pg / s2Zoom[k])

    try:
        covar = np.linalg.inv(covar)
    except np.linalg.LinAlgError as (strerror):
        sys.stderr.write('ERROR with taking inverse of covarience matrix: %s\n' % (strerror))
        return float('NaN'), 10**0   # 1: singular_matrix flag

    if covar[2][2] <= 0:
        return float('NaN'), 2*10**0 # 2: negative_error flag
    else:
        return covar[2][2], 0   # (variance = sigma^2) of location of maximum (in pixels^2) from the Lorentzian fit


def resample_xcorr_function(xCorr, s2, calcLagVariance=False, ax=None):
    """Returns the best lag value and error for two spectra by resampling
    the correlation function to higher resolutation and locating the maximum.

    Variance is determined by .... ?"""
    SAMPLING_SIZE = (HALF_WIDTH*2)*1000 + 1
    xold = np.arange(len(xCorr)) - HALF_WIDTH
    xnew = np.linspace(xold[0], xold[-1], SAMPLING_SIZE)

    tck = interpolate.splrep(xold, xCorr)
    xCorrNew = interpolate.splev(xnew, tck)
    maxIndex = np.argmax(xCorrNew)
    lagFit = xnew[maxIndex]

    if ax is not None:
        plot(xnew, xCorrNew, '.', label='Spline Interpolation\nPeak Lag=%1.3f' % lagFit, color='DodgerBlue')
        #errorbar(xold, xCorr, yerr=np.sqrt(s2), ls='none', color='grey', alpha=0.5)
        plot(xold, xCorr, '*', label='Correlation Values', markersize=10, color='r')
        axvline(lagFit, 0, 1, ls='--', color='DodgerBlue')
        xlabel('Lag')
        ylabel('Correlation Function')
        legend(prop={'size':14})

    if calcLagVariance:
        raise Exception("Variance on the Lag Fit is not implemented yet.")
    else:
        sig2 = float('NaN')

    return lagFit, sig2, 0


def calculate_correlation_function(iFlux, iVar, jFlux, jVar, normalize=True, calcVariance=False):
    """Basic cross correlation routine. Returns an array of correlation values as a
    function of lag, accompanied by an array of variances determined by
    sigma^2 = sum(y1^2 * sig2^2 + y2^2 * sig1^2) for a given lag.

    Normalized will divide the xCorr by the total number of pixels."""
    # Cut off the ends (a few 10s of pixels) on one spectrum
    jFluxTrim = jFlux[HALF_WIDTH:-HALF_WIDTH]
    xCorr = np.correlate(iFlux, jFluxTrim, mode="valid")
    if normalize:
        pixCount = float(len(jFluxTrim))
        xCorr = xCorr / pixCount

    # Error calculation
    if calcVariance:
        s2 = []
        y2     = jFlux[HALF_WIDTH:-HALF_WIDTH]
        var2   = jVar[HALF_WIDTH:-HALF_WIDTH]
        for k in range(2*HALF_WIDTH + 1):  # from k=0 to 2*HW
            s2.append(0.0)
            y1     = iFlux[k:]
            var1   = iVar[k:]
            for m in range(len(y2)):
                s2[k] += y1[m]*y1[m]*var2[m] + y2[m]*y2[m]*var1[m]  # sigma^2 = sum(y1^2 * sig2^2 + y2^2 * sig1^2)
        if normalize:
            s2 = [val / pixCount / pixCount for val in s2]
    else:
        s2 = [float('NaN') for i in range(2*HALF_WIDTH + 1)]

    return xCorr, s2, 0


def calculate_lag(specFlux, specVar, templateFlux, templateVar, padding=True, ax=None):
    """Give a list of spectral fluxes and a template flux,
    cross-cross correlates the spectrum with the template
    and returns the best lag (in pixels) along with uncertainty
    determined by the quality of the fit to the xCorr function.

    Padding adds 1's on either end of the spectrum and template. This will
    only work with normalized spectra."""
    # Pad the ends with 1s
    if padding:
        paddedTemplateFlux = []
        paddedTemplateFlux.extend(np.ones(HALF_WIDTH+2))
        paddedTemplateFlux.extend(templateFlux)
        paddedTemplateFlux.extend(np.ones(HALF_WIDTH+2))

        paddedTemplateVar = []
        paddedTemplateVar.extend(np.zeros(HALF_WIDTH+2))
        paddedTemplateVar.extend(templateVar)
        paddedTemplateVar.extend(np.zeros(HALF_WIDTH+2))

        paddedSpecFlux = []
        paddedSpecFlux.extend(np.ones(HALF_WIDTH+2))
        paddedSpecFlux.extend(specFlux)
        paddedSpecFlux.extend(np.ones(HALF_WIDTH+2))

        paddedSpecVar = []
        paddedSpecVar.extend(np.zeros(HALF_WIDTH+2))
        paddedSpecVar.extend(specVar)
        paddedSpecVar.extend(np.zeros(HALF_WIDTH+2))

        xcorr, s2, xcorrFlags = calculate_correlation_function(paddedSpecFlux, paddedSpecVar, paddedTemplateFlux, paddedTemplateVar)
    else:
        xcorr, s2, xcorrFlags = calculate_correlation_function(specFlux, specVar, templateFlux, templateVar)

    lag, lag_var, fitFlags = resample_xcorr_function(xcorr, s2, ax=ax)
    lagFlags = xcorrFlags + fitFlags

    return lag, np.sqrt(lag_var), lagFlags

