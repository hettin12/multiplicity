from numpy import log

from multiplicity.radialvelocity import absoluterv
from multiplicity.fits import fitsIO

def get_all_by_key(dataDict, key):
    """Return all values in the dictionary for a single
    key.  Return the values as a list."""
    return [data[key] for pmf,data in dataDict.iteritems()]


def print_header(name, caption, col_strings, comments, label):
    """Print a header for a LaTeX table given the various column names."""
    outString = """
    \\newcommand{\\%s}{
      \\begin{deluxetable}{""" % name

    for i in range(len(col_strings)):
        outString += 'c'

    outString += """}
        \\tablecaption{%s}
        \\tablehead{""" % caption

    for i in range(len(col_strings)):
        outString += """\\colhead{%s} & """ % col_strings[i]
    outString = outString[:-2]

    outString += """}
        \\startdata

            DATA HERE

        \\enddata
        \\tablecomments{%s}
        \\label{%s}
      \\end{deluxetable}
    }
    """ % (comments, label)

    print outString


def print_data_row(data):
    """Print the row of data in the form required for LaTeX."""
    outString = '            '
    for val in data:
        outString += '%s & ' % val
    outString = outString[:-2] + '\\\\'
    print outString


def build_eoveri_table(dataDict, ssppDict):
    """Build a table with columns that look like:
    ID(primary PMF)   RA(J2000)   DEC(J2000)   Fe/H   Fe/H_err   Nexp   meanRV   meanRV_err   e   i   e/i
    -----------------------------------------------------------------------------------------------------
    """
    # Print header
    col_strings = ['ID', 'RA (J2000)', 'DEC (J2000)', '[Fe/H]', '$\sigma_\mathrm{[Fe/H]}$', '$N_\mathrm{exp}$', '\meanRV', '\sigma_{\rv}', '$e$', '$i$', '$e/i$']
    print_header('tblEOI', 'NONE', col_strings, 'NONE', 'tbl:eoi')

    # Print data
    itemList = []
    count = -1
    for key, data in dataDict.iteritems():
        count += 1

        fiberid = data['fiberid']
        rvs = data['absRVs']
        errors = data['empiricalErrs']
        meanRV = '%.3f' % absoluterv.calc_weighted_mean(rvs, errors)
        stdErr = '%.3f' % absoluterv.calc_weighted_standard_error(rvs, errors)

        e = '%.3f' % absoluterv.calc_weighted_std(rvs, errors)
        i = '%.3f' % absoluterv.calc_i(errors)
        eoveri = absoluterv.calc_eoveri(rvs, errors)

        paramDict = ssppDict[fiberid]
        feh = '%.3f' % paramDict['FEH_ADOP']
        fehErr = '%.3f' % paramDict['FEH_ADOP_UNC']
        ra = fitsIO.degrees_RA_to_hours(paramDict['RA'])
        dec = fitsIO.degrees_DEC_to_sexigesimal(paramDict['DEC'])

        thisList = [fiberid, ra, dec, feh, fehErr, data['exposureCount'], meanRV, stdErr, e, i,  eoveri]
        itemList.append(thisList)

    itemList.sort(key=lambda k: k[10], reverse=True)
    for q in itemList:
        q[10] = '%.3f' % (q[10])
        print_data_row(q)


def build_bayesFactor_table(dataDict, bayesDict):
    """Build a table with columns that look like:
    ID(primary PMF)   -2lnK
    -----------------------
    """
    # Print header
    col_strings = ['ID', '-2lnK']
    print_header('tblBayesFactor', 'NONE', col_strings, 'NONE', 'tbl:bayesFactor')

    # Print data
    itemList = []
    count = -1
    for key, data in dataDict.iteritems():
        count += 1

        fiberid = data['fiberid']
        K = bayesDict[fiberid][2]
        m2lnk = -2.0 * log(K)

        thisList = [fiberid, m2lnk]
        itemList.append(thisList)

    itemList.sort(key=lambda k: k[1])
    for q in itemList:
        q[1] = '%.3f' % (q[1])
        print_data_row(q)



