""" 
A set of classes for managing sqlite3 database files.  This wrapper is extremely vulnerable to 
SQL injection attacks.  Do not use publicly!!  The Database() currently only supports a single Table.

    * Database() is the parent class.
        create_table
        insert_column
        insert_entry
        id_in_table
        print_column_names
        print_entry
        print_table
        set_value
        set_many_values
        get_value
        get_many_values
        get_rows_with_fields

        * RVDatabase() is a child class that specifically handles radial velocity data produce from cross correlation testing.
            create_table (overwritten)

"""

import os, sys

import sqlite3

class Database():
    def __init__(self, dbPath, create=False, silent=True):
        self.tableName = 'NewTable'
        self.primaryKey = 'id'
        if create:
            if os.path.exists(dbPath):
                raise Exception("Warning: A table already exists for %s.  Not creating." % dbPath)
            if not silent:
                print "Creating db at %s." % dbPath
            self.connection = sqlite3.connect(dbPath)
            self.create_table()
        elif os.path.isfile(dbPath):
            if not silent:
                print '\n[] Connected to %s' % dbPath
            self.connection = sqlite3.connect(dbPath)
        else:
            raise Exception("Database %s does not exist." % dbPath)


    def __del__(self):
        self.connection.close()


    def __str__(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM %s" % (self.tableName))
        col_names = [cn[0] for cn in cursor.description]
        rows = cursor.fetchall()
        N = len(rows)
        outString = ''
        outString = outString + str(col_names) + '\n'
        for r in range(N):
            thisRow = list(str(col) for col in rows[r])
            outString = outString + str(thisRow) + '\n'
        return outString


    def create_table(self):
        """Make the table. Creating a second table adds a table, but all functions will reference 
        the most recently created table. This function is responsible
        for setting the database's tableName and primaryKey attributes."""
        cursor = self.connection.cursor()
        cursor.execute('''CREATE TABLE %s 
                       (%s text PRIMARY KEY,
                        modified text)''' % (self.tableName, self.primaryKey))
        self.connection.commit()


    def insert_column(self, colname, datatype):
        """Add a column to the database table. Datatypes include [int, real, text]."""
        cursor = self.connection.cursor()
        cursor.execute('''ALTER TABLE %s ADD COLUMN %s %s''' % (self.tableName, colname, datatype))
        self.connection.commit()


    def insert_entry(self, uniqueID):
        """Add a new row to the table with uniqueID."""
        cursor = self.connection.cursor()
        cursor.execute('''INSERT INTO %s (%s) VALUES (?)''' % (self.tableName, self.primaryKey), [uniqueID])
        self.connection.commit()


    def remove_entry(self, uniqueID):
        """Remove a row from the table based on uniqueID. This 
        can not be undone."""
        cursor = self.connection.cursor()
        cursor.execute('''DELETE FROM %s WHERE %s=?''' % (self.tableName, self.primaryKey), [uniqueID])
        self.connection.commit()


    def id_in_table(self, uniqueID):
        """Check if the id is listed in the table already.  If it 
        is, return the the row of data. If it is not, return None."""
        cursor = self.connection.cursor()
        cursor.execute('''SELECT * FROM %s WHERE %s=?''' % (self.tableName, self.primaryKey), [uniqueID])
        row = cursor.fetchone()
        return row


    def print_column_names(self):
        """Print out a list of all columns and their types."""
        cursor = self.connection.cursor()
        cursor.execute("PRAGMA table_info(%s)" % self.tableName)
        rows = cursor.fetchall()
        for row in rows:
            print row


    def get_column_names(self):
        """Return a list of names for the columns."""
        cursor = self.connection.cursor()
        cursor.execute("PRAGMA table_info(%s)" % self.tableName)
        rows = cursor.fetchall()
        names = [str(r[1]) for r in rows]
        return names


    def print_entry(self, uniqueID):
        """Print entire row of information for a given id."""
        row = self.id_in_table(uniqueID)
        if row is not None:
            print row


    def print_table(self, peek=False, cols=None):
        """Print the entire table.  Optionally, peek=True
        will print the first 10 entries.  Also, use
        cols=['colname1', 'colname2', ...] to print just those columns."""
        self.connection.row_factory = sqlite3.Row
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM %s" % (self.tableName))
        col_names = [cn[0] for cn in cursor.description]
        rows = cursor.fetchall()
        if peek:
            N = min(10, len(rows))
        else:
            N = len(rows)

        if cols is not None:
            print cols
            for r in range(N):
                lineout = []
                for col in cols:
                    lineout.append(rows[r][col])
                print lineout
        else:
            print col_names
            for r in range(N):
                print rows[r]


    def get_entry(self, uniqueID):
        """Return the row for this entry (None if not found)."""
        row = self.id_in_table(uniqueID)
        return row


    def get_all_entries(self):
        """Return a tuple (all rows in the table, column names)."""
        self.connection.row_factory = sqlite3.Row
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM %s" % (self.tableName))
        columnNames = [column[0] for column in cursor.description]
        rows = cursor.fetchall()
        return rows, columnNames


    def get_uniqueIDs(self):
        """Return a list of all uniqueIDs."""
        cursor = self.connection.cursor()
        cursor.execute('''SELECT %s FROM %s''' % (self.primaryKey, self.tableName))
        rows = cursor.fetchall()
        return [str(c[0]) for c in rows]


    def set_value(self, uniqueID, field, value):
        """Update the database with a single value for an entry."""
        if self.id_in_table(uniqueID) is not None:
            cursor = self.connection.cursor()
            counter = 0
            while counter < 50:
                try:
                    cursor.execute('''UPDATE %s SET %s=? WHERE %s=?''' % (self.tableName, field, self.primaryKey), [value, uniqueID])
                    cursor.execute('''UPDATE %s SET modified=datetime('now') WHERE %s=?''' % (self.tableName, self.primaryKey), [uniqueID])
                    self.connection.commit()
                    break
                except sqlite3.OperationalError as detail:
                    sys.stderr.write("Error trying to update (id %s, field %s, value %s):\n%s\n" % (uniqueID, field, value, detail))
                    if counter >= 49:
                        raise
            counter += 1

        else:
            sys.stderr.write("Warning: %s not found in table." % uniqueID)


    def set_many_values(self, uniqueID, **kwargs):
        """Set multiple columns of entry by using database keywords and the new values.

        kwargs: field1=val1, field2=val2, ..."""
        if not kwargs:
            sys.stderr.write("Warning: no fields specified for set_values.")
            return

        if self.id_in_table(uniqueID) is not None:
            cursor = self.connection.cursor()
            counter = 0
            while counter < 50:
                try:
                    for field, value in kwargs.items():
                        cursor.execute('''UPDATE %s SET %s=? WHERE %s=?''' % (self.tableName, field, self.primaryKey), [value, uniqueID])
                    cursor.execute('''UPDATE %s SET modified=datetime('now') WHERE %s=?''' % (self.tableName, self.primaryKey), [uniqueID])
                    self.connection.commit()
                    break
                except sqlite3.OperationalError as detail:
                    sys.stderr.write("Error trying to update id %s:\n%s\n" % (uniqueID, detail))
                    if counter >= 49:
                        raise
            counter += 1

        else:
            sys.stderr.write("Warning: %s not found in table." % uniqueID)


    def get_value(self, uniqueID, field):
        """Return the value given the row's ID and field of interest."""
        if self.id_in_table(uniqueID) is not None:
            cursor = self.connection.cursor()
            try:
                cursor.execute('''SELECT %s FROM %s WHERE %s=?''' % (field, self.tableName, self.primaryKey), [uniqueID])
                value = cursor.fetchone()
                return value[0]
            except sqlite3.OperationalError as detail:
                sys.stderr.write("Error trying to get (id %s, field %s):\n%s\n" % (uniqueID, field, detail))
                return None
        else:
            sys.stderr.write("Warning: %s not found in table." % uniqueID)


    def get_many_values(self, uniqueID, *args):
        """Return the values given the row's ID and a list of fields."""
        values = []
        for field in args:
            values.append(self.get_value(uniqueID, field))
        return values


    def get_rows_with_fields(self, *args):
        """Return a list of tuples.  All rows are returned, including fields 
        specified as *args.  No fields specified returns the complete database."""
        if args is None:
            fieldString = '*'
        else:
            fieldString = ', '.join(args)
        self.connection.row_factory = sqlite3.Row
        cursor = self.connection.cursor()
        cursor.execute('''SELECT %s FROM %s ORDER BY %s''' % (fieldString, self.tableName, self.primaryKey))
        rows = cursor.fetchall()
        return rows



class RVDatabase(Database):
    """Specifically handles RV data and stellar params from cross correlations and the SSPP."""

    def __init__(self, dbPath, create=False, silent=True):
        self.tableName = 'FiberData'
        self.primaryKey = 'fiberid'
        if create:
            if os.path.exists(dbPath):
                raise Exception("Warning: A table already exists for %s.  Not creating." % dbPath)
            if not silent:
                print "Creating db at %s." % dbPath
            self.connection = sqlite3.connect(dbPath)
            self.create_table()
        elif os.path.isfile(dbPath):
            if not silent:
                print '\n[] Connected to %s' % dbPath
            self.connection = sqlite3.connect(dbPath)
        else:
            raise Exception("Database %s does not exist." % dbPath)


    def create_table(self):
        """Make a database with the necessary columns for storing data
        from the SSPP and cross correlation testing."""
        cursor = self.connection.cursor()
        cursor.execute('''CREATE TABLE %s 
                       (%s text PRIMARY KEY, 
                        flags text, 
                        exposureCount int, 
                        taimids text, 
                        exptimes text,
                        exposureSNR text, 
                        exposureGoodPix text,
                        cleanExposureCount int, 
                        cleanExposureIndex text, 
                        absRVs text, 
                        empiricalErrs text,
                        eoveri real,
                        feh real, 
                        logg real, 
                        teff real, 
                        gmag real, 
                        modified text)''' % (self.tableName, self.primaryKey))
        self.connection.commit()


class MCDatabase(Database):
    """Specifically handles RV data and stellar params from Monte-Carlo runs."""

    def __init__(self, dbPath, create=False, silent=True):
        self.tableName = 'FiberData'
        self.primaryKey = 'fiberid'
        if create:
            if os.path.exists(dbPath):
                raise Exception("Warning: A table already exists for %s.  Not creating." % dbPath)
            if not silent:
                print "Creating db at %s." % dbPath
            self.connection = sqlite3.connect(dbPath)
            self.create_table()
        elif os.path.isfile(dbPath):
            if not silent:
                print '\n[] Connected to %s' % dbPath
            self.connection = sqlite3.connect(dbPath)
        else:
            raise Exception("Database %s does not exist." % dbPath)


    def create_table(self):
        """Make a database with the necessary columns for storing data
        from the monte-carlo stellar param draws and RV draws."""
        cursor = self.connection.cursor()
        cursor.execute('''CREATE TABLE %s 
                       (%s text PRIMARY KEY,
                        isBinary bit,
                        massPrimary real,
                        massRatio real,
                        separation real,
                        period real,
                        exposureCount int,
                        taimids text,
                        exposureSNR text,
                        feh real,
                        empiricalErrs text,
                        absRVs text,
                        eoveri real,
                        bayesFactor real,
                        modified text)''' % (self.tableName, self.primaryKey))
        self.connection.commit()

